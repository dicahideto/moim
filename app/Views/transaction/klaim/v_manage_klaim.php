

 <?php
     $step_user  = $_SESSION['step_sess'];
     
    if ($step_user == '0')
    {
        $linked     = 'xlink';
       
        
    }
    else if ($step_user == '1')
    {
       $linked     = 'xlink';
    }
    else
    {
        $linked     = '';
       
    }

    if ($linked     == 'xlink')
    {
        $display    = '';
    }
    else
    {
        $display    = 'display:none;';
    }

 ?>


 <?php

    date_default_timezone_set("Asia/Jakarta");
            
    $month_var      = date('m');
    $year_var       = date('Y');

    $gb_month = bulan_indo_to_alpha($month_var);

?>

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">

            <div class="card widget-todo">
                <div class="card-header border-bottom d-flex justify-content-between align-items-center flex-wrap">
                    <h4 class="card-title d-flex mb-25 mb-sm-0">
                        <i class='bx bx-check font-medium-5 pl-25 pr-75'></i>Berkas Klaim &nbsp; <span id='berkas_klaim' ><?php echo $gb_month.' '.$year_var;?> </span>
                    </h4>
                    <ul class="list-inline d-flex mb-25 mb-sm-0">
                        <li class="d-flex align-items-center">
                            <i class='bx bx-sort mr-50 font-medium-3'></i>
                            <div class="dropdown">
                                <div class="dropdown-toggle cursor-pointer" role="button" id="periode_sess" aria-haspopup="true" aria-expanded="true" data-toggle="modal" data-target="#default"><?php echo $gb_month.' '.$year_var;?></div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="" style='padding:10px;'>
                     <div class='col-md-12'>
                        <div style='text-align:center;<?php echo $display;?>'>
                            <input value='Import Data Klaim' type="button" id="import_data_klaim" class="btn btn-sm btn-info glow mr-1 mb-1">
                        </div>
                    </div>
                            
                   <table class="" id="t_list_manage_klaim" style="width:100%">
                        <thead>
                            <tr>
                                <th class='bpad10t bleft btop bbottom' >Grup</th>
                                <th class='bpad10t btop bbottom' style='text-align:center;' width='25%'>Total Data</th>
                                <th class='bpad10t btop bbottom bright' style='text-align:left;' width='30%'>Approval Terbaru/ Data Klaim Tanggal Keluar</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </div>
</section>

</div>
</div>



<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel1">Periode</h3>
                <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <select class="select21 form-control" id="bulan" name='selectselect' placeholder='Bulan'>
                            <option ></option>

                            <option value="01" <?php if($month_var == '1'){ echo ' selected="selected"'; } ?> >Januari</option>
                            <option value="02" <?php if($month_var == '2'){ echo ' selected="selected"'; } ?> >Februari</option>
                            <option value="03" <?php if($month_var == '3'){ echo ' selected="selected"'; } ?> >Maret</option>
                            <option value="04" <?php if($month_var == '4'){ echo ' selected="selected"'; } ?> >April</option>
                            <option value="05" <?php if($month_var == '5'){ echo ' selected="selected"'; } ?> >Mei</option>
                            <option value="06" <?php if($month_var == '6'){ echo ' selected="selected"'; } ?> >Juni</option>
                            <option value="07" <?php if($month_var == '7'){ echo ' selected="selected"'; } ?> >Juli</option>
                            <option value="08" <?php if($month_var == '8'){ echo ' selected="selected"'; } ?> >Agustus</option>
                            <option value="09" <?php if($month_var == '9'){ echo ' selected="selected"'; } ?> >September</option>
                            <option value="10" <?php if($month_var == '10'){ echo ' selected="selected"'; } ?> >Oktober</option>
                            <option value="11" <?php if($month_var == '11'){ echo ' selected="selected"'; } ?> >November</option>
                            <option value="12" <?php if($month_var == '12'){ echo ' selected="selected"'; } ?> >Desember</option>
                        </select>
                    </div>
                </div>

                <br />
                <div class='row'>
                    <div class='col-md-6'>
                       <select class="select22 form-control" id="tahun" name='selectselect' placeholder='Tahun'>
                            <option ></option>
                            <option value="2020" <?php if($year_var == '2020'){ echo ' selected="selected"'; } ?> >2020</option>
                            <option value="2021" <?php if($year_var == '2021'){ echo ' selected="selected"'; } ?> >2021</option>
                            <option value="2022" <?php if($year_var == '2022'){ echo ' selected="selected"'; } ?> >2022</option>
                            <option value="2023" <?php if($year_var == '2023'){ echo ' selected="selected"'; } ?> >2023</option>
                            <option value="2024" <?php if($year_var == '2024'){ echo ' selected="selected"'; } ?> >2024</option>
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Batal</span>
                </button>
                <button type="button" class="btn btn-primary ml-1" data-dismiss="modal" id='change_periode'>
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Ubah Periode</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>/public/tmpassets/app/transaction/klaim/Manage_klaim.js?v=1"></script>
<script type="text/javascript">
    Manage_klaim.init();
</script>
