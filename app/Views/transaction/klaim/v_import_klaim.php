<style>


#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  max-width: 60%;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>


<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
            </div>
        </div>
    </div>
</div>
<div class="content-body">

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Import Data Klaim</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class='col-md-12'>
                            <form class="form form-horizontal" id="form_main" enctype="multipart/form-data">
                                 <div class="form-group">
                                    <label>Type File</label>
                                </div>
                                <div class="form-group">
                                    <div class='radiocheck_span'>
                                        <div class='row'>
                                            <div class="col-md-3 col-5">
                                                
                                                    <div class="card border shadow-none mb-1 app-file-info">
                                                        <label>
                                                        <div class="card-content">
                                                            <div class="app-file-content-logo card-img-top">
                                                                <img data-toggle="modal" data-target="#myModal1" style='cursor:pointer;' id='myImgOne' class="d-block mx-auto" src="<?php echo base_url();?>/public/tmpassets/assets/sysimg/export_type_one.PNG" height="100" width="100" alt="Preview">
                                                            </div>
                                                            <div class="card-body p-50">
                                                                <div class="app-file-recent-details" align='center'>
                                                                    <input type='radio' name='file_num' id='file_num1' value='satu' /> 
                                                                        Type 1
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </label>
                                                    </div>
                                                
                                            </div>
                                            <div class="col-md-3 col-5">
                                                
                                                    <div class="card border shadow-none mb-1 app-file-info">
                                                        <label>
                                                        <div class="card-content">
                                                            <div class="app-file-content-logo card-img-top">
                                                                <img data-toggle="modal" data-target="#myModal2" style='cursor:pointer;' id='myImgTwo' class="d-block mx-auto" src="<?php echo base_url();?>/public/tmpassets/assets/sysimg/export_type_two.PNG" height="100" width="100" alt="Preview">
                                                            </div>
                                                            <div class="card-body p-50">
                                                                <div class="app-file-recent-details" align='center'>
                                                                    <input type='radio' name='file_num' id='file_num2' value='dua' /> 
                                                                        Type 2
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </label>
                                                    </div>
                                                
                                            </div>
                                        </div>
                                        


                                      
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label>Format Tanggal</label>
                                </div>
                                 <div class="form-group">
                                   
                                        <select class="select2 form-control" id="formattanggal" name='formattanggal' style='width:500px;' >
                                            <option ></option>
                                            <option value="one">1 Dec 2021 20:11 / 1 December 2021 20:11 / 1 Des 2021 20:11</option>
                                            <option value="two">01/12/2021 20:11</option>
                                            <option value="three">01-12-2021 20:11</option>
                                        </select>

                                </div>

                                <div class="form-group">
                                    <label>File Excel (xls, xlsx)</label>
                                    <input type="file" name="file" class="form-control" id="file" required accept=".xls, .xlsx" /></p>
                                </div>
                                <div class="form-group" style='text-align:center;'>
                                    <input type='button' id='but_upload' class="btn btn-primary" value='Upload'>
                                </div>
                            </form>
                        </div>

                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>


<script>
// Get the modal
var modal = document.getElementById("myModal");

// var img = document.getElementById("myImgOne");
// var img = $('#img01');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
$("#myImgOne").click(function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
});

$("#myImgTwo").click(function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
});

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

window.onclick = function(event) {
    // alert(event.target.id);
   if (event.target.id == "caption") {
      $("#myModal").hide();
   }
}


</script>



<script src="<?php echo base_url(); ?>/public/tmpassets/app/transaction/klaim/Import_klaim.js"></script>
<script type="text/javascript">
    Import_klaim.init();

   

</script>

