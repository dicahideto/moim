
<style>
    /* The heart of the matter */
    .testimonial-group > .row {
      overflow-x: scroll;
      white-space: nowrap;
      width:100%;


    }

    div.sticky {
      bottom: 1%;
      right: 45%;
      position: fixed;
      z-index: 3000;
    }
            
</style>    

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
            </div>
        </div>
    </div>
</div>
<div class="content-body">

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Data Import</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class='row'>
                                <div class='col-md-6' style='text-align:left;'>
                                   
                                </div>
                                <div class='col-md-6' style='text-align:right;'>
                                    <button type="button" id="submit_process" class="btn btn-sm btn-primary mr-1 mb-1" >Simpan Data Import</button>
                                </div>
                        </div>
                        <div class="sticky">
                            <button id="left-button" class="btn btn-sm btn-outline-secondary">
                                swipe left
                            </button>
                            
                            <button id="right-button" class="btn btn-sm btn-outline-secondary">
                                swipe right
                            </button>
                        </div>

                        <br />

                        <div class="testimonial-group " >
                            <div class="row flex-nowrap" id="abcde"  style='white-space: nowrap;'>

                                <table class="" id="t_data_klaim" style="overflow-x: auto;overflow-y:visible;white-space: nowrap;font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th class='bpad7 bleft bright btop bbottom bright' style='min-width:50px;text-align:center' width='50px'>#</th>
                                            <th class='bpad7  bleft bright btop bbottom' style='min-width:80px;text-align:center;' width='80px'><input type='checkbox' id="checkAll"/></th>
                                            <th class='bpad7  bright btop bbottom' style='min-width:50px;text-align:center;' width='50px'>No</th>
                                            <th class='bpad7  bright btop bbottom' style='min-width:150px;' width='150px'>No. RM</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px' >Nama Pasien</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Jaminan</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Unit/ Dept</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Ruang/ Bed</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Masuk</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Keluar</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>DPJP</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>





<script src="<?php echo base_url(); ?>/public/tmpassets/app/transaction/klaim/List_import_klaim.js"></script>
<script type="text/javascript">
    List_import_klaim.init();
</script>
