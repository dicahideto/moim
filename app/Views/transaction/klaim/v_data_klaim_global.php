 
<style>
    /* The heart of the matter */
    .testimonial-group > .row {
      overflow-x: scroll;
      white-space: nowrap;
      width:100%;
    }

    div.sticky {
      bottom: 1%;
      right: 45%;
      position: fixed;
      z-index: 3000;
    }       
</style>    

<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
            </div>
        </div>
    </div>
</div>
<div class="content-body">

<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Data Klaim Per Grup</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class='row'>
                                <div class='col-md-8' >
                                    <fieldset id='filter_normal'>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Filter Utama</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" id='value_filter_utama'>
                                        </div>
                                    </fieldset>
                                    <fieldset id='filter_date' style='display:none;'>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Filter Utama</span>
                                            </div>
                                            <input type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" id='value_filter_utama_date'>
                                        </div>
                                    </fieldset>
                                    <fieldset id='filter_date_range' style='display:none;'>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Awal &nbsp;&nbsp;</span>
                                            </div>
                                            <input type="text" class="form-control value_filter_utama_date_" placeholder="" aria-describedby="basic-addon1" id='value_filter_utama_date_one'>
                                            
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Akhir &nbsp;</span>
                                            </div>
                                            <input type="text" class="form-control value_filter_utama_date_" placeholder="" aria-describedby="basic-addon1" id='value_filter_utama_date_two'>

                                        </div>
                                       
                                    </fieldset>
                                </div>
                                <div class='col-md-2'>
                                    <select class="form-control"  name='selectselect' id='value_select_filter'>
                                        <option ></option>
                                        <option value="tgl_keluar">Tanggal Keluar</option>
                                        <option value="tgl_keluar_range">Range Tanggal Keluar</option>
                                        <option value="nama_pasien">Nama Pasien</option>
                                        <option value="rm_pasien">RM Pasien</option>
                                    </select>
                                </div>
                                <div class='col-md-2'>
                                    <input type="button" id="submit_filter_cari" class="btn btn-sm btn-info glow mr-1 mb-1" value="Cari" style=''>
                                    <input type="button" id="submit_excel" class="btn btn-sm btn-primary glow mr-1 mb-1" value="Excel" style=''>
                                </div>
                        </div>

                        <div class="sticky">
                            <button id="left-button" class="btn btn-sm btn-outline-secondary">
                                swipe left
                            </button>
                            
                            <button id="right-button" class="btn btn-sm btn-outline-secondary">
                                swipe right
                            </button>
                        </div>

                        <br />
                        <div class="testimonial-group " >
                            <div class="row flex-nowrap" id="abcde"  style='white-space: nowrap;'>
                                <table class="" id="t_data_klaim" style="overflow-x: auto;overflow-y:visible;white-space: nowrap;font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th class='bpad7 bleft bright btop bbottom' style='min-width:50px;text-align:center;' width='50px'>No</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:150px;' width='150px'>Progress</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Diselesaikan/ Approval</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:150px;' width='150px'>No. RM</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px' >Nama Pasien</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Jaminan</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Unit/ Dept</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Ruang/ Bed</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Masuk</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Keluar</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>DPJP</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Eval</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan='13' id='default_empty_td' style='padding:10px;' class='btop bleft bbottom bright bpad7'>
                                                Data Kosong
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>

<script src="<?php echo base_url(); ?>/public/tmpassets/app/transaction/klaim/Data_klaim_global.js"></script>
<script type="text/javascript">
    Data_klaim_global.init();

</script>
