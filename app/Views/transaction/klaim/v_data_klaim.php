
<style>
    /* The heart of the matter */
    .testimonial-group > .row {
      overflow-x: scroll;
      white-space: nowrap;
      width:100%;


    }

    div.sticky {
      bottom: 1%;
      right: 45%;
      position: fixed;
      z-index: 3000;
    }
            
</style>    

<?php
    $step_user  = $_SESSION['step_sess'];
 
    if ($step_user == '0')
    {
        $linked     = 'xlink';
        $cursor     = 'cursor:pointer;';
        
    }
    else if ($step_user == '1')
    {
        if ($step_newpage == '1')
        {
            $linked     = 'xlink';
            $cursor     = 'cursor:pointer;';
        }
        else
        {
            $linked     = '';
            $cursor     = '';
        }
    }
    else
    {
        $linked     = '';
        $cursor     = '';
    }

    if ($linked     == 'xlink')
    {
        if ($step_user == '0')
        {
            if ($step_newpage == '1')
            {
                $display        = '';
                $display_del    = '';
            }
            else
            {
               $display     = 'display:none;';
               $display_del = '';
            }
        }
        else
        {
            $display        = '';
            $display_del    = '';
        }
    }
    else
    {
        $display        = 'display:none;';
        $display_del    = 'display:none;';
    }

   if ($step_user == '0')
   {
        $progress_btn_show = '';   
   }
   else
   {
        if ($step_user == $step_newpage)
        {
            $progress_btn_show = '';
        }
        else
        {
            $progress_btn_show = 'display:none;';   
        }
   }

   if ($step_newpage == '5' || $step_newpage == '100')
   {
        $proses_title = "Proses Selesai";
   }
   else
   {
        $proses_title = "Proses ke Tahap Selanjutnya";
   }


?>


<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
             <input type='hidden' id='step_newpage' value='<?php echo $step_newpage;?>'>       
             <input type='hidden' id='dateout' value='<?php echo $dateout;?>'>       
             <input type='hidden' id='code_group_date_out' value='<?php echo tgl_unique($dateout);?>'>       
             <input type='hidden' id='code_log' value='<?php echo $code_log; ?>' >
     
<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">

                        Daftar Data Klaim Per Grup &nbsp;
                        <div class="badge badge-pill badge-glow badge-light mr-1 mb-1">
                            <span style='margin-top:-10px;font-size:0.8em;color:#fff;'>
                                <?php 
                                    echo tgl_inggris($dateout);
                                ?>
                                | Progress : <?php echo $step_desc;?>
                            </span>    
                        </div>                        
                    </h4>

                </div>
                <!-- t_data_klaim -->
                <div class="card-content">
                    <div class="card-body">
                        <div class='row'>
                                <div class='col-md-6' style='text-align:left;'>
                                    <input  type="button" id="tambah_klaim" class="btn btn-sm btn-info glow mr-1 mb-1" value="Tambah Data Klaim" style='<?php echo $display;?>'>
                                </div>
                                <div class='col-md-6' style='text-align:right;'>
                                    <button type="button" id="submit_process" class="btn btn-sm btn-primary mr-1 mb-1" style='<?php echo $progress_btn_show;?>'><?php echo $proses_title;?></button>
                                </div>
                        </div>
                        <div class="sticky">
                            <button id="left-button" class="btn btn-sm btn-outline-secondary">
                                swipe left
                            </button>
                            
                            <button id="right-button" class="btn btn-sm btn-outline-secondary">
                                swipe right
                            </button>
                        </div>

                        <br />

                        <div class="testimonial-group " >
                            <div class="row flex-nowrap" id="abcde"  style='white-space: nowrap;'>

                                <table class="" id="t_data_klaim" style="overflow-x: auto;overflow-y:visible;white-space: nowrap;font-size:12px;">
                                    <thead>
                                        <tr>
                                            <th class='bpad7 bleft bright btop bbottom bright' style='min-width:50px;text-align:center;<?php echo $display_del;?>' width='50px'>#</th>
                                            <th class='bpad7  bleft bright btop bbottom' style='min-width:80px;text-align:center;' width='80px'><input type='checkbox' id="checkAll"/></th>
                                            <th class='bpad7  bright btop bbottom' style='min-width:50px;text-align:center;' width='50px'>No</th>
                                            <th class='bpad7  bright btop bbottom' style='min-width:150px;' width='150px'>No. RM</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px' >Nama Pasien</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Jaminan</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Unit/ Dept</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Ruang/ Bed</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Masuk</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Tgl Keluar</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>DPJP</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Eval</th>
                                            <th class='bpad7 bright btop bbottom' style='min-width:250px;' width='250px'>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>

                                 
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>

<script src="<?php echo base_url(); ?>/public/tmpassets/app/transaction/klaim/Data_klaim.js"></script>
<script type="text/javascript">
    Data_klaim.init();
</script>
