<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                
            </li>
            <li class="nav-item nav-toggle">
            	<a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
            	<i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i>
            	<i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">

	<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
    <?php
                $db = db_connect();
                $user_sess 							= $_SESSION['codeuser_sess'];
                $code_group_sess 					= $_SESSION['code_group_sess'];

                $query = $db->query("SELECT
									*
									FROM
									r_usergroupmenu satu
									LEFT JOIN
									m_menu dua ON (satu.code_menu = dua.code) where dua.menu_rank = '1' and dua.status_cd = 'normal' and satu.code_group = '$code_group_sess' order by dua.ordering desc");
                
                if ($query->getNumRows() > 0)
                {
                    $results = $query->getResult();
                    foreach ($results as $m1)
                    {
                       if ($m1->have_sm == 1)
					   {
							echo "<li class=' nav-item'><a href='javascript:;'><i class='menu-livicon' data-icon='".$m1->icon."'></i><span class='menu-title' data-i18n='Invoice'>".$m1->nama."</span></a>";
							echo "<ul class='menu-content'>";

							$query = $db->query("SELECT
												*
												FROM
												r_usergroupmenu satu
												LEFT JOIN
												m_menu dua ON (satu.code_menu = dua.code) where dua.menu_rank = '2' and dua.menu_id_child = '$m1->code' and satu.code_group = '$code_group_sess' and dua.status_cd = 'normal' order by dua.ordering asc");
							if ($query->getNumRows() > 0)
                			{	
								$results = $query->getResult();
								foreach ($results as $m2)
								{
									if ($m2->have_sm == 1)
									{
										echo "<li>
												<a href='".base_url($m2->url)."'><i class='bx bx-right-arrow-alt'></i><span class='menu-item' data-i18n='Invoice List'>".$m2->nama."</span></a>
										";
										echo "<ul class='menu-content' >";

										$query = $db->query("SELECT
												*
												FROM
												r_usergroupmenu satu
												LEFT JOIN
												m_menu dua ON (satu.code_menu = dua.code) where dua.menu_rank = '3' and dua.menu_id_child = '$m2->code'  and satu.code_group = '$code_group_sess' and dua.status_cd = 'normal' order by dua.ordering asc");
										if ($query->getNumRows() > 0)
                						{
											$results = $query->getResult();
											foreach ($results as $m3)
											{
												if ($m3->have_sm == 1)
												{
													echo "<li><a href='javascript:;'><i class='bx bx-right-arrow-alt'></i><span class='menu-item' data-i18n='Error'>".$m3->nama."</span></a>
															<ul class='menu-content' style='padding-left:10px;'>";
													$query = $db->query("SELECT
																		*
																		FROM
																		r_usergroupmenu satu
																		LEFT JOIN
																		m_menu dua ON (satu.code_menu = dua.code) where dua.menu_rank = '4' and dua.menu_id_child = '$m3->code' and satu.code_group = '$code_group_sess' and dua.status_cd = 'normal' order by dua.ordering asc");
													if ($query->getNumRows() > 0)
													{
														$results = $query->getResult();
														foreach ($results as $m4)
														{
															echo "<li><a href='".base_url($m4->url)."'><i class='bx bx-right-arrow-alt'></i><span class='menu-item' data-i18n='Invoice List'>".$m4->nama."</span></a></li>";
														}
													}
													echo 	"</ul>";
													echo "</li>";
												}
												else
												{
													echo "<li>
															<a href='".base_url($m3->url)."'><i class='bx bx-right-arrow-alt'></i><span class='menu-item' data-i18n='Invoice List'>".$m3->nama."</span></a>
														</li>
													";
												}
											}
										}

										echo "</ul>";
										echo "</li>";
									}
									else{
										echo "<li>
												<a href='".base_url($m2->url)."'><i class='bx bx-right-arrow-alt'></i><span class='menu-item' data-i18n='Invoice List'>".$m2->nama."</span></a>
											</li>
										";
									}
								}
							}
							echo "</ul>";
							echo "</li>";
					   }
					   else
					   {
							echo "<li class=' nav-item'><a href='".base_url($m1->url)."'><i class='menu-livicon' data-icon='bank'></i><span class='menu-title' data-i18n='Invoice'>".$m1->nama."</span></a>
								
									</li>";
					   }
						
                    }
                }
                
			?>
		</ul>


            
        <!-- <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
            <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="notebook"></i><span class="menu-title" data-i18n="Invoice">Invoice</span></a>
                
            </li>

            <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="notebook"></i><span class="menu-title" data-i18n="Invoice">Invoice</span></a>
                <ul class="menu-content">
                    <li><a href="app-invoice-list.html"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">Invoice List</span></a>
                    </li>
                    <li><a href="app-invoice.html"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice">Invoice</span></a>
                    </li>
                </ul>
            </li>
        
            <li class=" nav-item"><a href="#"><i class="menu-livicon" data-icon="share"></i><span class="menu-title" data-i18n="Miscellaneous">Miscellaneous</span></a>
                <ul class="menu-content">
                    <li><a href="page-coming-soon.html" target="_blank"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Coming Soon">Coming Soon</span></a>
                    </li>
                    <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Error">Error</span></a>
                        <ul class="menu-content">
                            <li><a href="#"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Error">Error</span></a>
                                <ul class="menu-content" style='padding-left:10px;'>
                                    <li><a href="app-invoice-list.html"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">Invoice List</span></a>
                                    </li>
                                    <li><a href="app-invoice.html"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="Invoice">Invoice</span></a>
                                    </li>
                                </ul>
                            <li><a href="error-500.html" target="_blank"><i class="bx bx-right-arrow-alt"></i><span class="menu-item" data-i18n="500">500</span></a>
                            </li>
                        </ul>
                    </li>
               
                </ul>
            </li>
        </ul> -->
    </div>
</div>