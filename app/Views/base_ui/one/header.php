
<head>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="DCA Dev">
    <meta name="keywords" content="DCA">
    <meta name="author" content="PIXINVENT">
    <title>MOIM</title>
    <link rel="apple-touch-icon" href="<?php echo base_url();?>/public/tmpassets/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/public/tmpassets/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/css/extensions/swiper.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/pages/dashboard-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/css/pages/app-invoice.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/css/forms/select/select2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/sweetalert2/dist/sweetalert2.css">

    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    <!-- END: Custom CSS-->

    <script>
        var PathOrigin = window.location.origin + '/moim/'; 
    </script>

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/vendors.min.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>

    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />

    <!-- BEGIN Vendor JS-->

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/charts/apexcharts.min.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/extensions/swiper.min.js"></script>
    <!-- END: Page Vendor JS-->

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/configs/vertical-menu-light.js"></script>


    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/components.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/footer.js"></script>

    <script src="<?php echo base_url(); ?>/public/tmpassets/myapp-global/Dica_app.js"></script>
    <script src="<?php echo base_url(); ?>/public/tmpassets/myapp-global/app.js"></script>
    <script src="<?php echo base_url(); ?>/public/tmpassets/myapp-global/Var.js"></script>

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>

   <!--  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->

    <script src="<?php echo base_url(); ?>/public/tmpassets/app-assets/vendors/js/forms/select/select2.js"></script>

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-datetime-picker/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/mycustom/table/tablecst.css"/>
    <link type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/dataTables.checkboxes.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/JSZip-2.5.0/jszip.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/pdfmake-0.1.36/pdfmake.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/pdfmake-0.1.36/vfs_fonts.js"></script>
    
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/datatable_src/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/datatable_src/dataTables.bootstrap4.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/DataTables-1.10.20/js/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/AutoFill-2.3.4/js/dataTables.autoFill.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/AutoFill-2.3.4/js/autoFill.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/dataTables.buttons.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/buttons.bootstrap4.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/buttons.colVis.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/buttons.flash.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/buttons.html5.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Buttons-1.6.1/js/buttons.print.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/ColReorder-1.5.2/js/dataTables.colReorder.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/FixedColumns-3.3.0/js/dataTables.fixedColumns.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/FixedHeader-3.1.6/js/dataTables.fixedHeader.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/KeyTable-2.5.1/js/dataTables.keyTable.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Responsive-2.2.3/js/dataTables.responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/RowGroup-1.1.1/js/dataTables.rowGroup.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/RowReorder-1.2.6/js/dataTables.rowReorder.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Scroller-2.0.1/js/dataTables.scroller.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/Select-1.3.1/js/dataTables.select.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/datatables_full/dataTables.checkboxes.min.js"></script>

    <link href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/jqdatetimepicker/jquery.datetimepicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/jqdatetimepicker/jquery.datetimepicker.full.js"></script>


    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/ui/blockUI.min.js"></script>

    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/jquery-redirect/jquery.redirect.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/codemirror/lib/codemirror.css"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/codemirror/lib/codemirror.js"></script>
    <script src="<?php echo base_url();?>/public/tmpassets/app-assets/plugins/codemirror/addon/selection/active-line.js"></script>




    <script>
        $(document).click(function(e) {
            if( e.target.id != 'info' && $(e.target).attr('class') != 'dc' && $(e.target).attr('class') != undefined) {
                $('div[id^=subres_]').remove();
            }
        });
    </script>



    

</head>