<div id="tolee_" style='border:1px solid #DFE3E7;'>
	 <form class="form form-horizontal" id="form_main">
	 	<input type="hidden" id="code" value='<?php echo $code;?>' class="form-control" name="code" placeholder="">

	 	<table border='0px' width='100%' style='margin-top:20px;'>
	 		<tr>
	 			<td width='25%' style='padding:10px;text-align:right;margin-top:10px;padding:18px;' valign='top'>
 					<label>Username</label>
	 			</td>
	 			<td style='padding:10px;'>
	 				<div class='row'>
	 					<div class='col-md-8'>
	 						<input type="text" id="username" value='<?php echo $username;?>' class="form-control" name="username" placeholder="">
	 					</div>
	 				</div>
	 				
	 			</td>
	 		</tr>
	 		<tr>
	 			<td width='25%' style='padding:10px;text-align:right;padding:18px;' valign='top'>
 					<label>Password</label>
	 			</td>
	 			<td style='padding:10px;'>
	 				<div class='row'>
	 					<div class='col-md-8'>
	 						<input type="password" id="password" value='<?php echo $password;?>' class="form-control" name="password"  placeholder="">
	 					</div>
	 				</div>
	 				
	 			</td>
	 		</tr>
	 		<tr>
	 			<td width='25%' style='padding:10px;text-align:right;padding:18px;' valign='top'>
 					<label>Nama</label>
	 			</td>
	 			<td style='padding:10px;'>
	 				<div class='row'>
	 					<div class='col-md-8'>
	 						<input type="text" id="nama" value='<?php echo $name;?>' class="form-control" name="nama" placeholder="">
	 					</div>
	 				</div>
	 				
	 			</td>
	 		</tr>
	 		
	 		<tr>
	 			<td width='25%' style='padding:10px;text-align:right;padding:18px;' valign='top'>
 					<label>Group User</label>
	 			</td>
	 			<td style='padding:10px;'>

	 				<div class='row'>
	 					<div class='col-md-8'>
	 						<select class="select2 form-control" id="selectselect" name='group_user' id='group_user' >
									<option value=""></option>
									<?php
										if(!empty($list_select_daftar_group))
										{
											foreach($list_select_daftar_group as $rows_supp):
												if ($code_group == $rows_supp->code)
												{
													$selc = "selected";
												}
												else
												{
													$selc = "";
												}
										?>
										   <option value="<?php echo $rows_supp->code;?>" <?php echo $selc;?> >
										   		<?php echo $rows_supp->desc;?>
								   			</option>
									   <?php
											endforeach;
										}
									?>
							</select>							
	 					</div>
	 				</div>
	 				
	 			</td>
	 		</tr>
	 		
	 		<tr>
	 			<td colspan='2' align='center' style='padding:10px;'>
                    <input type="button" id="submit_form_main" class="btn btn-sm btn-primary mr-1 mb-1" value='Simpan'>
                    <input type="button" id="batal_form_main" class="btn btn-sm btn-light-secondary mr-1 mb-1" value='Batal'>
	 			</td>
	 		</tr>
	 	</table>

    </form>
</div>	

