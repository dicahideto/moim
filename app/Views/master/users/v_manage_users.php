

<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Manajemen User</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Users</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class='col-md-12'>
                                <button type="button" id="tambah_user" class="btn btn-sm btn-info mr-1 mb-1">Tambah User</button>
                            </div>
                            <div class="col-md-8">
                               
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row ">
                                    <div class="col-lg-2 col-3" style="text-align:right;">
                                        <label class="col-form-label">Filter</label>
                                    </div>
                                    <div class="col-lg-10 col-9">
                                        <input type="text" id="qfilter" class="form-control" name="qfilter" placeholder="Username/ Nama">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table border='0px' width='100%'>
                            <tr>
                                <td>
                                    <div id='form_edit_' style=''></div>
                                </td>
                            </tr>
                        </table>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="t_list_user" class="table_cst" style="width:100%">
                                    <thead style="background-color:#81DAF5">
                                        <tr>
                                            <th class='bpad7 bleft btop bbottom' width='50px' style='text-align:center;'>
                                                No
                                            </th>
                                            <th class='bpad7 bleft btop bbottom' style='text-align:left;' width='18%'>
                                                Username
                                            </th>
                                            <th class='bpad7 bleft btop bbottom' style='text-align:left;'>
                                                Nama
                                            </th>
                                            <th class='bpad7 bleft btop bbottom' style='text-align:left;' width='12%' style='text-align:center;'>
                                                Group
                                            </th>
                                            <th class='bpad7 bleft btop bright bbottom' width='10%' style='text-align:center;'>
                                                Status Aktif
                                            </th>
                                            <th class='bpad7 bleft btop bright bbottom' width='14%' style='text-align:center;'>
                                                #
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>



<script src="<?php echo base_url(); ?>/public/tmpassets/app/master/users/Manage_users.js"></script>
<script type="text/javascript">
    Manage_users.init();
</script>
