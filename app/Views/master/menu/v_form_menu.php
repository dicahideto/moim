<div id='tolee_'>
<?php

$position_id = '';
$menu_id = array();
if(!empty($dataRole)){
	foreach ($dataRole as $dr){
		$position_id 		= $dr->code_group;
		$menu_id[] 			= $dr->code_menu;
	}
}
?>			
<form class="kt-form validate" id="form_amenu" >

    <div style="border:solid #eeeedd 1px;padding:20px;">
        <div align="center">
        </div>
        
        <input type="hidden" name="code" id="code" value="<?php echo $namid;?>">
        
        <ul style="list-style-type: none;">
        <?php 
        foreach($parent_menu as $pm)
        {
            if(in_array($pm->code, $menu_id))
            {
                echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$pm->code.'" checked="checked"> <b>'.ucwords(strtolower($pm->nama)).'</b></label>';
            }
            else
            {
                echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$pm->code.'" > <b>'.ucwords(strtolower($pm->nama)).'</b></label>';
            }
            
            echo '<ul style="list-style-type: none; margin-left:20px;">';
            foreach($sub_menu as $spm)
            {
                if($pm->code == $spm->menu_id_child)
                {
                    if(in_array($spm->code, $menu_id))
                    {
                        echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm->code.'" checked="checked"> '.ucwords(strtolower($spm->nama)).'</label></li>';
                    }
                    else
                    {
                        echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm->code.'"> '.ucwords(strtolower($spm->nama)).'</label></li>';
                    }
                }
                
                
                echo '<ul style="list-style-type: none; margin-left:20px;">';
                foreach($sub_menu2 as $spm2)
                {
                    if($spm->code == $spm2->menu_id_child && $pm->code == $spm->menu_id)
                    {
                        if(in_array($spm2->code, $menu_id))
                        {
                            echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm2->code.'" checked="checked"> '.ucwords(strtolower($spm2->nama)).'</label></li>';
                        }
                        else
                        {
                            echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm2->code.'"> '.ucwords(strtolower($spm2->nama)).'</label></li>';
                        }
                    }
                    
                        echo '<ul style="list-style-type: none; margin-left:20px;">';
                        foreach($sub_menu3 as $spm3)
                        {
                            if($pm->code == $spm->menu_id && $spm2->code == $spm3->menu_id_child && $spm->code == $spm2->menu_id_child && $pm->code == $spm->menu_id_child)
                            
                            {
                                if(in_array($spm3->code, $menu_id))
                                {
                                    echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm3->code.'" checked="checked"> '.ucwords(strtolower($spm3->nama)).'</label></li>';
                                }
                                else
                                {
                                    echo '<li><label><input type="checkbox" class="icheck menu" name="menu[]" value="'.$spm3->code.'"> '.ucwords(strtolower($spm3->nama)).'</label></li>';
                                }
                            }
                        }
                        echo '</ul>';
                }
                
                echo '</ul>';
            }
            echo '</ul>';
            
        }
        ?>
        </ul>

        <div align='center'>
            <input type="button" id="submit_form_amenu" class="btn btn-sm btn-primary" value='Simpan'>
            <input type="button" class="btn btn-sm btn-secondary" id='goback' value='Cancel'>
        </div>
    </div>
    
   
    </form>

</div>