<?php

$position_id = '';
$menu_id = array();
if(!empty($dataRole)){
	foreach ($dataRole as $dr){
		$position_id 		= $dr->code_group;
		$menu_id[] 			= $dr->code_menu;
	}
}
?>


<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Akses Menu</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Grup per Menu</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class='col-md-12'>
                            <div style='text-align:center;'>
                               
                            </div>
                        </div>

                        <table class="" id="t_daftar_group" style="width:100%">
                            <thead>
                                <tr>
                                    <th class='bpad7 bleft bright btop bbottom' width='3%' style='text-align:center;'>No</th>
                                    <th class='bpad7 btop bbottom' style='text-align:center;' >Grup</th>
                                    <th class='bpad7 btop bbottom bright bleft' style='text-align:center;' width='10%'>Urutan Approval</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>

		
<script src="<?php echo base_url(); ?>/public/tmpassets/app/master/menu/Manage_menu.js"></script>
<script type="text/javascript">
    Manage_menu.init();
</script>











