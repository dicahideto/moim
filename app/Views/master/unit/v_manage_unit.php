

<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Unit</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Daftar Unit</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class='col-md-12'>
                                <button type="button" id="tambah_user" class="btn btn-sm btn-info mr-1 mb-1">Tambah Unit</button>
                            </div>
                            <div class="col-md-8">
                               
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row ">
                                    <div class="col-lg-2 col-3" style="text-align:right;">
                                        <label class="col-form-label">Filter</label>
                                    </div>
                                    <div class="col-lg-10 col-9">
                                        <input type="text" id="qfilter" class="form-control" name="qfilter" placeholder="Unit">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table border='0px' width='100%'>
                            <tr>
                                <td>
                                    <div id='form_edit_' style=''></div>
                                </td>
                            </tr>
                        </table>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="t_list_user" class="table_cst" style="width:100%">
                                    <thead style="background-color:#81DAF5">
                                        <tr>
                                            <th class='bpad7 bleft btop bbottom' width='50px' style='text-align:center;'>
                                                No
                                            </th>
                                            <th class='bpad7 bleft btop bbottom' style='text-align:left;' >
                                                Unit
                                            </th>
                                            <th class='bpad7 bleft btop bbottom' style='text-align:center;' width='70px'>
                                                #
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>



<script src="<?php echo base_url(); ?>/public/tmpassets/app/master/unit/Manage_unit.js"></script>
<script type="text/javascript">
    Manage_unit.init();
</script>
