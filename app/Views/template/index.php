<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <?= $this->include('template/headersrc') ?>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- BEGIN: Header-->
    <?= $this->include('template/header') ?>
    <!-- END: Header-->

    <!-- BEGIN: Main Menu-->
    <?= $this->include('template/sidebar') ?>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                
                <?= $this->renderSection('content') ?>    
                <!-- <section id="dashboard-ecommerce">
                    
                </section> -->
            </div>
        </div>
    </div>
    <!-- END: Content-->

   
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <?= $this->include('template/footer') ?>
    <!-- END: Footer-->

    <!-- BEGIN: Vendor JS-->
    <?= $this->include('template/footersrc') ?>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>