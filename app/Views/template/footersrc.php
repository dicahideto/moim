<script>
    var PathOrigin = window.location.origin + '/evis/'; 
</script>

<script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/vendors.min.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/vendors/js/extensions/swiper.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/core/app-menu.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/core/app.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/components.js"></script>
<script src="<?php echo base_url();?>/public/tmpassets/app-assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
