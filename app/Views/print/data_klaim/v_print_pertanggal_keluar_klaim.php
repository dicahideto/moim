<?php 
$db = db_connect();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- <link rel="shortcut icon" href="https://ecs7.tokopedia.net/img/favicon.ico?v=20140313"> -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>/public/tmpassets/app-assets/images/ico/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Data Klaim <?php echo $date_show;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <link href="https://cdn.tokopedia.net/built/b5d55ee8f475d30d61aec209a6af1587.css" rel="stylesheet"></link>
    
    <style>
    
        @media print {
            .hidden-print {
                display: none !important;
            }
        }
    </style>
</head>

<body style="font-family: &quot;open sans&quot;, tahoma, sans-serif; margin: 0px; -webkit-print-color-adjust: exact;">


    <table style="padding:10px;" width='100%' border='0px' cellspacing='0px'> 
        <tbody>
            <tr style="margin-top: 8px; margin-bottom: 8px;">
                <td style="padding:5px;font-weight:bold;font-size:12px;" valign='top'></td>
                <td style=" ">
                    
                </td>
                    <td style="padding:5px; padding-left: 10px; text-align: right; width: 200px;" class="hidden-print" valign='top'>
                        <strong>
                            <a style="color:green;text-decoration:none;" href="javascript:window.print()">
                                <span style="color:#42B549;text-decoration:none;font-size:11px;">
                                    Cetak Browser
                                </span>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <!-- <a style="color:green;text-decoration:none;" href="<?php echo base_url();?>/Transaction/Klaim/Manage_klaim/excel_data_klaim?dateout=<?php echo $dateout;?>&step=<?php echo $step;?> " target="_blank"> -->
                            <a style="color:green;text-decoration:none;cursor:pointer;" onclick='unduhExcel();'>
                                <span style="color:#42B549;text-decoration:none;font-size:11px;">
                                     Export Excel 
                                </span>
                            </a>
                        </strong>
                    </td>
            </tr>
        </tbody>
    </table>

    <div id='table_main'>
    <div style='padding:5px;font-weight:bold;font-size:12px;margin-left:10px;'>
        Daftar Data Klaim<br /><?php echo $date_show;?>
    </div>

    <br />
    <br />
    <div style='padding:10px;'>
         <table class="" id="" style="font-size:9px;"  cellspacing='0' width='2700px'>
            <thead>
                <tr>
                    <th class='bpad7  bright btop bbottom' style='text-align:center;border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='50px'>No</th>
                    <th class='bpad7  bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='80px'>No. RM</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px' >Nama Pasien</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px'>Jaminan</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px'>Unit/ Dept</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px'>Ruang/ Bed</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='120px'>Tgl Masuk</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='120px'>Tgl Keluar</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='280px'>DPJP</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px'>Eval</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='250px'>Keterangan</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' >Progress</th>
                    <th class='bpad7 bright btop bbottom' style='border-left:1px solid #bbb;border-right:1px solid #bbb;border-top:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' width='200px'>Dibuat/ Approval</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                    if(!empty($list_data)){
                        foreach ($list_data as $d)
                        {
                        $i++
                        ?>
                        <tr>
                            <td style='text-align:center;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $i;?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->ext_id;?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->person_nm; ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->jaminan; ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->org; ?>
                           </td >
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->ruang; ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo tgl_indo_miring_time($d->tgl_masuk); ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo tgl_indo_miring_time($d->tgl_keluar); ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->dpjp; ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->kelengkapan; ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php echo $d->keterangan; ?>
                           </td>
                           <td style='text-align:center;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php

                                $sqk = $db->query("select 
                                                    `desc`
                                                    from
                                                    m_group
                                                    where 
                                                    step = '".$d->progress."' and status_cd = 'normal' ");

                                    $row = $sqk->getRow();
                                    if (isset($row))
                                    {
                                       echo $prog = $row->desc;
                                    }
                                    else
                                    {

                                    }

                                    if ($d->progress == '100')
                                    {
                                        echo "SELESAI";
                                    }
                                ?>
                           </td>
                           <td style='text-align:left;border-left:1px solid #bbb;border-right:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                                <?php

                                    if ($d->progress == 1)
                                    {
                                        $us = $d->code_user_process_zero;
                                    }
                                    else if ($d->progress == 2)
                                    {
                                        $us = $d->code_user_process_one;
                                    }
                                    else if ($d->progress == 3)
                                    {
                                        $us = $d->code_user_process_two;
                                    }
                                    else if ($d->progress == 4)
                                    {
                                        $us = $d->code_user_process_three;
                                    }
                                    else if ($d->progress == 5)
                                    {
                                        $us = $d->code_user_process_four;
                                    }
                                    else if ($d->progress == 100)
                                    {
                                        $us = $d->code_user_process_five;
                                    }

                                    $sq = $db->query("select 
                                                            name
                                                        from
                                                            m_user
                                                        where 
                                                            `code` = '".$us."' 
                                                    "
                                                    );

                                    $row = $sq->getRow();
                                    if (isset($row))
                                    {
                                       echo $row->name;
                                       // echo "<br />";
                                       
                                    }
                                    else
                                    {

                                    }
                                ?>
                           </td>
                            </tr>
                       <?php    
                        }
                    }
                ?>
            </tbody>
        </table>
    </div>

    </div>

</body>
</html>


<script>
    function unduhExcel() {
        // var a = moment(jQuery('#h_dttm').val());
        // var aa = moment(jQuery('#h_dttm').val()).format('DD-MM-YYYY');
        // var b = moment(jQuery('#h_dttm2').val());
        // var bb = moment(jQuery('#h_dttm2').val()).format('DD-MM-YYYY');

        // if(b.diff(a,'days')>=31){
        //     alert('Periode penarikan data terlalu panjang..!');
        //     return;
        // } else {
            
            // var datea = new Date(jQuery('#h_dttm').val());
            // var newdatea = datea.toString('dd-MM-yy');


            // var dateb = new Date(jQuery('#h_dttm2').val());
            // var newdateb = datea.toString('dd-MM-yy');


            var htmls = document.getElementById('table_main').innerHTML;
            var uri = 'data:application/vnd.ms-excel;base64,';
            var template = `<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><style> .str{ mso-number-format:\@; } </style><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body class='str'><table class='str'>{table}</table></body></html>`;
            var base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            };

            var format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            };

            var ctx = {
                worksheet : 'Worksheet',
                table : htmls
            }

            var link = document.createElement('a');
            link.download = 'Data Klaim <?php echo $dateout;?>.xls';
            link.href = uri + base64(format(template, ctx));
            link.click();


        // }
    }
</script>   