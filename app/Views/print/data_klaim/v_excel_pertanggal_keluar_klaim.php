<?php
$db = db_connect();
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Klaim - $date_show.xls");

?>

<style> .str{ mso-number-format:\@; } </style>

<table >
    <tr>
        <td colspan='17' align='center'>
            Daftar Data Klaim
        </td>
    </tr>
    <tr>
        <td colspan='17' align='center'>
            <?php echo $date_show;?>
        </td>
    </tr>
    <tr>
        <td colspan='17' align='center'>
            
        </td>
    </tr>
</table>
<table border='1'>
    <tr>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
            No
        </td> 
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
            No. RM
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
            Nama Pasien
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
            Jaminan
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
            Unit/ Dept
        </td>  
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Ruang/ Bed
        </td> 
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Tgl Masuk
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Tgl Keluar
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Tgl Diterima RM
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           DPJP
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Eval
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Keterangan
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Progress
        </td>
        <td align='center' valign='top' style='background-color:#bbb;font-weight:bold;'>
           Dibuat/ Approval
        </td>
    </tr>


<?php
    $i = 0;
    if(!empty($list_data)){
        foreach ($list_data as $d){
        $i++
        ?>
            <tr>
                <td style='text-align:center;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $i;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->ext_id;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->person_nm;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->jaminan;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->org;?>
               </td >
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->ruang;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo tgl_indo_miring_time($d->tgl_masuk);?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo tgl_indo_miring_time($d->tgl_keluar);?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo tgl_indo_miring_time($d->tgl_diterima_rm);?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;' valign='top'>
                    <?php echo $d->dpjp;?>
               </td>
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;border-right:1px solid #bbb;' valign='top'>
                    <?php echo $d->kelengkapan;?>
               </td>    
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;border-right:1px solid #bbb;' valign='top'>
                    <?php echo $d->keterangan;?>
               </td>    
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;border-right:1px solid #bbb;' valign='top'>
                    <?php
                      $sqk = $db->query("select 
                                          `desc`
                                          from
                                          m_group
                                          where step = '".$d->progress."' and status_cd = 'normal' ");

                          $row = $sqk->getRow();
                          if (isset($row))
                          {
                             echo $prog = $row->desc;
                          }
                          else
                          {

                          }
                      ?>
               </td>    
               <td style='text-align:left;border-left:1px solid #bbb;border-bottom:1px solid #bbb;padding:3px;border-right:1px solid #bbb;' valign='top'>
                   <?php

                                    if ($d->progress == 1)
                                    {
                                        $us = $d->code_user_process_zero;
                                    }
                                    else if ($d->progress == 2)
                                    {
                                        $us = $d->code_user_process_one;
                                    }
                                    else if ($d->progress == 3)
                                    {
                                        $us = $d->code_user_process_two;
                                    }
                                    else if ($d->progress == 4)
                                    {
                                        $us = $d->code_user_process_three;
                                    }
                                    else if ($d->progress == 5)
                                    {
                                        $us = $d->code_user_process_four;
                                    }
                                    else if ($d->progress == 100)
                                    {
                                        $us = $d->code_user_process_five;
                                    }


                                    $sq = $db->query("select 
                                                    name
                                                    from
                                                    m_user
                                                    where `code` = '".$us."' ");

                                    $row = $sq->getRow();
                                    if (isset($row))
                                    {
                                       echo $row->name;
                                    }
                                    else
                                    {

                                    }

                                ?>
               </td>    
            </tr>
       <?php    
        }
    }
?>

</table>

