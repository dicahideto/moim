<div class="content-header row">
    <div class="content-header-left col-12 mb-2 mt-1">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h5 class="content-header-title float-left pr-1 mb-0">Manajemen Klaim</h5>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
	<div class="container mt-3">
		<?php
		if(session()->getFlashdata('message')){
		?>
			<div class="alert alert-info">
				<?= session()->getFlashdata('message') ?>
			</div>
		<?php
		}
		?>
		<!-- <form method="post" action="<?php echo base_url();?>/Dev/Import_excel/prosesExcelSpread" enctype="multipart/form-data"> -->
		<form id="form_main" enctype="multipart/form-data">
			<div class="form-group">
				<label>File Excel</label>
				<input type="file" name="file" class="form-control" id="file" required accept=".xls, .xlsx" /></p>
			</div>
			<div class="form-group">
				<input type='button' id='but_upload' class="btn btn-primary" value='Upload'>
			</div>
		</form>
		
	</div>

</div>


<script src="<?php echo base_url(); ?>/public/tmpassets/app/dev/Import_excel.js"></script>
<script type="text/javascript">
    Import_excel.init();
</script>
