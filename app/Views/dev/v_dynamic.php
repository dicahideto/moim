
<style>
  .CodeMirror { border: 1px solid silver; }
  .CodeMirror-empty { outline: 1px solid #c22; }
  .CodeMirror-empty.CodeMirror-focused { outline: none; }
  .CodeMirror pre.CodeMirror-placeholder { color: #999; }
</style>
    
<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Dynamic Table</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> </h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                       <form class="form form-horizontal" id="form_main">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Query Main</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <textarea id="code" name="code"></textarea>
                                    </div>
                                    
                                    <div class="col-sm-12 d-flex justify-content-end">
                                        <button type="button" id="submit_form_main" class="btn btn-primary mr-1 mb-1">Submit</button>
                                        <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                    </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-4">
                                        <label>List Table</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select class="select2 form-control" id="selectselect" name='selectselect' >
                                          <option ></option>
                                          <option value="square">Square</option>
                                          <option value="rectangle">Rectangle</option>
                                          <option value="rombo">Rombo</option>
                                          <option value="romboid">Romboid</option>
                                          <option value="trapeze">Trapeze</option>
                                          <option value="traible">Triangle</option>
                                          <option value="polygon">Polygon</option>
                                        </select>
                                    </div>
                                </div>


                                

                            </div>
                        </form>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>

<script src="<?php echo base_url(); ?>/public/tmpassets/app/dev/Dynamic.js"></script>
<script type="text/javascript">
    Dynamic.init();
</script>
    <script>
var nonEmpty = false;
var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
  mode: "application/xml",
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true
});

function toggleSelProp() {
  nonEmpty = !nonEmpty;
  editor.setOption("styleActiveLine", {nonEmpty: nonEmpty});
  var label = nonEmpty ? 'Disable nonEmpty option' : 'Enable nonEmpty option';
  document.getElementById('toggleButton').innerText = label;
}
</script>