
<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Form Layouts</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Horizontal Form</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <form class="form form-horizontal" id="form_main">
                            <div class="form-body">
                                <div class="row">
                                    
                                    <div class="col-md-4">
                                        <label>First Name</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="first-name" class="form-control" name="fname" id="fname" placeholder="First Name" >
                                    </div>
                                    <div class="col-md-4">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="email" id="email-id" class="form-control" name="email-id" placeholder="Email">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Mobile</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="number" id="contact-info" class="form-control" name="contact" placeholder="Mobile">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Password</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Select 2</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select class="select2 form-control" id="selectselect" name='selectselect' >
                                            <option ></option>
                                            <option value="square">Square</option>
                                            <option value="rectangle">Rectangle</option>
                                            <option value="rombo">Rombo</option>
                                            <option value="romboid">Romboid</option>
                                            <option value="trapeze">Trapeze</option>
                                            <option value="traible">Triangle</option>
                                            <option value="polygon">Polygon</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Radio2</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <div class='radiocheck_span'>
                                            <label style='padding-right:20px;'><input type='radio' name='Colo2r' value='Red' /> Red</label>
                                            <label style='padding-right:20px;'><input type='radio' name='Colo2r' value='Green' /> Green</label>
                                            <label style='padding-right:20px;'><input type='radio' name='Colo2r' value='Blue' /> Blue</label>
                                        </div>

                                    </div>
                                    
                                    <div class="col-md-4">
                                        <label>Checkbox</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <div class='radiocheck_span'>
                                            <input type="checkbox" name="agree" class="checkbox-input" id="defaultCheck1" >
                                            <label for="defaultCheck1">Touch me!</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Select 2 Loadmore</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <select class="select2 form-control" id="select2loadmore" name='select2loadmore' >
                                            <option ></option>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Select Pagination</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" class="form-control" placeholder="" id="qemp"/>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Select Pagination Sample II</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" class="form-control" placeholder="" id="qemp2"/>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Datepicker</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                   
                                            <div class="input-group">
                                                <input type="text" class="form-control"  aria-describedby="basic-addon2" id="kt_datepicker" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">Date</span>
                                                </div>
                                            </div>
                                    </div>
                                   

                                    <div class="col-md-4">
                                        <label>Date Time</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                   
                                            <div class="input-group">
                                                <input type="text" class="form-control" aria-describedby="basic-addon2" id="kt_datetimepicker" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">Datetime</span>
                                                </div>
                                            </div>
                                    </div>
                                   
                                    <div class="col-md-4">
                                        <label>Date Time</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="my-datepicker" class="datepicker">  
                                        <div id="cal2">dsfasefdsfds</div>
                                    </div>
                                   

                                   
                                    <div class="col-md-4">
                                        <label>JQ Date Time</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="jqdatetimepicker" class="form-control">  
                                        
                                    </div>
                                   

                                    
                                    
                                    <div class="col-sm-12 d-flex justify-content-end">
                                        <button type="button" id="submit_form_main" class="btn btn-primary mr-1 mb-1">Submit</button>
                                        <button type="reset" class="btn btn-light-secondary mr-1 mb-1">Reset</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>



<script src="<?php echo base_url(); ?>/public/tmpassets/app/dev/Form.js"></script>
<script type="text/javascript">
    Form.init();
</script>
