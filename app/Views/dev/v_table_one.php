

<div class="content-header row">
                <div class="content-header-left col-12 mb-2 mt-1">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h5 class="content-header-title float-left pr-1 mb-0">Form Layouts</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


<section id="basic-horizontal-layouts">
    <div class="row match-height">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Horizontal Form</h4>
                </div>

                <div class="card-content">
                    <div class="card-body">
                      <!--   <table id="table_daftar_item_fp" class="table table-striped table-bordered compact" style="width:100%">
                            <thead style="background-color:#81DAF5">
                                <tr >
                                    <th>No</th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                </tr>
                            </thead>
                        </table> -->
                                                
                        <div class="row">
                            <div class="col-md-12">
                                <table id="tes" class="table table-striped table-bordered compact" style="width:100%">
                                    <thead style="background-color:#81DAF5">
                                        <tr >
                                            <th>No</th>
                                            <th>Kode</th>
                                            <th>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                               
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row ">
                                    <div class="col-lg-2 col-3" style="text-align:right;">
                                        <label class="col-form-label">Filter</label>
                                    </div>
                                    <div class="col-lg-10 col-9">
                                        <input type="text" id="qfilter" class="form-control" name="fname" placeholder="Unit">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <table id="tese" class="table_cst" style="width:100%">
                                    <thead style="background-color:#81DAF5">
                                        <tr>
                                            <th class='bpad7 bleft btop bbottom' width='70px'>No</th>
                                            <th class='bpad7 bleft btop bbottom' >Kode</th>
                                            <th class='bpad7 bleft btop bright bbottom' width='30%'>Nama</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>



<script src="<?php echo base_url(); ?>/public/tmpassets/app/dev/Table.js"></script>
<script type="text/javascript">
    Table.init();
</script>
