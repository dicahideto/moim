<?php
$session = session();
$errors = $session->getFlashdata('errors');

?>
<div class="limiter">
    <div class="container-login100" style="background-image: url('<?php echo base_url();?>/public/tmpassets/login/images/blue-1273089_1280.webp');">
        <div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
        


            <form class="login100-form validate-form flex-sb flex-w" action='<?php base_url();?>signin/process_checking_login' method='post'>
                <?= csrf_field(); ?>
                <span class="login100-form-title p-b-53">
                    MOIM<br />                                   
                    <span style='font-size:0.6em;'>Monitoring Klaim</span>
                    <?php if($errors != null): ?>
                        <div class="alert alert-danger" role="alert" style='margin-bottom:-30px;margin-top:20px;'>
                            <p class="mb-0">
                                <?php
                                    echo $errors.'<br>';
                                ?>
                            </p>
                        </div>
                    <?php endif ?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
                </span>
                
                <div class="p-t-31 p-b-9">
                    <span class="txt1">
                        Username
                    </span>
                </div>
                <div class="wrap-input100 validate-input" data-validate = "Username is required">
                    <input class="input100" type="text" name="username" >
                    <span class="focus-input100"></span>
                </div>
                
                <div class="p-t-13 p-b-9">
                    <span class="txt1">
                        Password
                    </span>
                </div>
                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                    <input class="input100" type="password" name="password" >
                    <span class="focus-input100"></span>
                </div>

                <div class="container-login100-form-btn m-t-17">
                    <button class="login100-form-btn">
                        Sign In
                    </button>
                </div>

                <div class="w-full text-center p-t-55">
                    <span class="txt2">
                        Prototype &#169; DCA 
                    </span>

                    <a href="#" class="txt2 bo1">
                       
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="dropDownSelect1"></div>