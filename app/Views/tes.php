

<?php

date_default_timezone_set("Asia/Jakarta");
        
$month_var      = date('m');
$year_var       = date('Y');

$gb_month = bulan_indo_to_alpha($month_var);

?>
<div class="content-body">

<section id="widgets-Statistics">
    <div class="row">
        <div class="col-12 mt-1 mb-2">
            <h4>Berkas Monitoring Klaim</h4>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
                              
            <div class="card widget-todo">
                <div class="card-header border-bottom d-flex justify-content-between align-items-center flex-wrap">
                    <h4 class="card-title d-flex mb-25 mb-sm-0">
                        <i class='bx bx-check font-medium-5 pl-25 pr-75'></i>Berkas Klaim
                    </h4>
                    <ul class="list-inline d-flex mb-25 mb-sm-0">
                        <li class="d-flex align-items-center">
                            <i class='bx bx-sort mr-50 font-medium-3'></i>
                            <div class="dropdown">
                                <div class="dropdown-toggle cursor-pointer" role="button" id="periode_sess" aria-haspopup="true" aria-expanded="true" data-toggle="modal" data-target="#default"><?php echo $gb_month.' '.$year_var;?></div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="card-body px-0 py-1">
                    <div class="card-body">
                        <div id='result_monitoring'></div>
                    </div>

                </div>
            </div>
        

    </div>
    </div>


    <div class='row'>
        <!--
        <div class="row" style='padding:20px;'>
                        <div class="col-xl-2 col-md-4 col-sm-6" >
                            <div class="card text-center card_berkas" style='cursor:pointer;background-color:#bbb;' data-id='1'>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-info mx-auto my-1">
                                            1
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis"><?php echo $desc_one;?></p>
                                        <h2 class="mb-0"><?php echo $ct_one;?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-4 col-sm-6">
                            <div class="card text-center card_berkas" style='cursor:pointer;' data-id='2'>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-warning mx-auto my-1">
                                            2
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis"><?php echo $desc_two;?></p>
                                        <h2 class="mb-0"><?php echo $ct_two;?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-4 col-sm-6">
                            <div class="card text-center card_berkas" style='cursor:pointer;' data-id='3'>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto my-1">
                                           3
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis"><?php echo $desc_three;?></p>
                                        <h2 class="mb-0"><?php echo $ct_three;?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-4 col-sm-6">
                            <div class="card text-center card_berkas" style='cursor:pointer;' data-id='4'>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-primary mx-auto my-1">
                                           4
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis"><?php echo $desc_four;?></p>
                                        <h2 class="mb-0"><?php echo $ct_four;?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-4 col-sm-6">
                            <div class="card text-center card_berkas" style='cursor:pointer;' data-id='5'>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto my-1">
                                           5
                                        </div>
                                        <p class="text-muted mb-0 line-ellipsis"><?php echo $desc_five;?></p>
                                        <h2 class="mb-0"><?php echo $ct_five;?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
-->

    </div>
    

    
        <div id='data_main'></div> 
    </div>
</section>

</div>
</div>


<div class="modal fade text-left" id="default" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel1">Periode</h3>
                <button type="button" class="close rounded-pill" data-dismiss="modal" aria-label="Close">
                    <i class="bx bx-x"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <select class="select21 form-control" id="bulan" name='selectselect' placeholder='Bulan'>
                            <option ></option>

                            <option value="01" <?php if($month_var == '1'){ echo ' selected="selected"'; } ?> >Januari</option>
                            <option value="02" <?php if($month_var == '2'){ echo ' selected="selected"'; } ?> >Februari</option>
                            <option value="03" <?php if($month_var == '3'){ echo ' selected="selected"'; } ?> >Maret</option>
                            <option value="04" <?php if($month_var == '4'){ echo ' selected="selected"'; } ?> >April</option>
                            <option value="05" <?php if($month_var == '5'){ echo ' selected="selected"'; } ?> >Mei</option>
                            <option value="06" <?php if($month_var == '6'){ echo ' selected="selected"'; } ?> >Juni</option>
                            <option value="07" <?php if($month_var == '7'){ echo ' selected="selected"'; } ?> >Juli</option>
                            <option value="08" <?php if($month_var == '8'){ echo ' selected="selected"'; } ?> >Agustus</option>
                            <option value="09" <?php if($month_var == '9'){ echo ' selected="selected"'; } ?> >September</option>
                            <option value="10" <?php if($month_var == '10'){ echo ' selected="selected"'; } ?> >Oktober</option>
                            <option value="11" <?php if($month_var == '11'){ echo ' selected="selected"'; } ?> >November</option>
                            <option value="12" <?php if($month_var == '12'){ echo ' selected="selected"'; } ?> >Desember</option>
                        </select>
                    </div>
                </div>

                <br />
                <div class='row'>
                    <div class='col-md-6'>
                       <select class="select22 form-control" id="tahun" name='selectselect' placeholder='Tahun'>
                            <option ></option>
                            <option value="2020" <?php if($year_var == '2020'){ echo ' selected="selected"'; } ?> >2020</option>
                            <option value="2021" <?php if($year_var == '2021'){ echo ' selected="selected"'; } ?> >2021</option>
                            <option value="2022" <?php if($year_var == '2022'){ echo ' selected="selected"'; } ?> >2022</option>
                            <option value="2023" <?php if($year_var == '2023'){ echo ' selected="selected"'; } ?> >2023</option>
                            <option value="2024" <?php if($year_var == '2024'){ echo ' selected="selected"'; } ?> >2024</option>
                        </select>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-secondary" data-dismiss="modal">
                    <i class="bx bx-x d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Batal</span>
                </button>
                <button type="button" class="btn btn-primary ml-1" data-dismiss="modal" id='change_periode'>
                    <i class="bx bx-check d-block d-sm-none"></i>
                    <span class="d-none d-sm-block">Ubah Periode</span>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>/public/tmpassets/app/home/Home.js?v=1"></script>
<script type="text/javascript">
    Home.init();
</script>
