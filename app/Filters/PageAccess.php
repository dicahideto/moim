<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class PageAccess implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        // Do something here
        if(session()->has('isLoggedIn'))
        {
        	$db = db_connect();

            $tes = "SELECT * FROM m_user limit 1";
            $result_tes = $db->query($tes);
            $row   = $result_tes->getRow();
            echo $row->username;

        }
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}