<?php	namespace App\Models;
use CodeIgniter\Model;

class Model_crud extends Model
{
	public function m_iud($type, $data, $table, $table_id ,$primarykey, $message) {
		$err = '';
		if ($type == 'delete'){
			try
			{
				$query = $this->db->table($table)->delete(array($table_id => $primarykey));
			}
			catch (\Exception $e)
			{
				$err = $e->getMessage();
			}
			
		}
		if ($type == 'update'){
			try
			{
				$query = $this->db->table($table)->update($data, array($table_id => $primarykey));
			}
			catch (\Exception $e)
			{
				$err = $e->getMessage();
			}
			
		}
		if ($type == 'insert'){
			try
			{
				$query = $this->db->table($table)->insert($data);
			}
			catch (\Exception $e)
			{
				$err = $e->getMessage();
			}
		}
		
		if($err){
			$resp['error'] 		= 'error';
			$resp['message']	= $err;
			$resp['status']		= "error";
			
		}
		else{
			$resp['error'] 		= '';
			$resp['status'] 	= 'success';
			$resp['id'] 		= $this->db->insertID();
			$resp['message'] 	= $message;
				
		}
		
		return $resp ;
	}
	
	public function getNo($nomor, $panjang) {
		// $q = $this->db->query("SELECT `format_nomor`($nomor, $panjang) as nomor ");
		// return $q->row();	
		$q = $this->db->query("SELECT `format_nomor`($nomor, $panjang) as nomor ");
		return $q->getRow();
		// return $ro->nomor;

	}
	
	public function insert_batch_menu($position_id, $data, $table)
	{
		$builder = $this->db->table($table);
		
		$builder->where('code_group', $position_id);
		$builder->delete();

		$builder->insertBatch($data);
		if($this->db->affectedRows() > 0)
		{
			return true;
		}
		return false;
		
	}
	
	
}


?>