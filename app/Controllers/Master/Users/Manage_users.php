<?php

namespace App\Controllers\Master\Users;
use App\Controllers\BaseController;

class Manage_users extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('master/users/v_manage_users');
	}

	public function tq_daftar_user(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			
			$filter = " AND m.name LIKE '%$filterGet%'";
		}

		$perPageCount 	= 10;
		$item 			= array();

		$select_rows = $db->query("
										SELECT                                   
											m.code,                               
											m.code_group,                         
											m.username,                           
											m.name,                               
											g.`desc` AS group_user,               
											m.status_cd                           
										FROM                                     
											m_user m                              
										LEFT JOIN                                
											m_group g ON (m.code_group = g.code)  
										WHERE                                    
											m.status_cd IN ('normal', 'nullified')
										$filter
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT                                
						m.code,                               
						m.code_group,                         
						m.username,                           
						m.name,                               
						g.`desc` AS group_user,               
						m.status_cd                           
					FROM                                     
						m_user m                              
					LEFT JOIN                                
						m_group g ON (m.code_group = g.code)  
					WHERE                                    
						m.status_cd IN ('normal', 'nullified')
					$filter
				order by m.name desc 
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $code_group, $username, $name, $group_user, $status_cd) = fetchlist($b);	
				$n++;

				if ($status_cd == 'normal')
				{
					$sc = 'AKTIF';
				}
				else
				{
					$sc = 'SUSPENDED';
				}
				$ret 	.= "
							<tr>
								<td class='bleft bright bbottom bpad7' style='text-align:center;'>
									$n
								</td>
								<td class='bright bbottom bpad7' style='text-align:left;'>
									$username
								</td>
								<td class='bright bbottom bpad7'>
									<span class='xlink name_click' data-code='$code'>$name</span>
								</td>
								<td class='bright bbottom bpad7' style='text-align:left;'>
									$group_user
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;'>
									$sc
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;'>
									<span class='xlink non_aktif' data-code='$code'>Non Aktifkan</span> &nbsp;
									<span class='xlink hapus' data-code='$code'>Hapus</span>
								</td>
							</tr>
							<tr>
								<td colspan='6' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='3 class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = table_footer_manage($tbl_ar, 't_list_user', $backPage, $nextPage, $pageNumber, $pagesCount);

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
		
	}

	public function Form_user(){
		$db = db_connect();

		$code 	= $this->request->getVar('code');

		
		
		$query = $db->query("SELECT code, code_group, username, password, name FROM m_user where code='$code'");
		$row = $query->getRowArray();
		if (isset($row))
		{
			 $data['code'] 			= $row['code'];
		     $data['code_group'] 	= $row['code_group'];
		     $data['username'] 		= $row['username'];
		     $data['password'] 		= $row['password'];
		     $data['name'] 			= $row['name'];
		     $data['list_select_daftar_group']	= $this->modeltransc->select_dropdown("select `code`, `desc` from m_group where status_cd = 'normal' ");
		}
		else
		{
			$data['code'] 				= '';
			$data['code_group'] 		= '';
			$data['username'] 			= '';
	     	$data['password'] 			= '';
	    	$data['name'] 				= '';
	    	$data['list_select_daftar_group']	= $this->modeltransc->select_dropdown("select `code`, `desc` from m_group where status_cd = 'normal' ");
		}

		return view("master/users/v_form_user", $data);
	}

	public function save_user()
	{
		$db 					= db_connect();

		$code 					= $this->request->getVar('code');
		$username 				= $this->request->getVar('username');
		$password 				= $this->request->getVar('password');
		$nama 					= $this->request->getVar('nama');
		$group_user 			= $this->request->getVar('group_user');
		
		$initial				= 'USER';
		$table					= 'm_user';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			$data 		= array('username' 			=> $username, 
									'password' 		=> $password, 
									'code_group' 	=> $group_user, 
									'name' 			=> $nama
								);	

			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
		}
		else
		{
			$var_increment	= $initial.date('y').date('m').date('d');
			$sql_cd			= "SELECT 
									MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
								FROM 
									$table
								WHERE 
									left(CODE,10) = '".$var_increment."'";
			$query_cd	= $db->query($sql_cd);
			foreach($query_cd->getResult() as $row_cd){
				$tmpNo_cd	= $row_cd->tmpNo_cd;
				if(is_null($row_cd->tmpNo_cd)){
					$tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
				}
				else{
					$tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4); 
					$tmpNo_cd 	= $tmpNo_cd->nomor;
					$tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
				}
			}
			
			$data 		= array('code' 				=> $tmpNo_cd, 
									'username' 		=> $username, 
									'password' 		=> $password, 
									'code_group' 	=> $group_user, 
									'name' 			=> $nama
								);	
									
			$type		= "insert";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
			
		}
	
		echo json_encode($resp);
	}


	public function non_aktif_hapus()
	{
		$db 					= db_connect();

		$code 					= $this->request->getVar('code');
		$tipe 					= $this->request->getVar('tipe');

		$initial				= 'USER';
		$table					= 'm_user';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			if ($tipe == 'non')
			{
				$data 		= array('status_cd' 		=> 'nullified'
								);	

				$type		= "update";
				$message	= 'Sukses Memproses Data';
				$table_id	= "code";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
			}
			else if ($tipe == 'hap')
			{
				$data 		= array();	

				$type		= "delete";
				$message	= 'Sukses Memproses Data';
				$table_id	= "code";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
			}
		}
		else
		{
			
		}
	
		echo json_encode($resp);
	}


}
