<?php

namespace App\Controllers\Master\Users;
use App\Controllers\BaseController;

class Manage_group extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('master/users/v_manage_group');
	}

	public function t_list_group(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			
			$filter = " AND m.desc LIKE '%$filterGet%'";
		}

		$perPageCount 	= 10;
		$item 			= array();

		$select_rows = $db->query("
										SELECT
											m.code,
											m.desc
										FROM
											m_group m
										WHERE 
											m.status_cd IN ('normal')
										$filter
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT
					m.code,
					m.desc
				FROM
					m_group m
				WHERE 
					m.status_cd IN ('normal')
					$filter
				order by m.code asc
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $desc) = fetchlist($b);	
				$n++;

				$ret 	.= "
							<tr>
								<td class='bleft bright bbottom bpad7' style='text-align:center;'>
									$n
								</td>
								<td class='bright bbottom bpad7'>
									<span class='xlink desc_click' data-code='$code'>$desc</span>
								</td>
								
							</tr>
							<tr>
								<td colspan='2' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='2' class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = table_footer_manage($tbl_ar, 't_list_group', $backPage, $nextPage, $pageNumber, $pagesCount);

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
		
	}

	public function Form_user(){
		$db = db_connect();

		$code 	= $this->request->getVar('code');

		
		
		$query = $db->query("SELECT code, `desc` FROM m_group where code='$code'");
		$row = $query->getRowArray();
		if (isset($row))
		{
			 $data['code'] 		= $row['code'];
		     $data['desc'] 		= $row['desc'];
		     
		}
		else
		{
			$data['code'] 		= '';
			$data['desc'] 		= '';
		}

		return view("master/users/v_form_manage_group", $data);
	}

	public function save_group()
	{
		$db 					= db_connect();

		$code 					= $this->request->getVar('code');
		$desc 		 			= $this->request->getVar('desc');
		
		$initial				= 'GROUP';
		$table					= 'm_group';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			$data 		= array('desc' 		=> $desc
								);	

			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
		}
		else
		{
			
		}
	
		echo json_encode($resp);
	}


	


}
