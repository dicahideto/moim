<?php
namespace App\Controllers\Master\Menu;
use App\Controllers\BaseController;

class Manage_menu extends BaseController {
	
	public function __construct()
	{
        $this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
    }
	
	public function index()
	{
        $db         = db_connect();

		$id 		= $this->request->getVar('id');
		$nam 		= $this->request->getVar('nam');
		$dataRole 	= null;
		
		if(!empty($id))
		{
            $dataRole = $this->modeltransc->select_by_id_sql_obj("SELECT * FROM r_usergroupmenu where code_group = '$id'");
            
		}
		if(!empty($nam))
		{
			$data['nam'] 		 = $nam;
			$data['namid'] 		 = $id;
		}
		else
		{
			$data['nam'] 		 = '';
			$data['namid'] 		 = '';
		}
		
		
		// $data['parent_menu']	     = $this->modeltransc->select_data_custom_II('isParent is null and isActive in (1)', 'dafmenu');
		$data['parent_menu']	= $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '1' ");
        $data['sub_menu']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '2' ");
        $data['sub_menu2']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '3' ");
        $data['sub_menu3']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '4' ");
		// $data['sub_menu'] 		 = $this->model->select_data_custom_II('isParent in (1) and isActive in (1)', 'dafmenu');
		// $data['sub_menu2'] 		 = $this->model->select_data_custom_II('isParent in (2) and isActive in (1)', 'dafmenu');
		// $data['sub_menu3'] 		 = $this->model->select_data_custom_II('isParent in (3) and isActive in (1)', 'dafmenu');
		$data['dataRole'] 		 = $dataRole;
		
		// $this->template->display('apad/v_menu_setting', $data);
        return view_one('Master/menu/v_manage_menu', $data);
	}


    public function t_daftar_group(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			
			$filter = " AND m.name LIKE '%$filterGet%'";
		}

		$perPageCount 	= 10;
		$item 			= array();

		$select_rows = $db->query("
                                    SELECT 
                                    m.code,
                                    m.`desc`,
                                    m.step
                                    FROM
                                    m_group m
                                    WHERE
                                    status_cd = 'normal'
										$filter
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT 
                    m.code,
                    m.`desc`,
                    m.step
                FROM
                    m_group m
                WHERE
                    m.status_cd = 'normal'
					$filter
				order by m.step asc 
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $desc, $step) = fetchlist($b);	
				$n++;

				$ret 	.= "
							<tr>
								<td class='bleft bright bbottom bpad7' style='text-align:center;'>
									$n
								</td>
								<td class='bright bbottom bpad7' style='text-align:left;'>
                                    <span class='xlink desc_click' data-code='$code' data-desc='$code'>$desc</span>
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;'>
                                    $step
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='3' class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = table_footer_manage($tbl_ar, 't_list_user', $backPage, $nextPage, $pageNumber, $pagesCount);

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
		
	}


    public function Form_menu(){
        $db = db_connect();

        $id 		= $this->request->getVar('id');
		$nam 		= $this->request->getVar('nam');
		$dataRole 	= null;
		
		if(!empty($id))
		{
            $dataRole = $this->modeltransc->select_by_id_sql_obj("SELECT * FROM r_usergroupmenu where code_group = '$id'");
            
		}
		if(!empty($nam))
		{
			$data['nam'] 		 = $nam;
			$data['namid'] 		 = $id;
		}
		else
		{
			$data['nam'] 		 = '';
			$data['namid'] 		 = '';
		}
		
		$data['parent_menu']	= $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '1' ");
        $data['sub_menu']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '2' ");
        $data['sub_menu2']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '3' ");
        $data['sub_menu3']	    = $this->modeltransc->select_by_id_sql_obj("select * from m_menu where status_cd = 'normal' and menu_rank = '4' ");
		$data['dataRole'] 		 = $dataRole;

        return view("master/menu/v_form_menu", $data);
    }



	
	public function save()
	{
        $db = db_connect();
		$code 		= $this->request->getVar('code');
		$menu_id 	= $this->request->getVar('menu');
		
        if(!empty($menu_id)){
            $sql_del = "DELETE FROM r_usergroupmenu where code_group = '$code'";
            $db->query($sql_del);
        }
        $data_add = array();
        foreach($menu_id as $m){
            if(!empty($m)){

                $sql = "INSERT INTO r_usergroupmenu
                            (code_group, code_menu)
                        VALUES
                            ('$code', '$m')";
                $db->query($sql);
              
            }
        }
        
      
            $resp['error'] 		= '';
            $resp['status'] 	= 'success';
            $resp['message'] 	= 'success';
        
		
		
		echo json_encode($resp);
	}
	

}
