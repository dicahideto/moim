<?php

namespace App\Controllers\Master\Unit;
use App\Controllers\BaseController;

class Manage_unit extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('master/unit/v_manage_unit');
	}

	public function tq_daftar_unit(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";
		$perPageCount 	= 10;

		if ($pageGet == '') {
			$pageNumber = 1;
			$n = 0;
		} else {
			$pageNumber = $pageGet;
			$n = ($pageNumber*$perPageCount)-$perPageCount;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			
			$filter = " AND m.org_nm LIKE '%$filterGet%'";
		}

		
		$item 			= array();

		$select_rows = $db->query("
										SELECT                                   
											m.org_id,                               
											m.org_nm                
										FROM                                     
											m_orgs m   
										WHERE                                    
											m.status_cd IN ('active')
										$filter
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT                                
						m.org_id,                               
						m.org_nm                        
					FROM                                     
						m_orgs m  
					WHERE                                    
						m.status_cd IN ('active')
					$filter
				order by m.created_dttm desc 
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($org_id, $org_nm) = fetchlist($b);	
				$n++;

				$ret 	.= "
							<tr>
								<td class='bleft bright bbottom bpad7' style='text-align:center;'>
									$n
								</td>
								<td class='bright bbottom bpad7'>
									<span class='xlink name_click' data-code='$org_id'>$org_nm</span>
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;'>
									<span class='xlink hapus' data-code='$org_id'>Hapus</span>
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding:0px;' class=''>
									<div id='form_edit_${org_id}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='2 class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = table_footer_manage($tbl_ar, 't_list_user', $backPage, $nextPage, $pageNumber, $pagesCount);

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
		
	}

	public function Form_user(){
		$db = db_connect();

		$code 	= $this->request->getVar('code');

		$query = $db->query("SELECT org_id, org_nm FROM m_orgs where org_id = '$code' ");
		$row = $query->getRowArray();
		if (isset($row))
		{
			 $data['org_id'] 		= $row['org_id'];
		     $data['org_nm'] 		= $row['org_nm'];
		    
		}
		else
		{
			$data['org_id'] 		= '';
			$data['org_nm'] 		= '';
		
	    	
		}

		return view("master/unit/v_form_unit", $data);
	}

	public function save_user()
	{
		$db 					= db_connect();

		$code 					= $this->request->getVar('code');
		$org_nm 				= $this->request->getVar('org_nm');
		
		$table					= 'm_orgs';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT org_id FROM ".$table." WHERE org_id = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			$data 		= array('org_nm' 			=> $org_nm
								);	

			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "org_id";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
		}
		else
		{
			$data 		= array('org_nm' 		=> $org_nm
								);	
									
			$type		= "insert";
			$message	= 'Sukses Memproses Data';
			$table_id	= "org_id";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
			
		}
	
		echo json_encode($resp);
	}


	public function non_aktif_hapus()
	{
		$db 					= db_connect();

		$code 					= $this->request->getVar('code');
		$tipe 					= $this->request->getVar('tipe');

		$table					= 'm_orgs';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT org_id FROM ".$table." WHERE org_id = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			// if ($tipe == 'non')
			// {
				$data 		= array('status_cd' 		=> 'nullified'
								);	

				$type		= "update";
				$message	= 'Sukses Memproses Data';
				$table_id	= "org_id";
				
				$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
				$resp['error'] 		= $query['error'];
				$resp['message']	= $query['message'];
				$resp['status']		= $query['status'];
			// }
			// else if ($tipe == 'hap')
			// {
			// 	$data 		= array();	

			// 	$type		= "delete";
			// 	$message	= 'Sukses Memproses Data';
			// 	$table_id	= "org_id";
				
			// 	$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			// 	$resp['error'] 		= $query['error'];
			// 	$resp['message']	= $query['message'];
			// 	$resp['status']		= $query['status'];
			// }
		}
		else
		{
			
		}
	
		echo json_encode($resp);
	}


}
