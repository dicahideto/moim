<?php

namespace App\Controllers\Transaction\Klaim;
use App\Controllers\BaseController;

class Import_klaim extends BaseController
{
	public function __construct(){
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
    {
        $db = db_connect();
        $user 		            = $_SESSION['codeuser_sess'];
        $sql_del = "DELETE FROM temp_t_klaim
                    WHERE status_cd='normal' AND code_user_process_one = '$user' ";
        $query_del = $db->query($sql_del);

		return view_one('Transaction/klaim/v_import_klaim');
	}

	public function check_case_jaminan($jaminan){
		$db = db_connect();

		$obj_id_list = array();

		$sql = "SELECT 
						description
					FROM
						m_jaminan a
					";

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($obj_idres) = fetchlist($b);

					if( in_array( $obj_idres ,$obj_id_list ) )
					{
						
					}
					else
					{
						array_push($obj_id_list,$obj_idres);
					}
				}
			}
		
		if (in_array(strtolower($jaminan), array_map('strtolower', $obj_id_list))) {
			return true;
		}
		else
		{
			return false;
		}
	}

	public function prosesExcelSpread()
	{
		$db = db_connect();

		$code					= '';
		$initial				= 'KLAIM';
		$table					= 'temp_t_klaim';
        $user 		            = $_SESSION['codeuser_sess'];

		$file 			= $this->request->getFile('file');
		$formattanggal 	= $this->request->getVar('formattanggal');
		$file_num 		= $this->request->getVar('file_num');
		$ext = $file->getClientExtension();

		if ($ext == 'xls'){
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			$act 	= 'normal';
		}
		else if ($ext == 'xlsx')
		{
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			$act 	= 'normal';
		}
        else if ($ext == 'xlsx')
		{
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
			$act 	= 'normal';
		}
		else
		{
			$act 	= 'fail';
		}

		if ($act == 'fail')
		{
			$resp['error'] 		= 'error';
			$resp['message']	= 'File harus dalam bentuk excel';
			$resp['status']		= 'error';
		}
		else
		{
			$spreadsheet = $render->load($file);
			$sheet = $spreadsheet->getActiveSheet()->toArray();


			if ($file_num == 'satu')
			{

				foreach($sheet as $x => $excel){
	                $var_increment	= $initial.date('y').date('m').date('d');
	                $sql_cd			= "SELECT 
	                                        MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
	                                    FROM 
	                                        $table
	                                    WHERE 
	                                        left(CODE,11) = '".$var_increment."'";
	                $query_cd	= $db->query($sql_cd);
	                foreach($query_cd->getResult() as $row_cd){
	                    $tmpNo_cd	= $row_cd->tmpNo_cd;
	                    if(is_null($row_cd->tmpNo_cd)){
	                        $tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
	                    }
	                    else{
	                        $tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4); 
	                        $tmpNo_cd 	= $tmpNo_cd->nomor;
	                        $tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
	                    }
	                }

					if ($x == 0)
					{
						continue;
					}

					$no_rm 				= $excel['0'];
					$nama_pasien 		= $excel['1'];
					$jaminan 			= $excel['2'];
					$unit_dept 			= $excel['3'];
					$ruang_bed 			= $excel['4'];
					$tgl_masuk 			= $excel['5'];
					$tgl_keluar 		= $excel['6'];
					$dpjp 				= $excel['7'];
					$keterangan			= $excel['8'];
					

					if ($no_rm == '')
					{
						continue;
					}

					if ($formattanggal == 'two')
					{
						$tgl_masuk 			= tgl_time_save_form_miring_sql($tgl_masuk);
						$tgl_keluar 		= tgl_time_save_form_miring_sql($tgl_keluar);
						
					}
					else if ($formattanggal == 'three')
					{
						$tgl_masuk 			= tgl_time_save_form_miring_sql_sec($tgl_masuk);
						$tgl_keluar 		= tgl_time_save_form_miring_sql_sec($tgl_keluar);
						
					}
					else
					{
						$tgl_masuk 			= tgl_time_save_form_miring_sql_convert_sec($tgl_masuk);
						$tgl_keluar 		= tgl_time_save_form_miring_sql_convert_sec($tgl_keluar);
						
					}

					$tgl_unique = tgl_unique($tgl_keluar);
	                
					if ($this->check_case_jaminan($jaminan))
					{
						$jaminan = strtoupper($jaminan);
					}
					else
					{
						$jaminan = '';
					}


					$data 		= array(
										'code'			        => $tmpNo_cd,
										'code_group_date_out'	=> $tgl_unique,
										'ext_id'			    => $no_rm,
										'person_nm'			    => $nama_pasien,
										'jaminan'			    => $jaminan,
										'org'				    => $unit_dept,
										'ruang'				    => $ruang_bed,
										'tgl_masuk'			    => $tgl_masuk,
										'tgl_keluar'		    => $tgl_keluar,
										'dpjp'				    => $dpjp,
										'keterangan'		    => $keterangan,
										'code_user_process_one'	=> $user
										);	
											
					$type		= "insert";
					$message	= 'Sukses Memproses Data';
					$table_id	= "code";
	                
	              
					$query		= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);
				}
			}
			else if ($file_num == 'dua')
			{
				$num_increment = 0;
				foreach($sheet as $x => $excel){
					$num_increment++;

					// if ($num_increment > 0)
					// {
						$var_increment	= $initial.date('y').date('m').date('d');
		                $sql_cd			= "SELECT 
		                                        MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
		                                    FROM 
		                                        $table
		                                    WHERE 
		                                        left(CODE,11) = '".$var_increment."'";
		                $query_cd	= $db->query($sql_cd);
		                foreach($query_cd->getResult() as $row_cd){
		                    $tmpNo_cd	= $row_cd->tmpNo_cd;
		                    if(is_null($row_cd->tmpNo_cd)){
		                        $tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
		                    }
		                    else{
		                        $tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4); 
		                        $tmpNo_cd 	= $tmpNo_cd->nomor;
		                        $tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
		                    }
		                }

						if ($x < 5)
						{
							continue;
						}

						$no_rm 				= $excel['0'];
						$nama_pasien 		= $excel['1'];
						$jaminan 			= $excel['3'];
						$unit_dept 			= $excel['4'];
						$ruang_bed 			= $excel['5'];
						$tgl_masuk 			= $excel['6'];
						$tgl_keluar 		= $excel['7'];
						$dpjp 				= $excel['15'];
					
						if ($no_rm == '')
						{
							continue;
						}

						if ($formattanggal == 'two')
						{
							$tgl_masuk 			= tgl_time_save_form_miring_sql($tgl_masuk);
							$tgl_keluar 		= tgl_time_save_form_miring_sql($tgl_keluar);
							
						}
						else if ($formattanggal == 'three')
						{
							$tgl_masuk 			= tgl_time_save_form_miring_sql_sec($tgl_masuk);
							$tgl_keluar 		= tgl_time_save_form_miring_sql_sec($tgl_keluar);
							
						}
						else
						{
							list($hari_masuk, $bulan_masuk, $tahun_masuk, $waktu_masuk) = explode(" ", $tgl_masuk);
							$tgl_masuk = $tahun_masuk.'-'.bulan_indo_to_sql($bulan_masuk).'-'.$hari_masuk.' '.$waktu_masuk;

							list($hari_keluar, $bulan_keluar, $tahun_keluar, $waktu_keluar) = explode(" ", $tgl_keluar);
							$tgl_keluar = $tahun_keluar.'-'.bulan_indo_to_sql($bulan_keluar).'-'.$hari_keluar.' '.$waktu_keluar;
							
							// var_dump(bulan_indo_to_sql('November'));die;
							// var_dump(bulan_indo_to_sql($bulan_masuk));die;
						}
		                
		                $tgl_unique = tgl_unique($tgl_keluar);

						if ($this->check_case_jaminan($jaminan))
						{
							$jaminan = strtoupper($jaminan);
						}
						else
						{
							$jaminan = '';
						}

						$data 		= array(
											'code'			        => $tmpNo_cd,
											'code_group_date_out'	=> $tgl_unique,
											'ext_id'			    => $no_rm,
											'person_nm'			    => $nama_pasien,
											'jaminan'			    => $jaminan,
											'org'				    => $unit_dept,
											'ruang'				    => $ruang_bed,
											'tgl_masuk'			    => $tgl_masuk,
											'tgl_keluar'		    => $tgl_keluar,
											'dpjp'				    => $dpjp,
											'code_user_process_one'	=> $user
											);	
												
						$type		= "insert";
						$message	= 'Sukses Memproses Data';
						$table_id	= "code";
		                
		              
						$query		= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);
						
					// }

				}	
			}


			$resp['error'] 		= 'success';
			$resp['message']	= 'Berhasil';
			$resp['status']		= 'success';
		}
		
		echo json_encode($resp);
	}

	public function tesdca(){
		$resp['error'] 		= 'success';
		$resp['message']	= 'Berhasil';
		$resp['status']		= 'success';

		echo json_encode($resp);
	}

}
