<?php

namespace App\Controllers\Transaction\Klaim;
use App\Controllers\BaseController;

class Data_klaim_global extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('transaction/klaim/v_data_klaim_global');
	}

	public function progress_nm_group($progress){
		$db 					= db_connect();

		$sqk = $db->query("SELECT 
									`desc`
									from
									m_group
									where step in ($progress) and status_cd != 'nullified' ");
		$row = $sqk->getRow();
		if (isset($row))
		{
		   return $row->desc;
		}
		else
		{
			return '';
		}
	}

	public function result_table_filter(){
		$db 					= db_connect();

		$user 		= $_SESSION['codeuser_sess'];
		$step_user 	= $_SESSION['step_sess'];

		$value_filter_utama 		= $this->request->getVar('value_filter_utama');
		$value_select_filter 		= $this->request->getVar('value_select_filter');
		$value_filter_utama_sec 	= $this->request->getVar('value_filter_utama_sec');

		if ($value_select_filter == 'tgl_keluar')
		{
			$value_filter_utama = tgl_save_sql($value_filter_utama);

			$filter = " and m.tgl_keluar >= '$value_filter_utama 00:00:00' and m.tgl_keluar <= '$value_filter_utama 23:59:59' ";
		}
		else if ($value_select_filter == 'tgl_keluar_range')
		{
			$value_filter_utama 	= tgl_save_sql($value_filter_utama);
			$value_filter_utama_sec = tgl_save_sql($value_filter_utama_sec);

			$filter = " and m.tgl_keluar >= '$value_filter_utama 00:00:00' and m.tgl_keluar <= '$value_filter_utama_sec 23:59:59' ";
		}
		else if ($value_select_filter == 'nama_pasien')
		{
			$filter = " and m.person_nm like '%$value_filter_utama%' ";
		}
		else if ($value_select_filter == 'rm_pasien')
		{
			$filter = " and m.ext_id = '$value_filter_utama' ";
		}

		$ret = "";

		$sql = "SELECT
					m.code,
					m.ext_id,
					m.person_nm,
					m.jaminan,
					m.org,
					m.ruang,
					m.tgl_masuk,
					m.tgl_keluar,
					m.`status`,
					m.dpjp,
					m.gap,
					m.hari,
					m.jam,
					m.keterangan,
					m.kelengkapan,
					m.tgl_rm_dilengkapi,
					m.kontrol,
					m.status_cd,
					m.progress,
					m.code_user_process_zero,
					m.code_user_process_one,
					m.code_user_process_two,
					m.code_user_process_three,
					m.code_user_process_four,
					m.code_user_process_five
					FROM
					t_klaim m
					where
					m.status_cd = 'normal'
					$filter
					ORDER BY m.code desc
				
				";

		// var_dump($sql);die;		
		
		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				// list($code, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $gap, $hari, $jam, $keterangan, $kelengkapan, $tgl_rm_dilengkapi, $kontrol, $status_cd, $progress, $code_user_process_zero, $code_user_process_one, $code_user_process_two, $code_user_process_three, $code_user_process_four, $code_user_process_five) = fetchlist($b);

				list($code, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $gap, $hari, $jam, $keterangan, $kelengkapan, $tgl_rm_dilengkapi, $kontrol, $status_cd, $progress, $code_user_process_zero, $code_user_process_one, $code_user_process_two, $code_user_process_three, $code_user_process_four, $code_user_process_five) = fetchlist($b);
				$n++;

				if ($step_user == '0')
				{
					$linked 	= 'xlink';
					$display	= '';
				}
				else if ($step_user == '1')
				{
					if ($progress == '1')
					{
						$linked 	= 'xlink';
						$display	= '';
					}
					else
					{
						$linked 	= '';
						$display	= 'display:none;';
					}
				}
				else
				{
					$linked 	= '';
					$display	= 'display:none;';
				}

				if ($progress == '1')
				{
					$us = $code_user_process_zero;
					$progress_nm = $this->progress_nm_group($progress);
				}
				else if ($progress == '2')
				{
					$us = $code_user_process_one;
					
					$progress_nm = $this->progress_nm_group($progress);
				}
				else if ($progress == '3')
				{
					$us = $code_user_process_two;
					
					$progress_nm = $this->progress_nm_group($progress);
				}
				else if ($progress == '4')
				{
					$us = $code_user_process_three;
					
					$progress_nm = $this->progress_nm_group($progress);
				}
				else if ($progress == '5')
				{
					$us = $code_user_process_four;
					
					$progress_nm = $this->progress_nm_group($progress);
				}
				else if ($progress == '100')
				{
					$us = $code_user_process_five;
					
					$progress_nm = "SELESAI";
				}

				$qsq = "select 
                                    name
                                    from
                                    m_user
                                    where `code` = '".$us."'";
				$sq = $db->query($qsq);

                $row = $sq->getRow();
                if (isset($row))
                {
                   $us_nm = $row->name;
                }
                else
                {
                	$us_nm = "";
                }
				
				$code 						= ($code == "" || $code == null ? "-" : $code);
				$ext_id 					= ($ext_id == "" || $ext_id == null ? "-" : $ext_id);
				$person_nm 					= ($person_nm == "" || $person_nm == null ? "-" : $person_nm);
				$jaminan 					= ($jaminan == "" || $jaminan == null ? "-" : $jaminan);
				$org 						= ($org == "" || $org == null ? "-" : $org);
				$ruang 						= ($ruang == "" || $ruang == null ? "-" : $ruang);
				$tgl_masuk 					= (tgl_indo_miring_time($tgl_masuk) == "" || $tgl_masuk == null ? "-" : tgl_indo_miring_time($tgl_masuk));
				$tgl_keluar 				= (tgl_indo_miring_time($tgl_keluar) == "" || $tgl_keluar == null ? "-" : tgl_indo_miring_time($tgl_keluar));
				$status 					= ($status == "" || $status == null ? "-" : $status);
				$dpjp 						= ($dpjp == "" || $dpjp == null ? "-" : $dpjp);
				$kelengkapan 				= ($kelengkapan == "" || $kelengkapan == null ? "-" : $kelengkapan);
				$keterangan 				= ($keterangan == "" || $keterangan == null ? "-" : $keterangan);
				
				$kontrol = ($kontrol 		== "" ? "-" : $kontrol);
				$status_cd = ($status_cd 	== "" ? "-" : $status_cd);


				if ($linked 	== 'xlink')
				{
					$clickable_ext_id 		= 'clickable_ext_id';
					$clickable_person 		= 'clickable_person';
					$clickable_jaminan 		= 'clickable_jaminan';
					$clickable_org 			= 'clickable_org';
					$clickable_ruang 		= 'clickable_ruang';
					$clickable_tgl_masuk 	= 'clickable_tgl_masuk';
					$clickable_tgl_keluar 	= 'clickable_tgl_keluar';
					
					$clickable_dpjp 		= 'clickable_dpjp';
					$clickable_kelengkapan 	= 'clickable_kelengkapan';
					$clickable_keterangan 	= 'clickable_keterangan';

				}
				else
				{
					$clickable_ext_id 		= '';
					$clickable_person 		= '';
					$clickable_jaminan 		= '';
					$clickable_org 			= '';
					$clickable_ruang 		= '';
					$clickable_tgl_masuk 	= '';
					$clickable_tgl_keluar 	= '';
					
					$clickable_dpjp 		= '';
					$clickable_kelengkapan 	= '';
					$clickable_keterangan 	= '';
				}


				$del_act 					= "<img src='".base_url()."/public/tmpassets/assets/sysimg/delete.png' style='width:18px;height:18px;cursor:pointer;' data-code='$code' class='clickable_del xlink' id='clickable_del_${code}'/>";

				$ret .= "<tr id='line_${code}'>
						<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;'>
							${n}
						</td>
						<td class='bleft bright bbottom bpad7 ' style='cursor:pointer;text-align:left;min-width:150px;' data-code='$code' >
							${progress_nm}
						</td>
						<td class='bleft bright bbottom bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							${us_nm}
						</td>
						<td class='bleft bright bbottom bpad7 ' style='cursor:pointer;text-align:left;min-width:150px;' data-code='$code' >
							<span class='$clickable_ext_id ${linked}' id='clickable_ext_id_${code}' data-code='$code' data-val='$ext_id'>$ext_id</span>
							<span class='clickable_ext_id_form' id='clickable_ext_id_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_person ${linked}' id='clickable_person_${code}' data-code='$code' data-val='$person_nm'>$person_nm</span>
							<span class='clickable_person_form' id='clickable_person_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_jaminan ${linked}' id='clickable_jaminan_${code}' data-code='$code' data-val='$jaminan'>$jaminan</span>
							<span class='clickable_jaminan_form' id='clickable_jaminan_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_org ${linked}' id='clickable_org_${code}' data-code='$code' data-val='$org'>$org</span>
							<span class='clickable_org_form' id='clickable_org_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code'  >
							<span class='$clickable_ruang ${linked}' id='clickable_ruang_${code}' data-code='$code' data-val='$ruang'>$ruang</span>
							<span class='clickable_ruang_form' id='clickable_ruang_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_tgl_masuk ${linked}' id='clickable_tgl_masuk_${code}' data-code='$code' data-val='$tgl_masuk' data-mrn='$ext_id'>$tgl_masuk</span>
							<span class='clickable_tgl_masuk_form' id='clickable_tgl_masuk_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_tgl_keluar ${linked}' id='clickable_tgl_keluar_${code}' data-code='$code' data-val='$tgl_keluar'>$tgl_keluar</span>
							<span class='clickable_tgl_keluar_form' id='clickable_tgl_keluar_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_dpjp ${linked}' id='clickable_dpjp_${code}' data-code='$code' data-val='$dpjp'>$dpjp</span>
							<span class='clickable_dpjp_form' id='clickable_dpjp_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_kelengkapan ${linked}' id='clickable_kelengkapan_${code}' data-code='$code' data-val='$kelengkapan'>$kelengkapan</span>
							<span class='clickable_kelengkapan_form' id='clickable_kelengkapan_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_keterangan ${linked}' id='clickable_keterangan_${code}' data-code='$code' data-val='$keterangan'>$keterangan</span>
							<span class='clickable_keterangan_form' id='clickable_keterangan_form_${code}'></span>
						</td>
					</tr>
				 	";
			}
		}
		else
		{
			$ret .= "<tr>
						<td colspan='13' style='padding:10px;' class='btop bleft bbottom bright bpad7'>
							Data Kosong
						</td>
					</tr>";
		}

		

		$resp['row_result'] = $ret;

		echo json_encode($resp); 

	}

}
