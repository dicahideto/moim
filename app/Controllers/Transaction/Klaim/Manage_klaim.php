<?php

namespace App\Controllers\Transaction\Klaim;
use App\Controllers\BaseController;

class Manage_klaim extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('transaction/klaim/v_manage_klaim');
	}

	public function check_date_out(){
		$db 	= db_connect();
		$date 	= date('Y-m-d');

		$sql = "SELECT ext_id FROM t_klaim WHERE tgl_keluar >= '$date 00:00:00' AND tgl_keluar <= '$date 23:59:59' and status_cd != 'nullified' LIMIT 1 ";

		$result_sql	= $db->query($sql);
		if ($result_sql->getNumRows() > 0)
		{
			$resp['message']	= 'error';
			$resp['date']		= $date;
		}
		else
		{
			$resp['message']	= 'success';
			$resp['date']		= $date;
		}

		echo json_encode($resp);
	}

	public function t_list_manage_klaim(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');

		$month 	= $this->request->getVar('month_var');
		$year 	= $this->request->getVar('year_var');

		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		$step_user 	= $_SESSION['step_sess'];

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			$filter = "";
			// $filter = " AND m.name LIKE '%$filterGet%'";
		}

		$perPageCount 	= 100;
		$item 			= array();

		$select_rows = $db->query("
										SELECT
											m.desc
										FROM
											m_group m
										where 
											m.code != 'GROUP20210211000'
										ORDER BY step asc
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT
					m.code,
					m.desc,
					m.step
					FROM
					m_group m
					where 
					m.code != 'GROUP20210211000'
					ORDER BY m.step asc
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $desc, $step) = fetchlist($b);	
				$n++;

				if ($step == '1')
				{
					$prog = '1';
					$prog_min = '2';
				}
				else if ($step == '2')
				{
					$prog = '2';
					$prog_min = '3';
				}
				else if ($step == '3')
				{
					$prog = '3';	
					$prog_min = '4';
				}
				else if ($step == '4')
				{
					$prog = '4';	
					$prog_min = '5';	
				}
				else if ($step == '5')
				{
					$prog = '5';	
					$prog_min = '100';		
				}
				else
				{
					$prog = '100';
					$prog_min = '5';
				}

				if ($step_user == $step)
				{
					$color_own = 'color:#81e340;font-weight:bold;';
				}
				else
				{
					$color_own = '';
				}

				$queryTotalProgress = $db->query("SELECT COUNT(code) as total from t_klaim WHERE progress = '$prog' and status_cd != 'nullified' AND tgl_keluar != '' AND
														tgl_keluar >= '$year-$month-01 00:00:00' and tgl_keluar <= '$year-$month-31 23:59:59' ");

				$row = $queryTotalProgress->getRowArray();
				if (isset($row))
				{
				     $total = $row['total'];
				}

				$query_name_approval = "SELECT
											a.code,
											a.progress,
											a.list_code,
											a.tanggal_approval,
											b.username,
											b.name
										FROM
											log_t_klaim_progress a
										LEFT JOIN
											m_user b ON (a.user = b.code)
										WHERE 
											a.progress = '$prog'
										and
											a.status_cd != 'nullified'
										limit 0,1
											";

				$result_query_name_approval = $db->query($query_name_approval);							
				$row_name_approval 			= $result_query_name_approval->getRowArray();
				if (isset($row_name_approval))
				{
				    $approval_name = 'Dari '.$row_name_approval['name'];

				    if (strlen($row_name_approval['list_code']) > 18)
				    {
				    	$code_klaim_only_one = substr($row_name_approval['list_code'], 0, strpos($row_name_approval['list_code'], ",")); 
				    }
				    else
				    {
				    	$code_klaim_only_one = $row_name_approval['list_code'];
				    }

				    $sql_get_date_out 			= "SELECT tgl_keluar from t_klaim where `code` = $code_klaim_only_one ";

				    // var_dump($sql_get_date_out);die;		
				    $result_sql_get_date_out 	= $db->query($sql_get_date_out);
				    $row_result_sql_get_date_out 			= $result_sql_get_date_out->getRowArray();
				    if (isset($row_result_sql_get_date_out))
					{
						$date_out_render = tgl_inggris($row_result_sql_get_date_out['tgl_keluar']);
					}
					else
					{
						$date_out_render = '';
					}

					$connector = '/';
				}
				else
				{
					$approval_name = '';
					$date_out_render = '';
					$connector = '';
				}

				$ret 	.= "
							<tr style='' class='hmm' data-step='${step}'>
								<td class='bleft  bbottom bpad10t' style='text-align:left;${color_own}'>
									<span class='xlink clickable_desc'  data-code='$code' data-step='${step}'>$desc</span>
								</td>
								<td class=' bbottom bpad10t' style='text-align:center;'>
									$total
								</td>
								<td class='bright bbottom bpad10t' style='text-align:left;'>
									${approval_name} ${connector}
									${date_out_render}
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='3' class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

		$queryTotalProgressEnd = $db->query("SELECT COUNT(code) as total from t_klaim WHERE progress = '100' and status_cd = 'normal' and tgl_keluar != '' AND
														tgl_keluar >= '$year-$month-01 00:00:00' and tgl_keluar <= '$year-$month-31 23:59:59' ");
		$row_end = $queryTotalProgressEnd->getRowArray();
		if (isset($row_end))
		{
		     $total_end = $row_end['total'];
		}

		$ret 	.= "
						<tr style='' class='hmm' data-step='${step}'>
							<td class='bleft  bbottom bpad10t' style='text-align:left;${color_own}'>
								<span class='xlink clickable_desc_end'  data-step='100'>DATA SELESAI</span>
							</td>
							<td class=' bbottom bpad10t' style='text-align:center;'>
								$total_end
							</td>
							<td class='bright bbottom bpad10t' style='text-align:left;'>
								
							</td>
						</tr>
						<tr>
							<td colspan='3' style='padding:0px;' class=''>
								<div id='form_edit_100'></div>
							</td>
						</tr>
						";

        $detailRet = "";

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
	}

	public function History_app(){
		$db = db_connect();

		$dateout 		= $this->request->getVar('dateout');
		$step 			= $this->request->getVar('step');
		$ret 			= "";

		$data['code'] 	= '';

		$ret .= "	<div id='nduuk_' style='border:1px solid #DFE3E7;' >
						<div style='padding:10px;'>
						<table width='100%' style='border:1px solid #DFE3E7;'>
							<tr>		
				";

		$sql 	= "SELECT 
						`desc`, step
					FROM
						m_group
					WHERE
						status_cd = 'normal'
					AND
						step NOT IN (0)
				";
		
		$result_sql = $db->query($sql);

		$total_column = $result_sql->getNumRows();

		$width_header = 100/$total_column;

		if ($result_sql->getNumRows() > 0)
		{
		   foreach (fetchloopsql($result_sql) as $b)
			{
				list($desc, $step_res) = fetchlist($b);	

				$tgl_uniq = tgl_unique($dateout);

				$step_res = $step_res + 1;
				$sql2 = "SELECT 
							code, list_code, `user`, tanggal_approval, tgl_keluar
						FROM
							log_t_klaim_progress
						WHERE
							code_group_date_out = '$tgl_uniq'
						AND
							progress = $step_res
						AND
							status_cd != 'nullified'
						order BY
							tanggal_approval desc
						";

				$res_approve_view 	= '';
				$no_res_approve 	= 0;
		        $result_sql2		= $db->query($sql2);
				if ($result_sql2->getNumRows() > 0)
				{
					foreach (fetchloopsql($result_sql2) as $b)
					{
						$no_res_approve++;
						list($code_log, $list_code, $user_approved, $tanggal_approval, $tgl_keluar) = fetchlist($b);	

						$sadf = "${list_code}";
						$obj_id_list_in = explode("','",$sadf);

						$total_list_code = count($obj_id_list_in);
						$res_approve = "<span class='xlink detail_list_code' data-dateout='${tgl_keluar}' data-step_newpage='${step}'' data-code_log=${code_log}>(".$total_list_code.")  ".tgl_inggris_time($tanggal_approval)."</span>";
						$res_approve_view = $res_approve_view." ${no_res_approve}. ".$res_approve."<br />";
					}
				}
				else
				{
					$total_list_code = '';
					$res_approve = '';
					$res_approve_view = '';
				}

		        $ret .= "
					<td width='${width_header}' style='text-align:left;padding:5px;s' valign='top'>
						<b>${desc}</b>
						<br />
						<br />
						${res_approve_view}
					
					</td>	
				";
		    }
		}

		$ret .= "			</tr>
						</table>
						</div>
					</div>";

		return $ret;
	}

	public function detail_klaim_pertanggal_keluar(){
		$db 	= db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$step 		= $this->request->getVar('step');
		$ret 		= '';
		$btn_add	= '';
		$tanggal_approval = "Tanggal Approval";

		if ($step == 1)
		{
			$tanggal_approval = "Tanggal Pembuatan";
		}

		$query = $db->query("SELECT `desc` FROM m_group WHERE step = 1 AND status_cd = 'normal'");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_user = $row->desc;
		}
		else
		{
			$desc_user = '';
		}

		if ($step == 1)
		{
			$btn_add = "<input value='Tambah Data Klaim' type='button' id='input_data_klaim' class='' style='font-size:0.7em;background-color:#ffffff;padding:7px;border:1px solid #bbb;'>";
		}
		else
		{
			$btn_add = '';
		}

		$ret 	.= "<div id='tolee_' style='border-left:1px solid #DFE3E7;border-right:1px solid #DFE3E7;border-bottom:1px solid #DFE3E7;'>
						<div style='padding:10px;'>
							<div class='row' style='padding-top:10px;'>
	                            <div class='col-md-8'>
	                               ${btn_add}
	                            </div>
	                            <div class='col-md-4'>
	                                <div class='form-group row '>
	                                    <div class='col-lg-2 col-3' style='text-align:right;'>
	                                        <label class='col-form-label'>Filter</label>
	                                    </div>
	                                    <div class='col-lg-10 col-9'>
	                                        <input type='text' id='kt_datepicker' class='form-control' name='fname' placeholder=''>
	                                    </div>
	                                </div>
	                            </div>
								<div class='col-md-12'>
									<div style='text-align:center;padding:10px;'>
										<!-- 
										<input type='button' value='Harian' style='width:200px;border:1px solid #bbb;height:25px;'> &nbsp;&nbsp;
										<input type='button' value='Bulanan' style='width:200px;border:1px solid #bbb;height:25px;'>
										-->
									</div>
								</div>
                        	</div>
							<table class='' id='t_list_klaim_pertanggal_keluar' style='width:100%'>
	                            <thead>
	                                <tr>
	                                    <th class='bpad7 bleft btop bbottom'>Tanggal Keluar</th>
	                                    <th class='bpad7 bleft btop bbottom bright' style='text-align:center;' width='10%'>Total Data</th>
	                                    <th class='bpad7 bleft btop bbottom bright' style='text-align:center;' width='10%'>History Approval</th>
	                                    <th class='bpad7 bleft btop bbottom bright' style='text-align:center;' width='10%'>Export</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                
	                            </tbody>
	                        </table>
						</div>
					</div>
				";

		return $ret;
	}

	public function t_list_klaim_pertanggal_keluar()
	{
		$db = db_connect();

		$step 		= $this->request->getVar('step');

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');

		$month 		= $this->request->getVar('month_var');
		$year 		= $this->request->getVar('year_var');

		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			$filter = " AND DATE(tgl_keluar) = '".tgl_save_sql($filterGet)."'";
		}

		// var_dump($filter);die;

		$perPageCount 	= 10;
		$item 			= array();

		$select_rows = $db->query("SELECT 
										DATE(tgl_keluar) AS dateout
									FROM 
										t_klaim
									WHERE
										status_cd = 'normal'
									AND
										progress = '$step'
									AND tgl_keluar >= '$year-$month-01 00:00:00' and tgl_keluar <= '$year-$month-31 23:59:59'

										$filter
									GROUP BY 
										dateout
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT 
					DATE(tgl_keluar) AS dateout
				FROM 
					t_klaim
				WHERE
					status_cd = 'normal'
				AND
					progress = '$step'
				AND tgl_keluar >= '$year-$month-01 00:00:00' and tgl_keluar <= '$year-$month-31 23:59:59'

					$filter
				GROUP BY 
					dateout
				ORDER BY 
					dateout desc
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($dateout) = fetchlist($b);	
				$n++;

				if ($dateout == '')
				{
					continue;
				}

				$tgl_uniq = tgl_unique($dateout);

				$sql_total = "SELECT count(code) as total FROM t_klaim where tgl_keluar >= '$dateout 00:00:00' and tgl_keluar <= '$dateout 23:59:59' and status_cd != 'nullified' and progress = '$step'";
				$result = $db->query($sql_total);
				$row = $result->getRowArray();
				if (isset($row))
				{
				    $total = $row['total'];
				}

				// var_dump($sql_total);die;

				// if ($step == 1)
				// {
				// 	$sql = "SELECT 
				// 				a.code, a.list_code, a.user, a.tanggal_approval, a.tgl_keluar, b.name, a.progress, a.code_group_date_out
				// 			FROM
				// 				log_t_klaim_progress a
				// 			left join
				// 				m_user b on (a.user = b.code)
				// 			WHERE
				// 				a.code_group_date_out = '$tgl_uniq'
				// 			and
				// 				a.status_cd != 'nullified'
				// 			AND
				// 				a.progress = $step
				// 			group by
				// 				a.user
				// 			order BY
				// 				a.tanggal_approval desc
				// 			";
				// }
				// else
				// {
				// 	$sql = "SELECT 
				// 				a.code, a.list_code, a.user, a.tanggal_approval, a.tgl_keluar, b.name, a.progress, a.code_group_date_out
				// 			FROM
				// 				log_t_klaim_progress a
				// 			left join
				// 				m_user b on (a.user = b.code)
				// 			WHERE
				// 				a.code_group_date_out = '$tgl_uniq'
				// 			and
				// 				a.status_cd != 'nullified'
				// 			AND
				// 				a.progress = $step
				// 			order BY
				// 				a.tanggal_approval desc
				// 		";
				// }

				$icon_export = "<img src='".base_url()."/public/tmpassets/assets/sysimg/print.png' class='clickable_print' data-dateout='$dateout' data-step='$step' style='cursor:pointer;' width='18px' height='18px'>";
				$icon_history = "<img src='".base_url()."/public/tmpassets/assets/sysimg/history.png' class='clickable_history' data-dateout='$dateout' data-step='$step' style='cursor:pointer;' width='18px' height='18px'>";

				$ret 	.= "<tr>
								<td class='bleft bright bbottom bpad7' style='text-align:left;' valign='top'>
									<span class='xlink clickable_dateout'  data-dateout='$dateout' data-step='$step'>".tgl_inggris_normal($dateout)."</span>
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;' valign='top'>
									$total
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;' valign='top'>
									$icon_history
								</td>
								<td class='bright bbottom bpad7' style='text-align:center;' valign='top'>
									$icon_export
								</td>
							</tr>
							<tr>
								<td colspan='5' style='padding:0px;' class=''>
									<div id='form_edit_sec_${dateout}'></div>
								</td>
							</tr>
						";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='5' class='bpad7 bleft bright bbottom' style='border:1px solid #DFE3E7;padding:7px;text-align:center;'>
								Data Kosong 
							</td>
						</tr>
					";
		}

        $li = "";
        $li .= "<ul class='pagination justify-content-end style='margin-top:-20px;'>";

        if ($pageNumber != 1)
		{
				$li .= "<li class='page-item previous '>
			            	<a class='page-link t_list_klaim_pertanggal_keluar' data-page='$backPage' data-step='$step' data-filter='$filterGet' href='javascript:;' >
			                    <i class='bx bx-chevron-left'></i>
			                </a>
			            </li>";

				$li .= "<li class='page-item active' aria-current='page'>
			            	<a class='page-link ' href='javascript:;' >
			            		$pageNumber
			        		</a>
			        	</li>";
			
			if ($pageNumber == $pagesCount)
			{
				$li .= "";
			}
			else
			{
				$li .= "
						<li class='page-item next'>
			            	<a class='page-link t_list_klaim_pertanggal_keluar' data-page='$nextPage' data-step='$step' data-filter='$filterGet' href='javascript:;' >
			                    <i class='bx bx-chevron-right'></i>
			            	</a>
			        	</li>
						";
			}
		}
		else
		{
			$li .= "<li class='page-item active' aria-current='page'>
			            	<a class='page-link ' href='javascript:;' >
			            		$pageNumber
			        		</a>
			        	</li>";
			if ($pageNumber == $pagesCount)
			{
				$li .= "";
			}
			else
			{
				$li .= "
						<li class='page-item next'>
			            	<a class='page-link t_list_klaim_pertanggal_keluar' data-page='$nextPage' data-step='$step' data-filter='$filterGet' href='javascript:;' >
			                    <i class='bx bx-chevron-right'></i>
			            	</a>
			        	</li>
						";
			}
		}	

        $li .= "</ul>";

        if ($pagesCount == 0)
		{
			$info_hal = "Hal 0 dari $pagesCount";
			$page_link = "";
		}
		else
		{
			$info_hal = "Hal $pageNumber dari $pagesCount";
			$page_link = $li;
		}

		$detailRet = "";
        $detailRet .= "
					<table border='0px' width='100%' class='$tbl_ar'>
						<tr>
							<td align='left' valign='top' width='50%' style='padding:20px;font-size:11px;color: #9e9e9e;'>
								$info_hal
							</td>
							<td align='right' valign='top' width='50%' style='padding:20px;'>
								$page_link
							</td>
						</tr>
					</table>
				";

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
	}

	public function View_data_klaim(){
		$db = db_connect();

		$dateout 		= $this->request->getVar('dateout');
		$step 			= $this->request->getVar('step');

		$sql = "select * from t_klaim where tgl_keluar >= '$dateout 00:00:00' and tgl_keluar <= '$dateout 23:59:59' and status_cd = 'normal'  order by progress";
		$data['list_data']	    = $this->modeltransc->select_by_id_sql_obj($sql);
		$data['date_show']	    = tgl_indo($dateout);
		$data['dateout']	    = $dateout;
		$data['step']	    	= $step;

		// var_dump($sql);die;
		echo view('print/data_klaim/v_print_pertanggal_keluar_klaim', $data);
	}

	public function excel_data_klaim(){
		$db = db_connect();


		$dateout 		= $this->request->getVar('dateout');
		$step 			= $this->request->getVar('step');

		$sql = "select * from t_klaim where tgl_keluar >= '$dateout 00:00:00' and tgl_keluar <= '$dateout 23:59:59' and status_cd = 'normal' order by progress";
		$data['list_data']	    = $this->modeltransc->select_by_id_sql_obj($sql);
		$data['date_show']	    = tgl_indo($dateout);

		echo view('print/data_klaim/v_excel_pertanggal_keluar_klaim', $data);
	}

}
