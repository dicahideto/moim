<?php

namespace App\Controllers\Transaction\Klaim;
use App\Controllers\BaseController;

class Data_klaim extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function tes(){
		$db = db_connect();

		$obj_id_list = array();

		$sql = "SELECT
					code, 
					description, 
					progress
				from
					m_jaminan
				where 
					status_cd = 'normal'
				AND
					progress != 'kosong'
					";

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $description, $progress) = fetchlist($b);
				echo "$description";
				echo "<br />";
				$str_arr = explode (",", $progress); 
				foreach ($str_arr as $value) {
				  echo "$value <br>";
				}
			}
		}	
	}

	public function index()
	{
		$db = db_connect();

		$step_newpage 	= $this->request->getVar('step_newpage');
		$dateout 		= $this->request->getVar('dateout');

		// ($dateout == '' ? $dateout = date('Y-m-d') : '');

		if ($step_newpage == '')
		{
			$step_newpage = '1';
		}

		$data['hmm'] 			= 'hmmm';
		$data['step_newpage'] 	= $step_newpage;
		$data['dateout'] 		= $dateout;
		$data['code_log'] 		= '';
		// $data['step_newpage'] 	= 'hmmm';

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = $step_newpage ");
		$row = $query->getRow();

		if (isset($row))
		{
		    $data['step_desc'] 		= $row->desc;
		}
		else
		{
			$data['step_desc'] 		= "Selesai";
		}

		return view_one('transaction/klaim/v_data_klaim', $data);
	}


	public function approval_prog(){
		$db = db_connect();

		$step_newpage 	= $this->request->getVar('step_newpage');
		$dateout 		= $this->request->getVar('dateout');
		$code_log 		= $this->request->getVar('code_log');
		$user 			= $this->request->getVar('user');

		$data['hmm'] 			= 'hmmm';
		$data['step_newpage'] 	= $step_newpage;
		$data['dateout'] 		= $dateout;
		$data['code_log'] 		= $code_log;
		$data['user'] 			= $user;

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = $step_newpage ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['step_desc'] 		= $row->desc;
		}

		return view_one('transaction/klaim/v_data_klaim', $data);
	}

	public function t_data_klaim_newone(){
		$db = db_connect();

		$code					= '';
		$initial				= 'KLAIM';
		$table					= 't_klaim';
		$user 					= $_SESSION['codeuser_sess'];

		$udah_ada 				= false;
		$resp['edit']			= '';

		$var_increment	= $initial.date('y').date('m').date('d');
		$sql_cd			= "SELECT
								MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
							FROM
								$table
							WHERE
								left(CODE,11) = '".$var_increment."'";
		$query_cd	= $db->query($sql_cd);
		foreach($query_cd->getResult() as $row_cd){
			$tmpNo_cd	= $row_cd->tmpNo_cd;
			if(is_null($row_cd->tmpNo_cd)){
				$tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
			}
			else{
				$tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4);
				$tmpNo_cd 	= $tmpNo_cd->nomor;
				$tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
			}
		}

		$date_new 	= date('Y-m-d H:i:s');
		$data 		= array('code' 						=> $tmpNo_cd,
							'tgl_keluar'				=> $date_new,
							'code_user_process_zero'	=> $user,
							'code_group_date_out'		=> tgl_unique($date_new),
							);

		$type		= "insert";
		$message	= 'Sukses Memproses Data';
		$table_id	= "code";

		$query				= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);


		$this->save_log_field_progress("'${tmpNo_cd}'", 1, tgl_unique($date_new), $date_new);

		$resp['error'] 		= $query['error'];
		$resp['message']	= $query['message'];
		$resp['status']		= $query['status'];
		$resp['code']		= $tmpNo_cd;

		echo json_encode($resp);
	}

	public function t_data_klaim_newone_html(){
		$db = db_connect();
		$user 		= $_SESSION['codeuser_sess'];
		$step_user 	= $_SESSION['step_sess'];

		$code 	= $this->request->getVar('code');

		$data['code'] = $code;

		$ret = "";

		$sql = "SELECT
					m.code,
					m.ext_id,
					m.person_nm,
					m.jaminan,
					m.org,
					m.ruang,
					m.tgl_masuk,
					m.tgl_keluar,
					m.`status`,
					m.dpjp,
					m.gap,
					m.hari,
					m.jam,
					m.keterangan,
					m.kelengkapan,
					m.tgl_rm_dilengkapi,
					m.kontrol,
					m.status_cd,
					m.progress
					FROM
					t_klaim m
					where
					m.status_cd = 'normal'
					AND
					m.code = '$code'
					ORDER BY m.code desc
				limit 0,1
				";

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $gap, $hari, $jam, $keterangan, $kelengkapan, $tgl_rm_dilengkapi, $kontrol, $status_cd, $progress) = fetchlist($b);
				$n++;

				if ($step_user == '0')
				{
					$linked 	= 'xlink';
					$display	= '';
				}
				else if ($step_user == '1')
				{
					if ($progress == '1')
					{
						$linked 	= 'xlink';
						$display	= '';
					}
					else
					{
						$linked 	= '';
						$display	= 'display:none;';
					}
				}
				else
				{
					$linked 	= '';
					$display	= 'display:none;';
				}

				$code 						= ($code == "" || $code == null ? "-" : $code);
				$ext_id 					= ($ext_id == "" || $ext_id == null ? "-" : $ext_id);
				$person_nm 					= ($person_nm == "" || $person_nm == null ? "-" : $person_nm);
				$jaminan 					= ($jaminan == "" || $jaminan == null ? "-" : $jaminan);
				$org 						= ($org == "" || $org == null ? "-" : $org);
				$ruang 						= ($ruang == "" || $ruang == null ? "-" : $ruang);
				$tgl_masuk 					= (tgl_indo_miring_time($tgl_masuk) == "" || $tgl_masuk == null ? "-" : tgl_indo_miring_time($tgl_masuk));
				$tgl_keluar 				= (tgl_indo_miring_time($tgl_keluar) == "" || $tgl_keluar == null ? "-" : tgl_indo_miring_time($tgl_keluar));
				$status 					= ($status == "" || $status == null ? "-" : $status);
				$keterangan 				= ($keterangan == "" || $status == null ? "-" : $keterangan);
				$dpjp 						= ($dpjp == "" || $dpjp == null ? "-" : $dpjp);
				$kelengkapan 						= ($kelengkapan == "" || $kelengkapan == null ? "-" : $kelengkapan);
				
				
				$kontrol = ($kontrol 		== "" ? "-" : $kontrol);
				$status_cd = ($status_cd 	== "" ? "-" : $status_cd);


				if ($linked 	== 'xlink')
				{
					$clickable_ext_id 		= 'clickable_ext_id';
					$clickable_person 		= 'clickable_person';
					$clickable_jaminan 		= 'clickable_jaminan';
					$clickable_org 			= 'clickable_org';
					$clickable_ruang 		= 'clickable_ruang';
					$clickable_tgl_masuk 	= 'clickable_tgl_masuk';
					$clickable_tgl_keluar 	= 'clickable_tgl_keluar';
					$clickable_keterangan 	= 'clickable_keterangan';
					$clickable_kelengkapan 	= 'clickable_kelengkapan';
					$clickable_dpjp 		= 'clickable_dpjp';
					

				}
				else
				{
					$clickable_ext_id 		= '';
					$clickable_person 		= '';
					$clickable_jaminan 		= '';
					$clickable_org 			= '';
					$clickable_ruang 		= '';
					$clickable_tgl_masuk 	= '';
					$clickable_tgl_keluar 	= '';
					$clickable_keterangan 	= '';
					$clickable_dpjp 		= '';
					$clickable_kelengkapan 		= '';
					
				}

				if ($progress != '100')
				{
					$linked_keterangan = 'xlink';
					$clickable_keterangan 	= 'clickable_keterangan';
				}
				else
				{
					$linked_keterangan = '';
					$clickable_keterangan 	= '';
				}

				$del_act 	= "<img src='".base_url()."/public/tmpassets/assets/sysimg/delete.png' style='width:18px;height:18px;cursor:pointer;' data-code='$code' class='clickable_del xlink' id='clickable_del_${code}'/>";

				$ret .= "<tr id='line_${code}'>
						<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;${display}'>
							$del_act
						</td>
						<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:80px;'>
							<input type='checkbox' value='$code' class='radio_check ' name='name_checked_radio' />
						</td>
						<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;'>
							new
						</td>
						<td class='bleft bright bbottom bpad7 ' style='cursor:pointer;text-align:left;min-width:150px;' data-code='$code' >
							<span class='$clickable_ext_id ${linked}' id='clickable_ext_id_${code}' data-code='$code' data-val='$ext_id'>$ext_id</span>
							<span class='clickable_ext_id_form' id='clickable_ext_id_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_person ${linked}' id='clickable_person_${code}' data-code='$code' data-val='$person_nm'>$person_nm</span>
							<span class='clickable_person_form' id='clickable_person_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_jaminan ${linked}' id='clickable_jaminan_${code}' data-code='$code' data-val='$jaminan'>$jaminan</span>
							<span class='clickable_jaminan_form' id='clickable_jaminan_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_org ${linked}' id='clickable_org_${code}' data-code='$code' data-val='$org'>$org</span>
							<span class='clickable_org_form' id='clickable_org_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code'  >
							<span class='$clickable_ruang ${linked}' id='clickable_ruang_${code}' data-code='$code' data-val='$ruang'>$ruang</span>
							<span class='clickable_ruang_form' id='clickable_ruang_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_tgl_masuk ${linked}' id='clickable_tgl_masuk_${code}' data-code='$code' data-val='$tgl_masuk' data-mrn='$ext_id'>$tgl_masuk</span>
							<span class='clickable_tgl_masuk_form' id='clickable_tgl_masuk_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_tgl_keluar ${linked}' id='clickable_tgl_keluar_${code}' data-code='$code' data-val='$tgl_keluar'>$tgl_keluar</span>
							<span class='clickable_tgl_keluar_form' id='clickable_tgl_keluar_form_${code}'></span>
						</td>
						
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_dpjp ${linked}' id='clickable_dpjp_${code}' data-code='$code' data-val='$dpjp'>$dpjp</span>
							<span class='clickable_dpjp_form' id='clickable_dpjp_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_kelengkapan ${linked}' id='clickable_kelengkapan_${code}' data-code='$code' data-val='$kelengkapan'>$kelengkapan</span>
							<span class='clickable_kelengkapan_form' id='clickable_kelengkapan_form_${code}'></span>
						</td>
						<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
							<span class='$clickable_keterangan ${linked} ${linked_keterangan}' id='clickable_keterangan_${code}' data-code='$code' data-val='$keterangan'>$keterangan</span>
							<span class='clickable_keterangan_form' id='clickable_keterangan_form_${code}'></span>
						</td>
						
					</tr>
				 	<tr>
						<td colspan='13' style='padding:0px;' class=''>
							<div id='form_edit_${code}'></div>
						</td>
					</tr>";
			}
		}

        return $ret;
	}


	public function t_data_klaim(){
		$db = db_connect();
		$user 		= $_SESSION['codeuser_sess'];
		$step_user 	= $_SESSION['step_sess'];

		$filterGet 			= $this->request->getVar('filter');
		$pageGet 			= $this->request->getVar('page');
		$step_newpage 	 	= $this->request->getVar('step_newpage');
		$dateout 	 		= $this->request->getVar('dateout');
		$code_log 	 		= $this->request->getVar('code_log');
		$tbl 	 			= $this->request->getVar('tbl');
		$tbl_ar				= $tbl."_arrah";
		$ret 				= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}

		if ($filterGet == '') {
			$filter = "";
		} else {
			$filter = "";
			// $filter = " AND m.name LIKE '%$filterGet%'";
		}

		if ($code_log != '')
		{
			if ($step_newpage == 1)
			{
				$sqk = $db->query("select 
									user, code_group_date_out, progress
									from
									log_t_klaim_progress
									where code = '$code_log'
									");
				$row = $sqk->getRow();
				if (isset($row))
				{
				    $usere 					=  $row->user;
				    $code_group_date_oute 	=  $row->code_group_date_out;
				    $progresse 				=  $row->progress;
				    
				    $obj_id_list = array();

					$sq = "SELECT 
								list_code
							FROM
								log_t_klaim_progress
							where 
								user = '$usere'
							and
								code_group_date_out = '$code_group_date_oute'
							and
								progress = '$progresse' 
							and 
								status_cd != 'nullified'
							";

					$result_sq	= $db->query($sq);
				
					
					if ($result_sq->getNumRows() > 0)
					{
						foreach (fetchloopsql($result_sq) as $b)
						{
							list($list_code) = fetchlist($b);	
							
							if( in_array( $list_code ,$obj_id_list ) )
							{
								
							}
							else
							{
								array_push($obj_id_list,$list_code);
							}
						}
					}

					$list_code = implode(",",$obj_id_list);
					
				}

				$query_list_code = " AND `code` in ($list_code)";
				
			}
			else
			{
				$query = $db->query("select list_code from log_t_klaim_progress where code = '$code_log'");
				$row = $query->getRow();
				if (isset($row))
				{
				    $list_code =  $row->list_code;
				    $query_list_code = " AND `code` in (${list_code})";
				}
			}
		}
		else
		{
			$query_list_code = '';
		}

		$perPageCount 	= 100;
		$item 			= array();

		$qq = "
										SELECT
										m.code,
										m.ext_id,
										m.person_nm,
										m.jaminan,
										m.org,
										m.ruang,
										m.tgl_masuk,
										m.tgl_keluar,
										m.`status`,
										m.dpjp,
										m.gap,
										m.hari,
										m.jam,
										m.keterangan,
										m.kelengkapan,
										m.tgl_rm_dilengkapi,
										m.kontrol,
										m.status_cd,
										m.progress
										FROM
										t_klaim m
										where
										m.status_cd = 'normal'
										and
										m.progress = '$step_newpage'
										$query_list_code
										AND
										m.tgl_keluar >= '$dateout 00:00:00' and m.tgl_keluar <= '$dateout 23:59:59'
										ORDER BY m.code desc
									";
		$select_rows = $db->query($qq);

		// var_dump($qq);die;
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);

		$sql = "SELECT
					m.code,
					m.ext_id,
					m.person_nm,
					m.jaminan,
					m.org,
					m.ruang,
					m.tgl_masuk,
					m.tgl_keluar,
					m.`status`,
					m.dpjp,
					m.gap,
					m.hari,
					m.jam,
					m.keterangan,
					m.kelengkapan,
					m.tgl_rm_dilengkapi,
					m.kontrol,
					m.status_cd,
					m.progress
					FROM
					t_klaim m
					where
					m.status_cd = 'normal'
					AND
					m.progress = '$step_newpage'
					$query_list_code
					AND
					m.tgl_keluar >= '$dateout 00:00:00' and m.tgl_keluar <= '$dateout 23:59:59'
					ORDER BY m.code desc
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . "
				";

		// var_dump($sql);die;

		$result_sql	= $db->query($sql);
		$data 		= array();


		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $gap, $hari, $jam, $keterangan, $kelengkapan, $tgl_rm_dilengkapi, $kontrol, $status_cd, $progress) = fetchlist($b);	
				$n++;

				if ($step_user == '0')
				{
					$linked 	= 'xlink';
					$cursor 	= 'cursor:pointer;';

				}
				else if ($step_user == '1')
				{
					if ($progress == '1')
					{
						$linked 	= 'xlink';
						$cursor 	= 'cursor:pointer;';

					}
					else
					{
						$linked 	= '';
						$cursor 	= '';

					}
				}
				else
				{
					$linked 	= '';
					$cursor 	= '';

				}

				$code 						= ($code == "" || $code == null ? "-" : $code);
				$ext_id 					= ($ext_id == "" || $ext_id == null ? "-" : $ext_id);
				$person_nm 					= ($person_nm == "" || $person_nm == null ? "-" : $person_nm);
				$jaminan 					= ($jaminan == "" || $jaminan == null ? "-" : $jaminan);
				$org 						= ($org == "" || $org == null ? "-" : $org);
				$ruang 						= ($ruang == "" || $ruang == null ? "-" : $ruang);
				$tgl_masuk 					= (tgl_indo_miring_time($tgl_masuk) == "" || $tgl_masuk == null ? "-" : tgl_indo_miring_time($tgl_masuk));
				$tgl_keluar 				= (tgl_indo_miring_time($tgl_keluar) == "" || $tgl_keluar == null ? "-" : tgl_indo_miring_time($tgl_keluar));
				$status 					= ($status == "" || $status == null ? "-" : $status);
				$dpjp 						= ($dpjp == "" || $dpjp == null ? "-" : $dpjp);
				$kelengkapan 						= ($kelengkapan == "" || $dpjp == null ? "-" : $kelengkapan);
				$keterangan 						= ($keterangan == "" || $dpjp == null ? "-" : $keterangan);
				
				$kontrol = ($kontrol 		== "" ? "-" : $kontrol);
				$status_cd = ($status_cd 	== "" ? "-" : $status_cd);

				if ($linked 	== 'xlink')
				{
					$display	= '';

					$clickable_ext_id 		= 'clickable_ext_id';
					$clickable_person 		= 'clickable_person';
					$clickable_jaminan 		= 'clickable_jaminan';
					$clickable_org 			= 'clickable_org';
					$clickable_ruang 		= 'clickable_ruang';
					$clickable_tgl_masuk 	= 'clickable_tgl_masuk';
					$clickable_tgl_keluar 	= 'clickable_tgl_keluar';
					$clickable_dpjp 		= 'clickable_dpjp';
					$clickable_kelengkapan 	= 'clickable_kelengkapan';
					$clickable_keterangan 	= 'clickable_keterangan';
				}
				else
				{
					$display	= 'display:none;';

					$clickable_ext_id 		= '';
					$clickable_person 		= '';
					$clickable_jaminan 		= '';
					$clickable_org 			= '';
					$clickable_ruang 		= '';
					$clickable_tgl_masuk 	= '';
					$clickable_tgl_keluar 	= '';
					$clickable_dpjp 		= '';
					$clickable_keterangan 	= '';
					$clickable_kelengkapan 	= '';
					
				}

				if ($progress != '100')
				{
					$linked_keterangan = 'xlink';
					$clickable_keterangan 	= 'clickable_keterangan';
				}
				else
				{
					$linked_keterangan = '';
					$clickable_keterangan 	= '';
				}

				$del_act 					= "<img src='".base_url()."/public/tmpassets/assets/sysimg/delete.png' style='width:18px;height:18px;cursor:pointer;' data-code='$code' class='clickable_del xlink' id='clickable_del_${code}'/>";
				$ret 	.= "
							<tr id='line_${code}'>
								<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;${display}'>
									$del_act
								</td>
								<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:80px;'>
									<input type='checkbox' value='$code' class='radio_check ' name='name_checked_radio' />
								</td>
								<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;'>
									$n
								</td>
								<td class='bleft bright bbottom bpad7 ' style='${cursor}text-align:left;min-width:150px;' data-code='$code' >
									<span class='$clickable_ext_id ${linked}' id='clickable_ext_id_${code}' data-code='$code' data-val='$ext_id'>$ext_id</span>
									<span class='clickable_ext_id_form' id='clickable_ext_id_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_person ${linked}' id='clickable_person_${code}' data-code='$code' data-val='$person_nm'>$person_nm</span>
									<span class='clickable_person_form' id='clickable_person_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_jaminan ${linked}' id='clickable_jaminan_${code}' data-code='$code' data-val='$jaminan'>$jaminan</span>
									<span class='clickable_jaminan_form' id='clickable_jaminan_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_org ${linked}' id='clickable_org_${code}' data-code='$code' data-val='$org'>$org</span>
									<span class='clickable_org_form' id='clickable_org_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code'  >
									<span class='$clickable_ruang ${linked}' id='clickable_ruang_${code}' data-code='$code' data-val='$ruang'>$ruang</span>
									<span class='clickable_ruang_form' id='clickable_ruang_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_tgl_masuk ${linked}' id='clickable_tgl_masuk_${code}' data-code='$code' data-val='$tgl_masuk'>$tgl_masuk</span>
									<span class='clickable_tgl_masuk_form' id='clickable_tgl_masuk_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_tgl_keluar ${linked}' id='clickable_tgl_keluar_${code}' data-code='$code' data-val='$tgl_keluar' data-mrn='$ext_id'>$tgl_keluar</span>
									<span class='clickable_tgl_keluar_form' id='clickable_tgl_keluar_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_dpjp ${linked}' id='clickable_dpjp_${code}' data-code='$code' data-val='$dpjp'>$dpjp</span>
									<span class='clickable_dpjp_form' id='clickable_dpjp_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_kelengkapan ${linked}' id='clickable_kelengkapan_${code}' data-code='$code' data-val='$kelengkapan'>$kelengkapan</span>
									<span class='clickable_kelengkapan_form' id='clickable_kelengkapan_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='cursor:pointer;text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_keterangan ${linked} ${linked_keterangan}' id='clickable_keterangan_${code}' data-code='$code' data-val='$keterangan'>$keterangan</span>
									<span class='clickable_keterangan_form' id='clickable_keterangan_form_${code}'></span>
								</td>
							</tr>
						 	<tr>
								<td colspan='13' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr class='data_klaim_kosong'>
							<td colspan='13' style='padding:10px;' class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = "";



		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;

		echo json_encode($resp);
	}


	public function form_fill(){
		$db = db_connect();

		$val 	= $this->request->getVar('val');

		if ($val == '-')
		{
			$val = '';
		}

		$ret 	= '';

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<input type='text' class='form-control form_text_embbed_table' value='$val'>";
		$ret .= "</span>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}

	public function form_fill_date(){
		$db = db_connect();

		$val 	= $this->request->getVar('val');

		if ($val == '-')
		{
			$val = '';
		}

		$ret 	= '';

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<input type='text'  class='form-control form_text_embbed_table' id='jqdatetimepicker' value='$val'>";
		$ret .= "</span>";
		$ret .= "<script>";
		$ret .= "$('#jqdatetimepicker').datetimepicker({
					  format:'d/m/Y H:i',
					  lang:'id'
					});
		        ";
		$ret .= "</script>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}


	public function form_fill_select_lite(){
		$db = db_connect();

		$val 			= $this->request->getVar('val');

		$ret 			= "";

		if ($val == '')
		{

		}
		else
		{

		}

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<select class='select2 form-control form_text_embbed_table' id='selectselect' name='selectselect' >
					<option ></option>
					<option value='JKN'>JKN</option>
					<option value='UMUM'>UMUM</option>
					<option value='PERUSAHAAN'>PERUSAHAAN</option>
				</select>

				<script>
					$('.select2').select2({
						dropdownAutoWidth: true,
						allowClear: true,
						placeholder: '',
						width: '100%'
					});
				</script>
				</span>

				";

		return $ret;
	}

	public function form_fill_select(){
		$db = db_connect();

		$val 			= $this->request->getVar('val');
		$url_select 	= $this->request->getVar('url_select');
		// $url_select 	= "Dev/Form_one/fetch_select_loadmore";

		$ret 	= '';

		if ($val == '')
		{
			$a = "<option>-</option>";
		}
		else
		{
			$a = "<option value='$val'>${val}</option>";
		}

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<select class='select2 form-control form_text_embbed_table' id='select2loadmore' name='select2loadmore' >";
		$ret .= $a;
        $ret .=  "</select>";
		
		$ret .= "<script>";
		$ret .= "
					$('#select2loadmore').select2({
					    placeholder: '',
					    allowClear: true,
					    container: '.form_text_embbed_table',
					    ajax: {
					        url: '${url_select}',
					        dataType: 'json',
					        delay: 250,
					        cache: false,
					        data: function (params) {
					            return {
					                term: params.term,
					                page: params.page || 1,
					            };
					        },
					        processResults: function(data, params) {
					            console.log(data);
					            var page = params.page || 1;
					            return {
					                results: $.map(data, function (item) { return {id: item.col, text: item.col}}),
					                pagination: {
					                    more: (page * 10) <= data[0].total_count
					                }
					            };
					        },
					    }
					});

		        ";
		$ret .= "</script>";

		$ret .= "</span>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}

	
	public function fetch_select_jaminan(){
		$db 			= db_connect();

		$page			= $this->request->getVar('page');
		$term			= $this->request->getVar('term');
	    $resultCount 	= 10;
	    $end 			= ($page - 1) * $resultCount;       
	    $start 			= $end + $resultCount;

	    $stmt 	= $db->query("SELECT description FROM m_jaminan WHERE description != '' and description LIKE '%".$term."%' LIMIT {$end},{$start}");
	  	
	    $count 	= $stmt->getNumRows();
        $data 	= [];
        if ($count > 0)
        {
        	$results = $stmt->getResultArray();
			foreach ($results as $row)
			{
			    $data[] = ['id'=>$row['description'], 'col'=>$row['description'], 'total_count'=>$count];
			}
			echo json_encode($data);
        }
        else
        {
        	$empty[] = ['id'=>'', 'col'=>'', 'total_count'=>'']; 
            echo json_encode($empty);
        }
	}

	
	public function fetch_select_kelengkapan(){
		$db 			= db_connect();

		$page			= $this->request->getVar('page');
		$term			= $this->request->getVar('term');
	    $resultCount 	= 10;
	    $end 			= ($page - 1) * $resultCount;       
	    $start 			= $end + $resultCount;

	    $stmt 	= $db->query("SELECT description FROM m_kelengkapan WHERE description != '' and description LIKE '%".$term."%' LIMIT {$end},{$start}");
	  	
	    $count 	= $stmt->getNumRows();
        $data 	= [];
        if ($count > 0)
        {
        	$results = $stmt->getResultArray();
			foreach ($results as $row)
			{
			    $data[] = ['id'=>$row['description'], 'col'=>$row['description'], 'total_count'=>$count];
			}
			echo json_encode($data);
        }
        else
        {
        	$empty[] = ['id'=>'', 'col'=>'', 'total_count'=>'']; 
            echo json_encode($empty);
        }
	}

	public function save_form_fill(){
		$db 	= db_connect();

		$value 		= $this->request->getVar('value');
		$code 		= $this->request->getVar('code');
		$column 	= $this->request->getVar('column');
		$type_val 	= $this->request->getVar('type_val');

		if ($type_val == 'date')
		{
			$value 	= tgl_time_save_form_miring_sql($value);

			if ($column == 'tgl_keluar')
			{	
				$query = $db->query("SELECT ext_id, code from t_klaim where code = '$code' ");
				$row = $query->getRow();
				if (isset($row))
				{
				    $ext_id 	= $row->ext_id;
				    $code_new 	= $row->code;
				    
					$query_check = $db->query("SELECT code from t_klaim where tgl_keluar = '$value' and ext_id = '$ext_id' ");
					$row_check = $query_check->getRow();
					if (isset($row_check))
					{
						 $code_old 	= $row->code;

						 if ($code_old != $code_new)
						 {
				 			$resp['error'] 		='error';
							$resp['message']	= 'Data Pasien Sudah Ada Pada Tanggal Keluar Terbaru';
							$resp['status']		= 'error';
							echo json_encode($resp);
							exit();
						 }
					}
				}
			}
			
		}

		$initial				= 'KLAIM';
		$table					= 't_klaim';

		$udah_ada 				= false;
		$resp['edit']			= '';

		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){

			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";

			$sql_get_last_val = "SELECT $column from $table WHERE $table_id='$code' limit 0,1";
			$result_sql_get_last_val = $db->query($sql_get_last_val);
			$row_result_sql_get_last_val = $result_sql_get_last_val->getRowArray();
			if (isset($row_result_sql_get_last_val))
			{
			    $last_val = $row_result_sql_get_last_val[$column];
			}
			else{
				$last_val = '';
			}

			if ($column != 'tgl_keluar')
			{
				$data 		= array($column 			=> $value
								);	
			}
			else
			{
				$data 		= array($column 			=> $value,
									'code_group_date_out'	=> tgl_unique($value)
								);	
			}
			

			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];

			if ($query['status'] == 'success')
			{
				$this->save_log_field($table, $code, $column, $value, $last_val);
			}
		}
		else
		{

		}

		echo json_encode($resp);

	}







	public function save_log_field($table, $code, $column, $newvalue, $lastvalue){
		$db 	= db_connect();

		$codenew				= '';
		$initial				= 'LOGTKF';
		$table					= 'log_t_klaim_field';
		$user 					= $_SESSION['codeuser_sess'];

		$udah_ada 				= false;
		$resp['edit']			= '';

		$var_increment	= $initial.date('y').date('m').date('d');
        $sql_cd			= "SELECT
                                MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
                            FROM
                                $table
                            WHERE
                                left(CODE,12) = '".$var_increment."'";
        $query_cd	= $db->query($sql_cd);
        foreach($query_cd->getResult() as $row_cd){
            $tmpNo_cd	= $row_cd->tmpNo_cd;
            if(is_null($row_cd->tmpNo_cd)){
                $tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
            }
            else{
                $tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4);
                $tmpNo_cd 	= $tmpNo_cd->nomor;
                $tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
            }
        }

        $data 		= array(
	            'code'			        => $tmpNo_cd,
	            'column_db'			        => $column,
	            'last_value'			        => $lastvalue,
	            'new_value'			        => $newvalue,
	            'user'			    => $user
            );

        $type		= "insert";
        $message	= 'Sukses Memproses Data';
        $table_id	= "code";

        $this->model->m_iud($type, $data, $table, $table_id ,$codenew, $message);

	}

	public function del_data_klaim(){
		$db 	= db_connect();

		$code 		= $this->request->getVar('code');

		$initial				= 'KLAIM';
		$table					= 't_klaim';

		$udah_ada 				= false;
		$resp['edit']			= '';

		$q_cek	= $db->query("SELECT code, tgl_keluar, progress FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		$row 	= $q_cek->getRow();
		if (isset($row))
		{
		    $dateout 	= tgl_save_sql($row->tgl_keluar);
		    $progress 	= $row->progress;
		}

		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			$data 		= array('status_cd' 			=> 'nullified'
								);

			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";

			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);

			$query_log			= $this->model->m_iud($type, $data, 'log_t_klaim_progress', 'list_code' , "'${code}'", $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];

			$count_sql			= $db->query("SELECT code FROM ".$table." WHERE tgl_keluar >= '$dateout 00:00:00' and tgl_keluar <= '$dateout 23:59:59' and status_cd != 'nullified'");
			$res_count_sql		= $count_sql->getNumRows();

			$data_kosong	= "
								<tr class='data_klaim_kosong'>
									<td colspan='13' style='padding:10px;' class='bpad7 bleft bright bbottom'>
										Data Kosong
									</td>
								</tr>
							";

			if ($res_count_sql > 0)
			{
				$resp['row_kosong']		= '';
			}
			else
			{
				$resp['row_kosong']		= $data_kosong;
			}

			$resp['dsfdsa'] = $dateout;
		}
		else
		{

		}

		echo json_encode($resp);
	}


	public function klaim_process(){
		$db 	= db_connect();

		$code 						= $this->request->getVar('listcode');
		$code_group_date_out 		= $this->request->getVar('code_group_date_out');
		$dateout 					= $this->request->getVar('dateout');
		$user 						= $_SESSION['codeuser_sess'];
		$step 						= $_SESSION['step_sess'];

		$initial				= 'KLAIM';
		$table					= 't_klaim';

		$udah_ada 				= false;
		$resp['edit']			= '';
		
		if ($step == '0')
		{
			$sql_get_stat = "SELECT
								progress
							FROM
								t_klaim
							where
								code in ($code)
							group by progress
							LIMIT 0,1
							";

			$result_sql	= $db->query($sql_get_stat);

			$n = 0;
			$prog_next = 0;
			if ($result_sql->getNumRows() > 0)
			{
				foreach (fetchloopsql($result_sql) as $b)
				{
					list($progress_val) = fetchlist($b);
					if ($progress_val == '')
					{
						$prog = '1';
					}
					else if ($progress_val == '5')
					{
						$prog = '100';
					}
					else
					{
						$prog_next = 1;
						$prog = $progress_val+1;
					}
				}
			}
			else
			{
				$prog = '1';
			}
		}
		else
		{
			$prog_next = 1;
			$prog = $step+1;
		}

		if ($prog == '1')
		{
			$code_user_process = 'code_user_process_one';
		}
		else if ($prog == '2')
		{
			$code_user_process = 'code_user_process_one';
		}
		else if ($prog == '3')
		{
			$code_user_process = 'code_user_process_two';
		}
		else if ($prog == '4')
		{
			$code_user_process = 'code_user_process_three';
		}
		else if ($prog == '5')
		{
			$code_user_process = 'code_user_process_four';
		}
		else if ($prog == '100')
		{
			$code_user_process = '';
		}

		$sql_check_jaminan = "SELECT m.code 
									FROM 
									t_klaim m 
									where m.code in ($code)
									and 
									jaminan is null
									LIMIT 0,1
									";
		
		$query = $db->query($sql_check_jaminan);
		$row = $query->getRow();
		if (isset($row))
		{
			$resp['error'] 		= 'error';
			$resp['message']	= 'Mohon Data Jaminan untuk diisi';
			$resp['status']		= 'error';
		}
		else
		{
			$searchForValue = ',';
			
			$var = explode(',',$code);
			foreach($var as $code_id)
			{
				$sql_check = "SELECT m.jaminan 
									FROM 
									t_klaim m 
									where m.code in ($code_id)
									LIMIT 0,1
									";
		
				$query = $db->query($sql_check);
				$row = $query->getRow();
				if (isset($row))
				{
					$jaminan_fetch = $row->jaminan;

					if ($prog_next == 1)
					{
						if ($jaminan_fetch == 'JKN')
						{
							// $prog = '1'
							// $prog = '2'
							// $prog = '3'
							// $prog = '4'
							// $prog = '5'
						}
						else if ($jaminan_fetch == 'JAMINAN KESEHATAN NASIONAL')
						{

						}
						else if ($jaminan_fetch == 'UMUM')
						{
							// $prog = '1'
							if ($prog == '1')
							{

							}
							else
							{
								$prog = '100';
							}
						}
						else if ($jaminan_fetch == 'PERUSAHAAN')
						{
							// $prog = '1'
							// $prog = '2'
							// $prog = '5'

							if ($prog == '1')
							{

							}
							else
							{
								if ($prog == '3')
								{
									$prog = '5';
								}
								else if ($prog == '4')
								{
									$prog = '5';
								}
							}
						}
					}
					else
					{

					}
				}
			}
			
			if ($prog == '100')
			{
				$query = "UPDATE t_klaim
						SET progress = '$prog'
						WHERE code IN ($code)";
			}
			else
			{
				$query = "UPDATE t_klaim
						SET $code_user_process = '$user', progress = '$prog'
						WHERE code IN ($code)";
			}

			// var_dump($prog);die;
			$result = $db->query($query);

			if ($result)
			{
				$this->save_log_field_progress($code, $prog, $code_group_date_out, $dateout);
				$resp['message'] = 'success';
			}
			else
			{
				$resp['message'] = 'error';
			}
		}

		echo json_encode($resp);
	}


	public function save_log_field_progress($code, $newvalue, $code_group_date_out, $dateout){
		$db 	= db_connect();

		$codenew				= '';
		$initial				= 'LOGTKP';
		$table					= 'log_t_klaim_progress';
		$user 					= $_SESSION['codeuser_sess'];

		$udah_ada 				= false;
		$resp['edit']			= '';

		$var_increment	= $initial.date('y').date('m').date('d');
        $sql_cd			= "SELECT
                                MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
                            FROM
                                $table
                            WHERE
                                left(CODE,12) = '".$var_increment."'";
        $query_cd	= $db->query($sql_cd);
        foreach($query_cd->getResult() as $row_cd){
            $tmpNo_cd	= $row_cd->tmpNo_cd;
            if(is_null($row_cd->tmpNo_cd)){
                $tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
            }
            else{
                $tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4);
                $tmpNo_cd 	= $tmpNo_cd->nomor;
                $tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
            }
        }

        $data 		= array(
	            'code'			        					=> $tmpNo_cd,
	            'code_group_date_out'			        	=> $code_group_date_out,
	            'tgl_keluar'			        			=> $dateout,
	            'list_code'			        				=> $code,
	            'progress'			        				=> $newvalue,
	            'user'			    						=> $user
            );

        $type		= "insert";
        $message	= 'Sukses Memproses Data';
        $table_id	= "code";

        $this->model->m_iud($type, $data, $table, $table_id ,$codenew, $message);
	}


}
