<?php

namespace App\Controllers\Transaction\Klaim;
use App\Controllers\BaseController;

class List_import_klaim extends BaseController
{
	public function __construct(){
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index(){
		return view_one('Transaction/klaim/v_list_import_klaim');
	}

    public function t_data_klaim(){
		$db         = db_connect();
		$user 		= $_SESSION['codeuser_sess'];
		$step_user 	= $_SESSION['step_sess'];

		$filterGet 			= $this->request->getVar('filter');
		$pageGet 			= $this->request->getVar('page');
		$step_newpage 	 	= $this->request->getVar('step_newpage');
		$tbl 	 			= $this->request->getVar('tbl');
		$tbl_ar				= $tbl."_arrah";
		$ret 				= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			$filter = "";
			// $filter = " AND m.name LIKE '%$filterGet%'";
		}

		$perPageCount 	= 100;
		$item 			= array();

		$select_rows = $db->query("
										SELECT
										m.code,
										m.ext_id,
										m.person_nm,
										m.jaminan,
										m.org,
										m.ruang,
										m.tgl_masuk,
										m.tgl_keluar,
										m.`status`,
										m.dpjp,
										m.gap,
										m.hari,
										m.jam,
										m.keterangan,
										m.kelengkapan,
										m.tgl_rm_dilengkapi,
										m.kontrol,
										m.status_cd
										
										FROM
										temp_t_klaim m
										where 
										m.status_cd = 'normal'
                                        and
                                        m.code_user_process_one = '$user'
										ORDER BY m.code desc
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT
					m.code,
					m.ext_id,
					m.person_nm,
					m.jaminan,
					m.org,
					m.ruang,
					m.tgl_masuk,
					m.tgl_keluar,
					m.`status`,
					m.dpjp,
					m.gap,
					m.hari,
					m.jam,
					m.keterangan,
					m.kelengkapan,
					m.tgl_rm_dilengkapi,
					m.kontrol,
					m.status_cd
					FROM
					temp_t_klaim m
					where 
					m.status_cd = 'normal'
                    and 
                    m.code_user_process_one = '$user'
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();
		

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($code, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $gap, $hari, $jam, $keterangan, $kelengkapan, $tgl_rm_dilengkapi, $kontrol, $status_cd) = fetchlist($b);	
				$n++;

				if ($step_user == '0')
				{
					$linked 	= 'xlink';
					$cursor 	= 'cursor:pointer;';
					
				}
				else if ($step_user == '1')
				{
                    $linked 	= 'xlink';
                    $cursor 	= 'cursor:pointer;';
				}
				else
				{
					$linked 	= '';
					$cursor 	= '';
					
				}

                $query_check_duplikasi = $db->query("SELECT code from t_klaim where ext_id = '$ext_id' and tgl_keluar = '$tgl_keluar' ");
                $row_duplikasi = $query_check_duplikasi->getRow();
                if (isset($row_duplikasi))
                {
                    $font_color_duplikasi = 'background-color:#ffe6e6;';
                }
                else
                {
                    $font_color_duplikasi = '';
                }



				$code 						= ($code == "" || $code == null ? "-" : $code);
				$ext_id 					= ($ext_id == "" || $code == null ? "-" : $ext_id);
				$person_nm 					= ($person_nm == "" || $code == null ? "-" : $person_nm);
				$jaminan 					= ($jaminan == "" || $code == null ? "-" : $jaminan);
				$org 						= ($org == "" || $code == null ? "-" : $org);
				$ruang 						= ($ruang == "" || $code == null ? "-" : $ruang);
				$tgl_masuk 					= ($tgl_masuk == "" || $code == null ? "-" : $tgl_masuk);
				$tgl_keluar 				= ($tgl_keluar == "" || $code == null ? "-" : $tgl_keluar);
				$status 					= ($status == "" || $code == null ? "-" : $status);
				$dpjp 						= ($dpjp == "" || $code == null ? "-" : $dpjp);
				
				$keterangan 				= ($keterangan == "" || $code == null ? "-" : $keterangan);
				
				$kontrol = ($kontrol 		== "" ? "-" : $kontrol);
				$status_cd = ($status_cd 	== "" ? "-" : $status_cd);

				if ($linked 	== 'xlink')
				{
					$display	= '';

					$clickable_ext_id 		= 'clickable_ext_id';
					$clickable_person 		= 'clickable_person';
					$clickable_jaminan 		= 'clickable_jaminan';
					$clickable_org 			= 'clickable_org';
					$clickable_ruang 		= 'clickable_ruang';
					$clickable_tgl_masuk 	= 'clickable_tgl_masuk';
					$clickable_tgl_keluar 	= 'clickable_tgl_keluar';
					$clickable_dpjp 		= 'clickable_dpjp';
					$clickable_keterangan 	= 'clickable_keterangan';
					
				}
				else
				{
					$display	= 'display:none;';
					
					$clickable_ext_id 		= '';
					$clickable_person 		= '';
					$clickable_jaminan 		= '';
					$clickable_org 			= '';
					$clickable_ruang 		= '';
					$clickable_tgl_masuk 	= '';
					$clickable_tgl_keluar 	= '';
					$clickable_dpjp 		= '';
					$clickable_keterangan 	= '';
				}

				$del_act 					= "<img src='".base_url()."/public/tmpassets/assets/sysimg/delete.png' style='width:18px;height:18px;cursor:pointer;' data-code='$code' class='clickable_del xlink' id='clickable_del_${code}'/>";
				$ret 	.= "
							<tr id='line_${code}'>
								<td class='bleft bright bbottom bpad7' style='text-align:center;min-width:50px;${display}'>
									$del_act
								</td>
								<td class='bleft bright bbottom bpad7' style='${font_color_duplikasi}text-align:center;min-width:80px;'>
									<input type='checkbox' value='$code' class='radio_check ' name='name_checked_radio' />
								</td>
								<td class='bleft bright bbottom bpad7' style='${font_color_duplikasi}text-align:center;min-width:50px;'>
									$n
								</td>
								<td class='bleft bright bbottom bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:150px;' data-code='$code' >
									<span class='$clickable_ext_id ${linked}' id='clickable_ext_id_${code}' data-code='$code' data-val='$ext_id'>$ext_id</span>
									<span class='clickable_ext_id_form' id='clickable_ext_id_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_person ${linked}' id='clickable_person_${code}' data-code='$code' data-val='$person_nm'>$person_nm</span>
									<span class='clickable_person_form' id='clickable_person_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_jaminan ${linked}' id='clickable_jaminan_${code}' data-code='$code' data-val='$jaminan'>$jaminan</span>
									<span class='clickable_jaminan_form' id='clickable_jaminan_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_org ${linked}' id='clickable_org_${code}' data-code='$code' data-val='$org'>$org</span>
									<span class='clickable_org_form' id='clickable_org_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code'  >
									<span class='$clickable_ruang ${linked}' id='clickable_ruang_${code}' data-code='$code' data-val='$ruang'>$ruang</span>
									<span class='clickable_ruang_form' id='clickable_ruang_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_tgl_masuk ${linked}' id='clickable_tgl_masuk_${code}' data-code='$code' data-val='$tgl_masuk'>$tgl_masuk</span>
									<span class='clickable_tgl_masuk_form' id='clickable_tgl_masuk_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_tgl_keluar ${linked}' id='clickable_tgl_keluar_${code}' data-code='$code' data-val='$tgl_keluar'>$tgl_keluar</span>
									<span class='clickable_tgl_keluar_form' id='clickable_tgl_keluar_form_${code}'></span>
								</td>
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_dpjp ${linked}' id='clickable_dpjp_${code}' data-code='$code' data-val='$dpjp'>$dpjp</span>
									<span class='clickable_dpjp_form' id='clickable_dpjp_form_${code}'></span>
								</td>
						
								<td class=' bbottom bright bpad7 ' style='${font_color_duplikasi}${cursor}text-align:left;min-width:250px;' data-code='$code' >
									<span class='$clickable_keterangan ${linked}' id='clickable_keterangan_${code}' data-code='$code' data-val='$keterangan'>$keterangan</span>
									<span class='clickable_keterangan_form' id='clickable_keterangan_form_${code}'></span>
								</td>
							</tr>
						 	<tr>
								<td colspan='18' style='padding:0px;' class=''>
									<div id='form_edit_${code}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='18' style='padding:10px;' class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = "";

       

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
	}



    public function del_data_klaim(){
		$db 	= db_connect();

		$code 		= $this->request->getVar('code');
	
		$initial				= 'KLAIM';
		$table					= 'temp_t_klaim';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			$data 		= array();	

			$type		= "delete";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
		}
		else
		{
			
		}
	
		echo json_encode($resp);
	}


    public function save_form_fill(){
		$db 	= db_connect();

		$value 		= $this->request->getVar('value');
		$code 		= $this->request->getVar('code');
		$column 	= $this->request->getVar('column');
		$type_val 	= $this->request->getVar('type_val');

		$initial				= 'KLAIM';
		$table					= 'temp_t_klaim';
		
		$udah_ada 				= false;
		$resp['edit']			= '';

		if ($type_val == 'date')
		{
			$value 	= tgl_time_save_form_miring_sql($value);

			if ($column == 'tgl_keluar')
			{	
				$query = $db->query("SELECT ext_id, code from ${table} where code = '$code' ");
				$row = $query->getRow();
				if (isset($row))
				{
				    $ext_id 	= $row->ext_id;
				    $code_new 	= $row->code;
				    
					$query_check = $db->query("SELECT code from t_klaim where tgl_keluar = '$value' and ext_id = '$ext_id' ");
					$row_check = $query_check->getRow();
					if (isset($row_check))
					{
						 $code_old 	= $row->code;

						 if ($code_old != $code_new)
						 {
				 			$resp['error'] 		='error';
							$resp['message']	= 'Data Pasien Sudah Ada Pada Tanggal Keluar Terbaru';
							$resp['status']		= 'error';
							echo json_encode($resp);
							exit();
						 }
					}
				}
			}
			
		}
		
		if ($column != 'tgl_keluar')
		{
			$data 		= array($column 			=> $value
							);	
		}
		else
		{
			$data 		= array($column 			=> $value,
								'code_group_date_out'	=> tgl_unique($value)
							);	
		}

		
		$q_cek	= $db->query("SELECT code FROM ".$table." WHERE code = '".$code."'");
		$j_cek	= $q_cek->getNumRows();
		if($j_cek > 0) {
			$udah_ada	= true;
		}
		if ($udah_ada){
			
			$type		= "update";
			$message	= 'Sukses Memproses Data';
			$table_id	= "code";
			
			$query				= $this->model->m_iud($type, $data, $table, $table_id , $code, $message);
			$resp['error'] 		= $query['error'];
			$resp['message']	= $query['message'];
			$resp['status']		= $query['status'];
		}
		else
		{
			
		}
	
		echo json_encode($resp);

	}


    public function form_fill(){
		$db = db_connect();

		$val 	= $this->request->getVar('val');

		if ($val == '-')
		{
			$val = '';
		}
		
		$ret 	= '';

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<input type='text' class='form-control form_text_embbed_table' value='$val'>";
		$ret .= "</span>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}

	public function form_fill_date(){
		$db = db_connect();

		$val 	= $this->request->getVar('val');

		if ($val == '-')
		{
			$val = '';
		}
		
		$ret 	= '';

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<input type='text'  class='form-control form_text_embbed_table' id='jqdatetimepicker' value='$val'>";
		$ret .= "</span>";
		$ret .= "<script>";
		$ret .= "$('#jqdatetimepicker').datetimepicker({
					  format:'d/m/Y H:i',
					  lang:'id'
					});
		        ";
		$ret .= "</script>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}

	public function form_fill_select(){
		$db = db_connect();

		$val 	= $this->request->getVar('val');
		$url_select 	= $this->request->getVar('url_select');
		// $url_select 	= "Dev/Form_one/fetch_select_loadmore";
		
		$ret 	= '';

		if ($val == '')
		{
			$a = "<option>-</option>";
		}
		else
		{
			$a = "<option value='$val'>${val}</option>";
		}

		$ret .= "<span class='tolee_form_'>";
		$ret .= "<select class='select2 form-control form_text_embbed_table' id='select2loadmore' name='select2loadmore' >";
		$ret .= $a;                    
        $ret .=  "</select>";
		$ret .= "</span>";
		$ret .= "<script>";
		$ret .= "
					$('#select2loadmore').select2({
					    placeholder: '',
					    allowClear: true,
					    container: '.form_text_embbed_table',
					    ajax: {
					        url: '${url_select}',
					        dataType: 'json',
					        delay: 250,
					        cache: false,
					        data: function (params) {
					            return {
					                term: params.term,
					                page: params.page || 1,
					            };
					        },
					        processResults: function(data, params) {
					            console.log(data);
					            var page = params.page || 1;
					            return {
					                results: $.map(data, function (item) { return {id: item.col, text: item.col}}),
					                pagination: {
					                    more: (page * 10) <= data[0].total_count
					                }
					            };
					        },              
					    }
					});

					// $('.form_text_embbed_table').change(function (e) {
					// 	var value = $('.form_text_embbed_table').val();
					// 	alert(value);
					// });		
				
					
					
		        ";
		$ret .= "</script>";

		return $ret;
		// return view("master/users/v_form_user", $data);

	}


    public function klaim_process(){
		$db 	= db_connect();

		$code 		= $this->request->getVar('listcode');
		$user 		= $_SESSION['codeuser_sess'];
		$step 		= $_SESSION['step_sess'];
		
        $codenew					= '';
		$initial				    = 'KLAIM';
		$table					    = 'temp_t_klaim';
       
		$initial				= 'KLAIM';
		$table					= 't_klaim';
		
		$udah_ada 				= false;
		$resp['edit']			= '';
		
		$sqk = $db->query("SELECT 
									code
									from
									temp_t_klaim
									where code in ($code) and tgl_keluar <= '2000-01-01 00:00:00' ");
		$row = $sqk->getRow();
		if (isset($row))
		{
		   	$resp['error'] 		= 'error';
			$resp['message']	= 'Tanggal Keluar tidak boleh dibawah tahun 2000';
			$resp['status']		= 'success';
			echo json_encode($resp);
		}
		else
		{

			$obj_id_list = array();

			  $ssql = "SELECT 
							a.description
						FROM
							m_jaminan a
						WHERE
							a.status_cd != 'nullified'
						";


				$result_sql	= $db->query($ssql);
				if ($result_sql->getNumRows() > 0)
				{
					foreach (fetchloopsql($result_sql) as $b)
					{
						list($description) = fetchlist($b);	

						if( in_array( $description ,$obj_id_list ) )
						{
							
						}
						else
						{
							array_push($obj_id_list,$description);
						}
					}
				}	
			

			  $obj_id_list_in = implode("','",$obj_id_list);

			$sql_check_jaminan = "SELECT m.code 
									FROM 
									temp_t_klaim m 
									where m.code in ($code)
									and 
									jaminan NOT IN ('$obj_id_list_in')
									LIMIT 0,1
									";
									// var_dump($sql_check_jaminan);die;
			$query = $db->query($sql_check_jaminan);
			$row = $query->getRow();
			if (isset($row))
			{
				$resp['error'] 		= 'error';
				$resp['message']	= 'Mohon Data Jaminan untuk diisi';
				$resp['status']		= 'error';
				echo json_encode($resp);
			}
			else
			{
				$obj_id_list = array();

				$sql = "SELECT
                m.code,
                m.code_group_date_out,
                m.ext_id,
                m.person_nm,
                m.jaminan,
                m.org,
                m.ruang,
                m.tgl_masuk,
                m.tgl_keluar,
                m.`status`,
                m.dpjp,
                m.keterangan,
                m.kontrol,
                m.status_cd,
                m.code_user_process_one
                FROM temp_t_klaim m WHERE m.code IN ($code)";

				$result_sql	= $db->query($sql);
				if ($result_sql->getNumRows() > 0)
				{
					foreach (fetchloopsql($result_sql) as $b)
					{
						list($code, $code_group_date_out, $ext_id, $person_nm, $jaminan, $org, $ruang, $tgl_masuk, $tgl_keluar, $status, $dpjp, $keterangan, $kontrol, $status_cd, $code_user_process_one) = fetchlist($b);	
					
						$query_check_duplikasi = $db->query("SELECT code from t_klaim where ext_id = '$ext_id' and tgl_keluar = '$tgl_keluar' ");
						$row_duplikasi = $query_check_duplikasi->getRow();
						if (isset($row_duplikasi))
						{
							
						}
						else
						{
							$var_increment	= $initial.date('y').date('m').date('d');
							$sql_cd			= "SELECT 
													MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
												FROM 
													$table
												WHERE 
													left(CODE,11) = '".$var_increment."'";
							$query_cd	= $db->query($sql_cd);
							foreach($query_cd->getResult() as $row_cd){
								$tmpNo_cd	= $row_cd->tmpNo_cd;
								if(is_null($row_cd->tmpNo_cd)){
									$tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
								}
								else{
									$tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4); 
									$tmpNo_cd 	= $tmpNo_cd->nomor;
									$tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
								}
							}

							if( in_array( $tmpNo_cd ,$obj_id_list ) )
							{
								
							}
							else
							{
								array_push($obj_id_list,$tmpNo_cd);
							}


							$data 		= array(
								'code'			        => $tmpNo_cd,
								'code_group_date_out'	=> $code_group_date_out,
								'ext_id'			    => $ext_id,
								'person_nm'			    => $person_nm,
								'jaminan'			    => $jaminan,
								'org'				    => $org,
								'ruang'				    => $ruang,
								'tgl_masuk'			    => $tgl_masuk,
								'tgl_keluar'		    => $tgl_keluar,
								'dpjp'				    => $dpjp,
								'keterangan'		    => $keterangan,
								'code_user_process_zero'	=> $user
								);	
									
							$type		= "insert";
							$message	= 'Sukses Memproses Data';
							$table_id	= "code";

							if ($this->model->m_iud($type, $data, $table, $table_id ,$codenew, $message))
							{
								$sql_update_stat = "UPDATE temp_t_klaim
													SET status_cd='nullified'
													WHERE code='$code' ";
								$query_do = $db->query($sql_update_stat);
							}
						}
					}
				}

				$sql_del = "DELETE FROM temp_t_klaim
							WHERE status_cd='nullified' AND code_user_process_one = '$user' ";
				$query_del = $db->query($sql_del);

				$obj_id_list_in = implode("','",$obj_id_list);

				$this->save_log_field_progress("'${obj_id_list_in}'", 1, tgl_unique($tgl_keluar), $tgl_keluar);

				$resp['error'] 		= 'success';
				$resp['message']	= 'Berhasil';
				$resp['status']		= 'success';
				echo json_encode($resp);
			}

			
		}

		
	}


	public function save_log_field_progress($code, $newvalue, $code_group_date_out, $dateout){
		$db 	= db_connect();

		$codenew				= '';
		$initial				= 'LOGTKP';
		$table					= 'log_t_klaim_progress';
		$user 					= $_SESSION['codeuser_sess'];

		$udah_ada 				= false;
		$resp['edit']			= '';

		$var_increment	= $initial.date('y').date('m').date('d');
        $sql_cd			= "SELECT
                                MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
                            FROM
                                $table
                            WHERE
                                left(CODE,12) = '".$var_increment."'";
        $query_cd	= $db->query($sql_cd);
        foreach($query_cd->getResult() as $row_cd){
            $tmpNo_cd	= $row_cd->tmpNo_cd;
            if(is_null($row_cd->tmpNo_cd)){
                $tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
            }
            else{
                $tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4);
                $tmpNo_cd 	= $tmpNo_cd->nomor;
                $tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
            }
        }

        $data 		= array(
	            'code'			        					=> $tmpNo_cd,
	            'code_group_date_out'			        	=> $code_group_date_out,
	            'tgl_keluar'			        			=> $dateout,
	            'list_code'			        				=> $code,
	            'progress'			        				=> $newvalue,
	            'user'			    						=> $user
            );

        $type		= "insert";
        $message	= 'Sukses Memproses Data';
        $table_id	= "code";

        $this->model->m_iud($type, $data, $table, $table_id ,$codenew, $message);
	}




}
