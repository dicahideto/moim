<?php

namespace App\Controllers;

class Signin extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->authLoginModel 	= new \App\Models\AuthLog();
	}

	public function tes(){
		echo $this->request->uri->getPath();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_login('login/v_login');
	}


	public function process_checking_login(){
		$db = db_connect();
		$username = trim($this->request->getVar('username'));
		$password = trim($this->request->getVar('password'));

		if ($this->authLoginModel->cek_username($username))
		{
			if ($this->authLoginModel->cek_username_password($username, $password))
			{
				$sql_fetch_data = "SELECT 
										m.code,
										m.username,
										m.`name`,
										m.code_group,
										g.`desc` AS group_name,
										g.step
									FROM 
										m_user m
									LEFT JOIN
										m_group g ON (m.code_group = g.code)
									WHERE 
										m.status_cd = 'normal' and m.username = '$username' and m.`password` = '$password' ";
				$result_sql_fetch_data = $db->query($sql_fetch_data);
				if ($result_sql_fetch_data->getNumRows() > 0)
				{
					foreach (fetchloopsql($result_sql_fetch_data) as $b)
					{
						list($code, $username, $name, $code_group, $group_name, $step) = fetchlist($b);		
					}
				}
				$sessData = [
					'codeuser_sess' => $code,
					'username_sess' => $username,
					'name_user_sess' => $name,
					'code_group_sess' => $code_group,
					'group_name_sess' => $group_name,
					'step_sess' => $step,
					'isLoggedIn' => TRUE
				];

				$this->session->set($sessData);

				return redirect()->to(site_url('/home'));
			}
			else
			{
				$this->session->setFlashdata('errors', 'Password tidak sesuai');

				return redirect()->to('/Signin');
			}
		}
		else{
			$this->session->setFlashdata('errors', 'Username tidak ditemukan');

			return redirect()->to('/Signin');
		}


	}


	public function logout()
	{
		$this->session->destroy();
		return redirect()->to('/Signin');
	}



}
