<?php

namespace App\Controllers\Dev;
use App\Controllers\BaseController;

class Table_one extends BaseController
{
	public function __construct()
	{
		require_once APPPATH.'ThirdParty\ssp.class.php';
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->db = db_connect();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('dev/v_table_one');
	}


	public function table_sample()
	{
        $table = "m_orgs";

        $primaryKey = "org_id";

		$dbDetails = array(
            "host" => $this->db->hostname,
            "user" => $this->db->username,
            "pass" => $this->db->password,
            "db"   => $this->db->database,
        );

		$filtering_manual = " ";
		
		$columns = array(
			array( 'db' => 'org_id',   'dt' => 0 ),
			array( 'db' => 'org_id',   'dt' => 1 ),
			array( 'db' => 'org_nm',   'dt' => 2 )
		);
		
		echo json_encode(
            \SSP::simple($_GET, $dbDetails, $table, $primaryKey, $columns)
        );

        // echo 
	}

	public function custom_sample_table_proses(){
		$db 		= db_connect();

		$json 		= $this->request->getVar('data');
		$ret 		= '';

		$mahasiswa 	= json_decode($json);

		$people 	= $mahasiswa->data;
		$count 		= count($mahasiswa->data);
		

		if ($count>0){
			$count_sub 	= count($people[0]);

			for ($i=0;$i<$count;$i++)
			{
				$ret 	.= "<tr>";
				for ($z=0;$z<$count_sub;$z++)
				{
					$ret 	.= "<td>";
					$ret 	.= $people[$i][$z];
					$ret 	.= "</td>";
				}
				$ret 	.= "</tr>";
				$ret 	.= "<tr>";
				$ret 	.= "<td colspan='$count_sub'>";
				$ret 	.= "</td>";
				$ret 	.= "</tr>";
			}
		}

		return $ret;
		// $people_arr = json_encode($people);
		// $carse = array("Volvo", "BMW", "Toyota");
		// // $array = (array) $people_arr;
		// // $array = [['One', 'Two', 'Three'],['1', '2', '3']];
		// $array = [["1","Rumah Sakit Cipto Mangunkusumo"],["2","Dewan Pengawas"],["3","Direktur Utama"],["4","Komite Etik Penelitian Kesehatan"],["5","Komite Keperawatan"]];

		// echo $count_s = count($array);
		// // print_r($array);
		// // echo $people[0][1];
		// // var_dump($people);
		// // echo $count_s;
		
		// if ($count>0){
			// foreach ($people as $car) {
			    // echo $people_arr[0][0];
			    
			    // echo "<br />";
			    // var_dump($carse);
			    // $ret 	= "<tr>
			    // 			<td>
			    // 				$car->org_id
			    // 			</td>
			    // 			<td>
			    // 				$car->org_nm
			    // 			</td>
			    // 			<td>
			    				
			    // 			</td>
			    // 		</tr>";
			// }	
		// }

		// return $ret;
		
	}

	public function custom_sample_table(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			$filter = "";
		}

		$perPageCount 	= 5;
		$item 			= array();

		$select_rows = $db->query("
										SELECT
										org_id, org_nm
										FROM
										m_orgs
										WHERE 
										status_cd = 'active'
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT
					org_id, org_nm
				FROM
					m_orgs
				WHERE 
					status_cd = 'active'
				order by org_nm desc 
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($org_id, $org_nm) = fetchlist($b);	
				$n++;
				$nestedData=array(); 
				
				$nestedData[] = $org_id;																				//2
				$nestedData[] = $org_nm;
				$nestedData[] = "tes";	
				
				$data[] = $nestedData;					
							
		
			}
		}
		
		$item = array(
				"draw"            			=> $rowCount,   // total data array
				"recordsTotal"            	=> $pagesCount,   // total data array
				"recordsFiltered"           => $lowerLimit,   // total data array
				"data"            			=> $data   // total data array
			);
		
		echo json_encode($item);
	}


	public function tese(){
		$db = db_connect();

		$filterGet 	= $this->request->getVar('filter');
		$pageGet 	= $this->request->getVar('page');
		$tbl 	 	= $this->request->getVar('tbl');
		$tbl_ar		= $tbl."_arrah";
		$ret 		= "";

		if ($pageGet == '') {
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}
		
		if ($filterGet == '') {
			$filter = "";
		} else {
			
			$filter = " AND org_nm LIKE '%$filterGet%'";
		}

		$perPageCount 	= 5;
		$item 			= array();

		$select_rows = $db->query("
										SELECT
											org_id, org_nm
										FROM
											m_orgs
										WHERE 
											status_cd = 'active'
										$filter
									"
								);
			
		$rowCount		= $select_rows->getNumRows();
		$pagesCount  	= ceil($rowCount / $perPageCount);
		$lowerLimit 	= ($pageNumber - 1) * $perPageCount;
		$nextPage 		= ($pageNumber + 1);
		$backPage 		= ($pageNumber - 1);		
											
		$sql = "SELECT
					org_id, org_nm
				FROM
					m_orgs
				WHERE 
					status_cd = 'active'
					$filter
				order by org_nm desc 
				limit " . ($lowerLimit) . " ,  " . ($perPageCount) . " 
				"; 

		$result_sql	= $db->query($sql);
		$data 		= array();

		$n = 0;
		if ($result_sql->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql) as $b)
			{
				list($org_id, $org_nm) = fetchlist($b);	
				$n++;
				$ret 	.= "
							<tr>
								<td class='bleft bright bbottom bpad7'>
									$org_id
								</td>
								<td class='bright bbottom bpad7'>
									<span class='xlink tese_click' data-org_id='$org_id'>$org_nm</span>
								</td>
								<td class='bright bbottom bpad7'>
									
								</td>
							</tr>
							<tr>
								<td colspan='3' style='padding:0px;' class='bleft bright  '>
									<div id='form_edit_${org_id}'></div>
								</td>
							</tr>
							";
			}
		}
		else
		{
			$ret	.= "
						<tr>
							<td colspan='3 class='bpad7 bleft bright bbottom'>
								Data Kosong
							</td>
						</tr>
					";
		}

        $detailRet = table_footer_manage($tbl_ar, 'table_tese', $backPage, $nextPage, $pageNumber, $pagesCount);

		$resp['recordPagination']	= $detailRet;
		$resp['datae']				= $ret;
		
		echo json_encode($resp);
		
	}


	function view_tese_detail(){
		$db = db_connect();

		$org_id 	= $this->request->getVar('org_id');

		$ret 	= "";

		$ret  	.= "<div id='tolee_'>";
		$ret  	.= $org_id;
		$ret  	.= "</div>";

		return $ret;
	}

}
