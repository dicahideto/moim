<?php

namespace App\Controllers\Dev;
use App\Controllers\BaseController;



class Import_excel extends BaseController
{
	public function __construct(){
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index(){

		return view_one('Dev/v_import_excel');
	}


	public function prosesExcelSpread()
	{
		$db = db_connect();

		$code					= '';
		$initial				= 'KLAIM';
		$table					= 't_klaim';

		$file = $this->request->getFile('file');
		$ext = $file->getClientExtension();

		if ($ext == 'xls'){
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
			$act 	= 'normal';
		}
		else if ($ext == 'xlsx')
		{
			$render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
			$act 	= 'normal';
		}
		else
		{
			$act 	= 'fail';
		}

		if ($act == 'fail')
		{
			$resp['error'] 		= 'error';
			$resp['message']	= 'File harus dalam bentuk excel';
			$resp['status']		= 'error';
		}
		else
		{
			$spreadsheet = $render->load($file);
			$sheet = $spreadsheet->getActiveSheet()->toArray();

			foreach($sheet as $x => $excel){

					$var_increment	= $initial.date('y').date('m').date('d');
					$sql_cd			= "SELECT 
											MAX(CAST(RIGHT(CODE,4) AS UNSIGNED)) as tmpNo_cd
										FROM 
											$table
										WHERE 
											left(CODE,11) = '".$var_increment."'";
					$query_cd	= $db->query($sql_cd);
					foreach($query_cd->getResult() as $row_cd){
						$tmpNo_cd	= $row_cd->tmpNo_cd;
						if(is_null($row_cd->tmpNo_cd)){
							$tmpNo_cd	= $initial.date('y').date('m').date('d').'0001';
						}
						else{
							$tmpNo_cd 	= $this->model->getNo($tmpNo_cd,4); 
							$tmpNo_cd 	= $tmpNo_cd->nomor;
							$tmpNo_cd	= $initial.date('y').date('m').date('d').$tmpNo_cd;
						}
					}


				if ($x == 0)
				{
					continue;
				}

				$no_rm 				= $excel['1'];
				$nama_pasien 		= $excel['2'];
				$jaminan 			= $excel['3'];
				$unit_dept 			= $excel['4'];
				$ruang_bed 			= $excel['5'];
				$tgl_masuk 			= $excel['6'];
				$tgl_keluar 		= $excel['7'];
				$tgl_diterima_rm 	= $excel['8'];
				$dpjp 				= $excel['9'];
				$gap 				= $excel['10'];
				$hari 				= $excel['11'];
				$jam 				= $excel['12'];
				$kelengkapan 		= $excel['13'];
				$tgl_rm_dilengkapi 	= $excel['14'];
				$keterangan 		= $excel['15'];

				if ($no_rm == '')
				{
					continue;
				}
				$data 		= array('code' 				=> $tmpNo_cd,
									'ext_id'			=> $no_rm,
									'person_nm'			=> $nama_pasien,
									'jaminan'			=> $jaminan,
									'org'				=> $unit_dept,
									'ruang'				=> $ruang_bed,
									'tgl_masuk'			=> tgl_indo_miring_time($tgl_masuk),
									'tgl_keluar'		=> tgl_indo_miring_time($tgl_keluar),
									'dpjp'				=> $dpjp,
									'tgl_diterima_rm'	=> tgl_indo_miring_time($tgl_diterima_rm),
									'gap'				=> $gap,
									'hari'				=> $hari,
									'jam'				=> $jam,
									'keterangan'		=> $keterangan,
									'kelengkapan'		=> $kelengkapan,
									'tgl_rm_dilengkapi'	=> $nama_pasien
									);	
										
				$type		= "insert";
				$message	= 'Sukses Memproses Data';
				$table_id	= "code";

				$query		= $this->model->m_iud($type, $data, $table, $table_id ,$code, $message);
			}

			$resp['error'] 		= 'success';
			$resp['message']	= 'Berhasil';
			$resp['status']		= 'success';
		}
		
	
		echo json_encode($resp);
	}

	

	public function tesdca(){
		$resp['error'] 		= 'success';
		$resp['message']	= 'Berhasil';
		$resp['status']		= 'success';

		echo json_encode($resp);
	}

}
