<?php

namespace App\Controllers\Dev;
use App\Controllers\BaseController;

class Form_one extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('dev/v_form_one');
	}



	public function fetch_select_loadmore(){
		$db 			= db_connect();

		$page			= $this->request->getVar('page');
		$term			= $this->request->getVar('term');
	    $resultCount 	= 10;
	    $end 			= ($page - 1) * $resultCount;       
	    $start 			= $end + $resultCount;

	    $stmt 	= $db->query("SELECT org_id, org_nm FROM m_orgs WHERE org_id != '' and org_nm LIKE '%".$term."%' LIMIT {$end},{$start}");
	  	
	    $count 	= $stmt->getNumRows();
        $data 	= [];
        if ($count > 0)
        {
        	$results = $stmt->getResultArray();
			foreach ($results as $row)
			{
			    $data[] = ['id'=>$row['org_id'], 'col'=>$row['org_nm'], 'total_count'=>$count];
			}
			echo json_encode($data);
        }
        else
        {
        	$empty[] = ['id'=>'', 'col'=>'', 'total_count'=>'']; 
            echo json_encode($empty);
        }
	}


	public function fetch_select_paging(){
		$db 					= db_connect();
		$request 				= \Config\Services::request();
		
		$pilter 				= $request->getVar('pilter');
		$pageGet 				= $request->getVar('pageNumb');
		$parent_div_name 		= $request->getVar('parent_div_name');
		$page_route			 	= $request->getVar('page_route');
		$perPageCount 			= 5;

		if (strlen($pilter) > 0)
		{
			$filter = " AND org_nm LIKE '%$pilter%'";
		}
		else
		{
			$filter = '';
		}

		$sql 			= "Select org_id, org_nm from m_orgs where org_id != '' $filter";
		
		$result 		= $db->query($sql);
		$rowCount 		= $result->getNumRows();
		
		if ($pageGet == '') 
		{
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}

		$lowerLimit   		= ($pageNumber - 1) * $perPageCount;
		$start_num			= $lowerLimit+1;					
		$sql_list 			= $sql. " limit " . ($lowerLimit) . " ,  " . ($perPageCount);
		$result_sql_list	= $db->query($sql_list);
		$rowCount_list		= $result_sql_list->getNumRows();
		$end_num			= $lowerLimit + $rowCount_list;

		$ret = '';
		
		$ret .= dcafheadselectoption($pageNumber, $parent_div_name, $page_route, $start_num, $end_num, $rowCount);

		if ($result_sql_list->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql_list) as $b)
			{
				list($org_id, $org_nm) = fetchlist($b);	

				$data_sel['org_id'] = $org_id;
				$data_sel['org_nm'] = $org_nm;

				$ret .= dcafselectoption($org_id, $org_nm, json_encode($data_sel));
			}
		}

		echo $ret;
	}
	
	public function fetch_select_paging2(){
		$db 					= db_connect();
		$request 				= \Config\Services::request();
		
		$pilter 				= $request->getVar('pilter');
		$pageGet 				= $request->getVar('pageNumb');
		$parent_div_name 		= $request->getVar('parent_div_name');
		$page_route			 	= $request->getVar('page_route');
		$perPageCount 			= 5;

		if (strlen($pilter) > 0)
		{
			$filter = " AND org_nm LIKE '%$pilter%'";
		}
		else
		{
			$filter = '';
		}

		$sql 			= "Select org_id, org_nm from m_orgs where org_id != '' $filter";
		
		$result 		= $db->query($sql);
		$rowCount 		= $result->getNumRows();
		
		if ($pageGet == '') 
		{
			$pageNumber = 1;
		} else {
			$pageNumber = $pageGet;
		}

		$lowerLimit   		= ($pageNumber - 1) * $perPageCount;
		$start_num			= $lowerLimit+1;					
		$sql_list 			= $sql. " limit " . ($lowerLimit) . " ,  " . ($perPageCount);
		$result_sql_list	= $db->query($sql_list);
		$rowCount_list		= $result_sql_list->getNumRows();
		$end_num			= $lowerLimit + $rowCount_list;

		$ret = '';
		
		$ret .= dcafheadselectoption($pageNumber, $parent_div_name, $page_route, $start_num, $end_num, $rowCount);

		if ($result_sql_list->getNumRows() > 0)
		{
			foreach (fetchloopsql($result_sql_list) as $b)
			{
				list($org_id, $org_nm) = fetchlist($b);	

				$data_sel['org_id'] = $org_id;
				$data_sel['org_nm'] = $org_nm;

				$ret .= dcafselectoption($org_id, $org_nm, json_encode($data_sel));
			}
		}

		echo $ret;
	}

}
