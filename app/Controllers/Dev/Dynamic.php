<?php

namespace App\Controllers\Dev;
use App\Controllers\BaseController;

class Dynamic extends BaseController
{

	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}

	public function index()
	{
		$data['hmm'] = 'hmmm';

		return view_one('dev/v_dynamic');
	}

	public function show_table(){
		$db = db_connect();

		$query = $db->query('SELECT name, title, email FROM my_table');
		$db->simpleQuery('YOUR QUERY')
		//OBJECT
		if ($query->getNumRows() > 0)
		{
		    $results = $query->getResult();
		    foreach ($results as $row)
		    {
		        echo $row->title;
		    }
		}


		
	}

}
