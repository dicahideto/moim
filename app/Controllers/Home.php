<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function __construct()
	{
		$this->session	 		= session();
		$this->request 			= \Config\Services::request();
		$this->model 			= new \App\Models\Model_crud();
		$this->modeltransc		= new \App\Models\Model_transc();
	}


	public function index()
	{
		$db = db_connect();

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 1 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_one'] 		= $row->desc;
		}


		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 1 ");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $data['ct_one'] 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 2 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_two'] 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 2 ");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $data['ct_two'] 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 3 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_three'] 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 3 ");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $data['ct_three'] 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 4 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_four'] 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 4 ");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $data['ct_four'] 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 5 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_five'] 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 5 ");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $data['ct_five'] 		= $rowct->total;
		}

		return view_one('tes', $data);
	}


	public function app_detail_progress(){
		$db = db_connect();

		$user 		= $_SESSION['codeuser_sess'];
		$step_user 	= $_SESSION['step_sess'];

		$year_var 	= $this->request->getVar('year_var');
		$month_var 	= $this->request->getVar('month_var');

		$code 	= $this->request->getVar('id');
		$ret 	= "";

		$sql_jkn = "SELECT COUNT(code) as total
				FROM 
					t_klaim 
				WHERE 
					jaminan in  ('JKN', 'JAMINAN KESEHATAN NASIONAL')
				AND 
					progress = '${code}' 
				AND
					status_cd != 'nullified'
				AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
					";


		$query = $db->query($sql_jkn);
		$row = $query->getRow();
		if (isset($row))
		{
		    $jkn 		= $row->total;
		}

		//==========================

		$sql_pt = "SELECT COUNT(code) as total
				FROM 
					t_klaim 
				WHERE 
					jaminan = 'PERUSAHAAN' 
				AND 
					progress = '${code}' 
				AND
					status_cd != 'nullified'
				AND
					tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
					";


		$query = $db->query($sql_pt);
		$row = $query->getRow();
		if (isset($row))
		{
		    $pt 		= $row->total;
		}


		//==========================

		$sql_umum = "SELECT COUNT(code) as total
				FROM 
					t_klaim 
				WHERE 
					jaminan = 'UMUM' 
				AND 
					progress = '${code}' 
				AND
					status_cd != 'nullified'
				AND
					tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
					";


		$query = $db->query($sql_umum);
		$row = $query->getRow();
		if (isset($row))
		{
		    $umum 		= $row->total;
		}


		$ret .= "
		<div class='row'>
				<div class='col-xl-4 col-md-6 col-12 dashboard-latest-update'>
						<div class='card'>
							<div class='card-header d-flex justify-content-between align-items-center pb-50'>
								<h4 class='card-title'>Progress Berkas Per Jaminan</h4>
								<div class='dropdown'>
									
								</div>
							</div>
							<div class='card-content'>
								<div class='card-body p-0 pb-1'>
									<ul class='list-group list-group-flush'>
										<li class='list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between'>
											<div class='list-left d-flex'>
												<div class='list-icon mr-1'>
													<div class='avatar bg-rgba-primary m-0'>
														<div class='avatar-content'>
															<i class='bx bx-stats text-primary font-size-base'></i>
														</div>
													</div>
												</div>
												<div class='list-content'>
													<span class='list-title'>Total Berkas JKN</span>
													<small class='text-muted d-block'></small>
												</div>
											</div>
											<span>${jkn}</span>
										</li>
										<li class='list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between'>
											<div class='list-left d-flex'>
												<div class='list-icon mr-1'>
													<div class='avatar bg-rgba-info m-0'>
														<div class='avatar-content'>
															<i class='bx bx-stats text-info font-size-base'></i>
														</div>
													</div>
												</div>
												<div class='list-content'>
													<span class='list-title'>Total Berkas UMUM</span>
													<small class='text-muted d-block'></small>
												</div>
											</div>
											<span>${umum}</span>
										</li>
										<li class='list-group-item list-group-item-action border-0 d-flex align-items-center justify-content-between'>
											<div class='list-left d-flex'>
												<div class='list-icon mr-1'>
													<div class='avatar bg-rgba-danger m-0'>
														<div class='avatar-content'>
															<i class='bx bx-stats text-danger font-size-base'></i>
														</div>
													</div>
												</div>
												<div class='list-content'>
													<span class='list-title'>Total Berkas PERUSAHAAN</span>
													<small class='text-muted d-block'></small>
												</div>
											</div>
											<span>${pt}</span>
										</li>
									</ul>
							</div>
					</div>
				</div>
			</div>
			</div>
				";

		return $ret;

	}


	public function periode_monitoring(){
		$db = db_connect();

		date_default_timezone_set("Asia/Jakarta");
		
		$month_val      = date('m');
		$year_val       = date('Y');

		$year_var 	= $this->request->getVar('year_var');
		$month_var 	= $this->request->getVar('month_var');

		if ($year_var == '')
		{
			$year_var = $year_val;
		}

		if ($month_var == '')
		{
			$month_var = $month_val;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 1 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $data['desc_one'] 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 1 
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $a1 		= $rowct->total;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 2 
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $a2 		= $rowct->total;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 3 
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $a3 		= $rowct->total;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 4 
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $a4 		= $rowct->total;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress = 5 
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $a5 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 1 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_one 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							and
							progress > 1
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $ct_one 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 2 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_two 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress > 2
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $ct_two 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 3 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_three 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress > 3
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $ct_three 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 4 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_four 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress > 4
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $ct_four 		= $rowct->total;
		}

		$query = $db->query("SELECT
							`desc`
							FROM 
							m_group m
							WHERE
							status_cd != 'nullified'
							AND
							step = 5 ");
		$row = $query->getRow();
		if (isset($row))
		{
		    $desc_five 		= $row->desc;
		}

		$queryct = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							progress > 5
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowct = $queryct->getRow();
		if (isset($rowct))
		{
		    $ct_five 		= $rowct->total;
		}

		$qto = $db->query("SELECT
							count(code) as total
							FROM 
							t_klaim
							WHERE
							status_cd != 'nullified'
							AND
							tgl_keluar >= '$year_var-$month_var-01 00:00:00' and tgl_keluar <= '$year_var-$month_var-31 23:59:59'
							");
		$rowqto = $qto->getRow();
		if (isset($rowqto))
		{
		    $rsqto 		= $rowqto->total;
		}

		$monthl = bulan_indo_to_alpha($month_var);

		$ret = "";

		$ret .= "
							<h3>
                                Jumlah Pasien s.d $monthl $year_var :  $rsqto
                            </h3>
                            <div class='list-group list-group-flush'>
                                <div class='list-group-item list-group-item-action border-0 d-flex card_berkas' style='background-color:#bee0ae;' data-id='1' data-month='$month_var' data-year='$year_var'>
                                    <div class='list-icon'>
                                        <i class='badge-circle badge-circle-light-secondary bx  mr-1 text-primary'>
                                        <span style='font-size:14px;'>1</span></i>
                                        <div style='text-align:center;border:0px solid black;width:200px;margin-left:-20px;'>$desc_one </div>
                                    </div>

                                    <div class='list-content' style='margin-left:50px;'>
                                        <h5>$ct_one sudah selesai</h5>
                                        <p class='mb-0'>
                                            $a1 berkas proses eval
                                        </p>
                                    </div>
                                </div>
                                <div class='list-group-item list-group-item-action border-0 d-flex card_berkas' style='background-color:#a5ccf3;' data-id='2' data-month='$month_var' data-year='$year_var'>
                                    <div class='list-icon'>
                                        <i class='badge-circle badge-circle-light-secondary bx  mr-1 text-primary'>
                                        <span style='font-size:14px;'>2</span></i>
                                        <div style='text-align:center;border:0px solid black;width:200px;margin-left:-20px;'>$desc_two </div>
                                    </div>
                                    <div class='list-content' style='margin-left:50px;'>
                                        <h5>$ct_two sudah selesai</h5>
                                        <p class='mb-0'>
                                            $a2 berkas proses billing
                                        </p>
                                    </div>
                                </div>
                                <div class='list-group-item list-group-item-action border-0 d-flex card_berkas' style='background-color:#ffeea0;' data-id='3' data-month='$month_var' data-year='$year_var'>
                                    <div class='list-icon'>
                                        <i class='badge-circle badge-circle-light-secondary bx  mr-1 text-primary'>
                                        <span style='font-size:14px;'>3</i></span>
                                        <div style='text-align:center;border:0px solid black;width:200px;margin-left:-20px;'>$desc_three</div>
                                    </div>
                                    <div class='list-content' style='margin-left:50px;'>
                                        <h5>$ct_three sudah selesai</h5>
                                        <p class='mb-0'>
                                            $a3 berkas proses verifikasi
                                        </p>
                                    </div>
                                </div>
                                <div class='list-group-item list-group-item-action border-0 d-flex card_berkas' style='background-color:#fed2a1;' data-id='4' data-month='$month_var' data-year='$year_var'>
                                    <div class='list-icon' >
                                        <i class='badge-circle badge-circle-light-secondary bx  mr-1 text-primary'>
                                        <span style='font-size:14px;'>4</span></i>
                                        <div style='text-align:center;border:0px solid black;width:200px;margin-left:-20px;'>$desc_four </div>
                                    </div>
                                    <div class='list-content' style='margin-left:50px;'>
                                        <h5>$ct_four sudah selesai</h5>
                                        <p class='mb-0'>
                                            $a4 berkas proses proses grouper
                                        </p>
                                    </div>
                                </div>
                                <div  class='list-group-item list-group-item-action border-0 d-flex card_berkas' style='background-color:#e0c6df;' data-id='5' data-month='$month_var' data-year='$year_var'>
                                    <div class='list-icon' >
                                        <i class='badge-circle badge-circle-light-secondary bx  mr-1 text-primary'>
                                        <span style='font-size:14px;'>5</span></i>
                                        <div style='text-align:center;border:0px solid black;width:200px;margin-left:-20px;'>$desc_five </div>
                                    </div>
                                    <div class='list-content' style='margin-left:50px;'>
                                        <h5>$ct_five sudah selesai</h5>
                                        <p class='mb-0'>
                                            $a5 berkas proses validasi IPP
                                        </p>
                                    </div>
                                </div>
                            </div>


			";

		return $ret;
	}
}
