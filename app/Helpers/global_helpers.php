<?php

if ( ! function_exists('global_helpers'))
{
	/*
		created by dicahideto
		created date 01 02 2016
	*/
	
    function view_one($view, $data=[]){
        $ret = '';

        $ret .= view('base_ui/one/topping');
		$ret .= view('base_ui/one/header');
		$ret .= view('base_ui/one/top_menu');
		$ret .= view('base_ui/one/side_menu');
		$ret .= view('base_ui/one/content_open');

		$ret .= view($view, $data);

		$ret .= view('base_ui/one/content_close');
		$ret .= view('base_ui/one/footer');
		$ret .= view('base_ui/one/footer_source');
		$ret .= view('base_ui/one/closing');

        return $ret;
    }

    function view_login($view, $data=[]){
        $ret = '';

        $ret .= view('base_ui/login/topping');
		$ret .= view('base_ui/login/header');
		$ret .= view($view, $data);
		$ret .= view('base_ui/login/footer');
		$ret .= view('base_ui/login/closing');

        return $ret;
    }

    function table_footer_manage($tbl_ar, $class_on_event, $backPage, $nextPage, $pageNumber, $pagesCount){
    	$li  = "";
        $li .= "<ul class='pagination justify-content-end style='margin-top:-20px;'>";

        if ($pageNumber != 1)
		{
				$li .= "<li class='page-item previous '>
			            	<a class='page-link $class_on_event' data-page='$backPage' href='javascript:;' >
			                    <i class='bx bx-chevron-left'></i>
			                </a>
			            </li>";

				$li .= "<li class='page-item active' aria-current='page'>
			            	<a class='page-link $class_on_event' data-page='$nextPage' href='javascript:;' >
			            		$pageNumber
			        		</a>
			        	</li>";
			
			if ($pageNumber == $pagesCount)
			{
				$li .= "";
			}
			else
			{
				$li .= "
						<li class='page-item next'>
			            	<a class='page-link $class_on_event' data-page='$nextPage' href='javascript:;' >
			                    <i class='bx bx-chevron-right'></i>
			            	</a>
			        	</li>
						";
			}
			
		}
		else
		{
			$li .= "<li class='page-item active' aria-current='page'>
			            	<a class='page-link $class_on_event' data-page='$nextPage' href='javascript:;' >
			            		$pageNumber
			        		</a>
			        	</li>";
			if ($pageNumber == $pagesCount)
			{
				$li .= "";
			}
			else
			{
				$li .= "
						<li class='page-item next'>
			            	<a class='page-link $class_on_event' data-page='$nextPage' href='javascript:;' >
			                    <i class='bx bx-chevron-right'></i>
			            	</a>
			        	</li>
						";
			}
		}	

        $li .= "</ul>";


        if ($pagesCount == 0)
		{
			$info_hal = "Hal 0 dari $pagesCount";
			$page_link = "";
		}
		else
		{
			$info_hal = "Hal $pageNumber dari $pagesCount";
			$page_link = $li;
		}

		$detailRet = "";
        $detailRet .= "
					<table border='0px' width='100%' class='$tbl_ar'>
						<tr>
							<td align='left' valign='top' width='50%' style='padding:20px;font-size:11px;color: #9e9e9e;'>
								$info_hal
							</td>
							<td align='right' valign='top' width='50%' style='padding:20px;'>
								$page_link
							</td>
						</tr>
					</table>
				";

		// return array($info_hal, $page_link);
		return $detailRet;
    }

	function dcafselectoption($value_click, $value_desc, $data_sel=[]){
		$ret = "<div class='selcusdca' data-id='$value_click' data-sel='$data_sel' style='font-size:11px;cursor:pointer;height: auto;padding: 10px;border-bottom:1px solid #f0f0f0;' onMouseOver=\"this.style.backgroundColor='orange';\" onMouseOut=\"this.style.backgroundColor='white';\"> $value_desc </div>";
		return $ret;
	}

	function dcafheadselectoption($pageNumber, $parent_div_name, $page_route, $start_num, $end_num, $rowCount)
	{
		$nextPage    		= ($pageNumber + 1);
		$backPage     		= ($pageNumber - 1);   
		
		$ret = '';

		$ret .= "<div style='position: sticky;top:0;padding:6px;text-align:center;border-bottom:1px solid #f0f0f0;background-color:#f8f9f8;'>
					<span style='float: left;padding-left:5px;width:30px;'>
				";

		if ($backPage < 1)
		{
			$ret .= "<span><h5 > &nbsp; </h5></span>";
		}
		else
		{
			$ret .= "<span style='cursor:pointer;' class='navigate_select_custom_page' data-parent='$parent_div_name' data-npage='$backPage' data-page_route='$page_route'><h5 > < </h5></span>";
		}

		$ret .="				
					</span>
					$start_num - $end_num [$rowCount] 
					<span style='float: right;padding-right:5px;width:30px;'>
				";

		if ($end_num == $rowCount)
		{
			$ret .= "<span><h5 > &nbsp; </h5></span>";
		}
		else
		{
			$ret .= "<span style='cursor:pointer;' class='navigate_select_custom_page' data-parent='$parent_div_name' data-npage='$nextPage' data-page_route='$page_route'><h5 > > </h5></span>";
		}

		$ret .="
					</span>
				</div>";

		return $ret;
	}

	function fetchlist($result)
	{
		$result_arraye = array_values((array)$result);
		return $result_arraye;
	}
	
	function fetchloopsql($result)
	{
		$a = $result->getResultArray();
		return $a;
	}

	function get0($length)
	{
		/*
			created by : dicahideto
			created date : 01 february
		*/
		$id = "";
		for( $i = 2; $i <= $length; $i++ ) {
			$id .= "0";
		}
		return $id;
	}
	
	function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {  
		$earth_radius = 6371;

		$dLat = deg2rad($latitude2 - $latitude1);  
		$dLon = deg2rad($longitude2 - $longitude1);  

		$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);  
		$c = 2 * asin(sqrt($a));  
		$d = $earth_radius * $c;  

		return $d;  
	}

	function money_mask_remove($real){
		$realin 				= str_replace( '.', '', $real );
		return $realin;
	}

	function null_help($var)
	{
		if ($var == "")
		{
			$hasil = null;
		}
		else
		{
			$hasil = $var;
		}
		
		return $hasil;
	}
	
	function get_contents($url, $ua = 'Mozilla/5.0 (Windows NT 5.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1', $referer = 'http://www.google.com/') {
		$header[0] = "Accept-Language: en-us,en;q=0.5";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, $ua);
		curl_setopt($curl, CURLOPT_REFERER, $referer);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		$content = curl_exec($curl);
		if(curl_errno($curl)){
			http_response_code(500);
		}
		curl_close($curl);
		return $content;
	}

	function url_get_contents ($Url) {
		if (!function_exists('curl_init')){ 
			die('CURL is not installed!');
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $Url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}

	function file_get_contents_curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
		curl_setopt($ch, CURLOPT_URL, $url);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	
	function take_number_only($num)
	{
		if ($num == "")
		{
			$abc = "";
		}
		else
		{
			$abc = preg_replace("/[^\d]/", "", $num);	
		}
		
		return $abc;
	}
	
	function encrypt_decrypt($action, $string) {
		/*
			created by : dicahideto
			created date : 01 february
		*/
		$output = false;

		$encrypt_method = "AES-256-CBC";
		$secret_key = '4lKh4t3ch-84t4M';
		$secret_iv = 'PGG Application';

		// hash
		$key = hash('sha256', $secret_key);
		
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		if( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		}
		else if( $action == 'decrypt' ){
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}

		return $output;
	}  
	
	
	function xmlEscape($string) {
		/*
			created by : dicahideto
			created date : 01 february
		*/
		return str_replace(array('&', '<', '>', '\'', '"'), array('&amp;', '&lt;', '&gt;', '&apos;', '&quot;'), $string);
	}	
	
	function gen_uuid() {
		/*
			created by : dicahideto
			created date : 01 february
		*/
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			// 32 bits for "time_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

			// 16 bits for "time_mid"
			mt_rand( 0, 0xffff ),

			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 4
			mt_rand( 0, 0x0fff ) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000,

			// 48 bits for "node"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
	
	function randomString($panjang)
	{
		$characters = 'abcdefghijklmnopqrstuvwxyxz'; 
		$randomString = ''; 
		
		for ($i = 0; $i < $panjang; $i++) { 
			$index = rand(0, strlen($characters) - 1); 
			$randomString .= $characters[$index]; 
		} 
		
		return $randomString; 
	}
	
	function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
	function random_word($id = 20){
		$pool = '1234567890abcdefghijkmnpqrstuvwxyz';
		
		$word = '';
		for ($i = 0; $i < $id; $i++){
			$word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
		}
		return $word; 
	}
	
	function tgl_indo_miring_time($tanggal)
	{
		
		if ($tanggal == '0000-00-00 00:00:00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('d/m/Y H:i', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}

	function tgl_indo_singkat($tanggal)
	{
		
		if ($tanggal == '0000-00-00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('d/m/Y', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}

	function tgl_inggris($tanggal)
	{
		
		if ($tanggal == '0000-00-00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('d M Y', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}
	
	function tgl_inggris_time($tanggal)
	{
		
		if ($tanggal == '0000-00-00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('d M Y H.i.s', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}
	
	
	function tgl_save_sql($tanggal)
	{
		if ($tanggal == '0000-00-00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('Y-m-d', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}

	function tgl_time_save_sql($tanggal)
	{
		if ($tanggal == '0000-00-00 00:00:00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			return date('Y-m-d H:i:s', strtotime(str_replace("-","/",$tanggal)));
		
		}
	}
	
	function tgl_time_save_form_miring_sql($tanggal)
	{
		if ($tanggal == '00/00/0000 00:00:00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			
			$tanggal            = explode(' ', $tanggal);

			$arr            = explode('/', $tanggal[0]);
			$newDate        = $arr[2].'-'.$arr[1].'-'.$arr[0];

			return $newDate." ".$tanggal[1];
		
		}
	}
	
	function tgl_time_save_form_miring_sql_sec($tanggal)
	{
		if ($tanggal == '00-00-0000 00:00:00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			
			$tanggal            = explode(' ', $tanggal);

			$arr            = explode('-', $tanggal[0]);
			$newDate        = $arr[2].'-'.$arr[1].'-'.$arr[0];

			return $newDate." ".$tanggal[1];
		
		}
	}
	
	function tgl_time_save_form_miring_sql_convert_sec($tanggal)
	{
		if ($tanggal == '00-00-0000 00:00:00' || $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			$arr            = explode(' ', $tanggal);
			return $arr[2].'-'.bulan_indo_to_num($arr[1]).'-'.$arr[0].' '.$arr[3];
		
		}
	}
	
	function tgl_unique($tanggal)
	{
		if ($tanggal == '0000-00-00 00:00:00' || $tanggal == '0000-00-00'|| $tanggal == '' || $tanggal == null)
		{
			return '';
		}
		else
		{
			$arr            = explode('-', $tanggal);
			return $arr[0].''.$arr[1].''.$arr[2];
		}
	}
	
	function tgl_inggris_normal($tanggal){
		if ($tanggal == '0000-00-00' || $tanggal == '')
		{
			return '';
		}
		else
		{
			$bulan = array (
			1 =>   'January',
			'February',
			'March',
			'April',
			'May',
			'Juny',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December'
		);
			$pecahkan = explode('-', $tanggal);
			
			return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
		}
	}
	
	function tgl_indo($tanggal)
	{
		if ($tanggal == '0000-00-00')
		{
			return '';
		}
		else
		{
			$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
			$pecahkan = explode('-', $tanggal);
			
			return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
		}
	}
	
	
	function bln_indo($tanggal)
	{
		// if ($tanggal == '00')
		// {
			// return '';
		// }
		// else
		// {
			$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
			$pecahkan = explode('-', $tanggal);
			
			return $bulan[ (int)$pecahkan[0] ];
		// }
	}

	
	function hari_ini_full($hari){
		if ($hari == '0000-00-00')
		{
			return '';
		}
		else
		{
			$date_ganti = date("D", strtotime($hari));
			
			switch($date_ganti){
				case 'Sun':
					$hari_ini = "Minggu";
				break;

				case 'Mon':			
					$hari_ini = "Senin";
				break;

				case 'Tue':
					$hari_ini = "Selasa";
				break;

				case 'Wed':
					$hari_ini = "Rabu";
				break;

				case 'Thu':
					$hari_ini = "Kamis";
				break;

				case 'Fri':
					$hari_ini = "Jumat";
				break;

				case 'Sat':
					$hari_ini = "Sabtu";
				break;
				
				default:
					$hari_ini = "Tidak di ketahui";		
				break;
			}

			return $hari_ini;
		}
	}
	
	function hari_ini($hari){
			switch($hari){
				case 'Sun':
					$hari_ini = "Minggu";
				break;

				case 'Mon':			
					$hari_ini = "Senin";
				break;

				case 'Tue':
					$hari_ini = "Selasa";
				break;

				case 'Wed':
					$hari_ini = "Rabu";
				break;

				case 'Thu':
					$hari_ini = "Kamis";
				break;

				case 'Fri':
					$hari_ini = "Jumat";
				break;

				case 'Sat':
					$hari_ini = "Sabtu";
				break;
				
				default:
					$hari_ini = "Tidak di ketahui";		
				break;
			}

			return $hari_ini;

	}

	function bulan_indo_to_sql($bulan){
		switch($bulan)
		{
			case 'January':
				return "01";
			break;

			case 'Januari':			
				return "01";
			break;

			case 'February':			
				return "02";
			break;

			case 'Februari':
				return "02";
			break;

			case 'March':
				return "03";
			break;

			case 'Maret':
				return "03";
			break;

			case 'April':
				return "04";
			break;

			case 'May':
				return "05";
			break;

			case 'Mei':
				return "05";
			break;

			case 'Juny':
				return "06";
			break;
			
			case 'Juni':
				return "06";
			break;
			
			case 'July':
				return "07";
			
			case 'Juli':
				return "07";
			
			case 'August':
				return "08";
			
			case 'Agustus':
				return "08";
			
			case 'September':
				return "09";
			
			case 'October':
				return "10";
			
			case 'Oktober':
				return "10";
			
			case 'November':
				return "11";
			
			case 'December':
				return "12";
			
			case 'Desember':
				return "12";
			
		
			default:
				return "01";		
			break;
		}

		return $bulan_ini;

	}

	function bulan_indo_to_alpha($bulan){
		switch($bulan){
				case '01':
					$bulan_ini = "Januari";
				break;

				case '02':			
					$bulan_ini = "February";
				break;

				case '03':			
					$bulan_ini = "Maret";
				break;

				case '04':
					$bulan_ini = "April";
				break;

				case '05':
					$bulan_ini = "Mei";
				break;

				case '06':
					$bulan_ini = "Juni";
				break;

				case '07':
					$bulan_ini = "Juli";
				break;

				case '08':
					$bulan_ini = "Agustus";
				break;

				case '09':
					$bulan_ini = "September";
				break;

				case '10':
					$bulan_ini = "Oktober";
				break;
				
				case '11':
					$bulan_ini = "November";
				break;
				
				case '12':
					$bulan_ini = "Desember";
				
				
				default:
					$bulan_ini = "Tidak di ketahui";		
				break;
			}

			return $bulan_ini;
	}



	function GetIP()
	{
		/*
			created by : dicahideto
			created date : 01 february
		*/
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	function duit_format($nilai)
	{
		//dica
		$jumlah_desimal ="0";
		$pemisah_desimal =",";
		$pemisah_ribuan =".";
			
		$hasil = number_format($nilai, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);
		
		return $hasil;
	}
	
	function no_commas($val)
	{
		//dica
		$jumlah_desimal ="0";
		$pemisah_desimal =",";
		$pemisah_ribuan =".";
			
		$hasil = number_format($val, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);
		
		return $hasil;
	}
	
	
	function number_to_words($number)
	{
		$before_comma = trim(to_word($number));
		$after_comma = trim(comma($number));
		return ucwords($results = $before_comma);
		//return ucwords($results = $before_comma.' koma '.$after_comma);
	}

	function to_word($number)
	{
		$words = "";
		$arr_number = array(
		"",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan",
		"sepuluh",
		"sebelas");

		if($number<12)
		{
			$words = " ".$arr_number[$number];
		}
		else if($number<20)
		{
			$words = to_word($number-10)." belas";
		}
		else if($number<100)
		{
			$words = to_word($number/10)." puluh ".to_word($number%10);
		}
		else if($number<200)
		{
			$words = "seratus ".to_word($number-100);
		}
		else if($number<1000)
		{
			$words = to_word($number/100)." ratus ".to_word($number%100);
		}
		else if($number<2000)
		{
			$words = "seribu ".to_word($number-1000);
		}
		else if($number<1000000)
		{
			$words = to_word($number/1000)." ribu ".to_word($number%1000);
		}
		else if($number<1000000000)
		{
			$words = to_word($number/1000000)." juta ".to_word($number%1000000);
		}
		else
		{
			$words = "undefined";
		}
		return $words;
	}

	
	function comma($number)
	{
		$after_comma = stristr($number,',');
		$arr_number = array(
		"nol",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan");

		$results = "";
		$length = strlen($after_comma);
		$i = 1;
		while($i<$length)
		{
			$get = substr($after_comma,$i,1);
			$results .= " ".$arr_number[$get];
			$i++;
		}
		return $results;
	}
	
}