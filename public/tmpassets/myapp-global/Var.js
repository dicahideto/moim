var nRow = null;
var aData = null;
var ajax = null;
var $showDuration = 1000;
var $hideDuration = 1000;
var $timeOut = 1000;
var $extendedTimeOut = 1000;
var $showEasing = "swing";
var $hideEasing = "linear";
var $showMethod = "fadeIn";
var $hideMethod = "fadeOut";
var opts_toaster = {
  "closeButton": true,
  "debug": false,
  "positionClass": "toast-top-full-width",
  "onclick": null,
  "showDuration": "1000",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

