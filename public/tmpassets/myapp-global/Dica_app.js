/**
Core script to handle the entire theme and core functions
**/

var Dica_app = function() {
	// alert(PathOrigin);
    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

	// var Path = window.location.origin + '/';
	// var Path = window.location.origin + '/scdm/'; 
	var Path = PathOrigin; 
	var smallCaf = Path + '/assets/sysimg/srcajaxwait.gif'; 
	var bigCaf = Path + '/assets/sysimg/ajax-loader-3.gif'; 
	
    var assetsPath = Path + 'assets/';
	
    return {
			
		init: function() {
			
		},	

		wew: function(a) {
			alert(a);
		},

		wew: function(a) {
			alert(a);
		},
		
		getPath: function() {
            return Path;
        },

        setPath: function(path) {
            Path = path;
        },
		
        getAssetsPath: function() {
            return assetsPath;
        },

		selectCustomPage : function (parent_div_name, page_route, pageNumb, filter){
			if (pageNumb === undefined || pageNumb == 'undefined')
			{
				page = 1;
			}
			else
			{
				page = pageNumb;
			}
			
			if (filter === undefined)
			{
				pilter = '';
			}
			else
			{
				pilter = filter;
			}
		
			var child_parent_div_name		= "subres_" + parent_div_name;
			var child_parent_div_name_caf 	= "subres_caf_" + parent_div_name;
			// var tese = "<span id='abe1' style='cursor:pointer;' onclick='dcae(1)' class='dc'>dica dica</span>";
			var tese = "";
			
			var remainHeight  = $(window).height() - ( $('#qemp').offset().top);
			

			var gonnacaf = "<div id='"+child_parent_div_name_caf+"' class='subres' style='text-align:center;z-index: 9999; width: 300px; max-height: 250px;overflow-y: auto;margin-top:2px;display: block;position:absolute;background-color:white;box-shadow: 0 3px 8px rgb(0 0 0 / 30%);'>" 
							+ "<img src='"+smallCaf+"' />" + 
							"</div>";

			

			$.ajax({
				type: "GET",
				cache: false,
				url: page_route+"?page_route="+page_route+"&parent_div_name="+parent_div_name+"&pageNumb="+page+"&pilter="+pilter,
				dataType: "html",
				beforeSend: function() {
					$("#"+parent_div_name+"").after( gonnacaf );
				},
				complete: function(){
					$('div[id^=subres_caf_]').remove();
				},
				success: function(res) 
				{
					tese = res;
				},
				error: function(xhr, ajaxOptions, thrownError)
				{
					
				},
				async: false
			});
			
			if (remainHeight > 300)
			{
				var gonna = "<div id='"+child_parent_div_name+"' class='subres' style='top:38px;z-index: 9999; width: 300px; max-height: 250px;overflow-y: auto;margin-top:2px;display: block;position:absolute;background-color:white;box-shadow: 0 3px 8px rgb(0 0 0 / 30%);'>" +
						 tese +
						"</div>";
			}
			else
			{
				var gonna = "<div id='"+child_parent_div_name+"' class='subres' style='bottom:38px;z-index: 9999; width: 300px; max-height: 250px;overflow-y: auto;margin-top:2px;display: block;position:absolute;background-color:white;box-shadow: 0 3px 8px rgb(0 0 0 / 30%);'>" +
						 tese +
						"</div>";
			}

			return $("#"+parent_div_name+"").after( gonna );

		},
		

		select2ajax : function (select_name, placeholder, back, front, table, url, param, value){
			return $(select_name).select2({
				placeholder: placeholder,
				allowClear: true,
				ajax: {
					method: "GET",
					url: url,
					dataType: 'json',
					quietMillis: 100,
					data: function (term, page) {
						return {
							term		: term, //search term
							page_limit	: 10, // page size
							field_back	: back, //field id
							field_front	: front, //field name
							table		: table,
							param		: param,
							value		: value
						};
					},
					results: function (data, page) {
						return { results: data.results };
					}
				},
				initSelection: function(element, callback) {
					return $.getJSON(url + "?id=" + (element.val()) + "&field_back=" + back + "&field_front=" + front + "&table=" + table + "&value=" + value, null, function(data) {
						return callback(data);
					});
				}
				
			});
		},
		
		ajax_form_table : function (clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, except_date=""){
			var block_ele = $('.content-body');
			$('#'+clickable_span).hide();
	        $.ajax(url, 
        	{
				type: 'POST',
				data: "val="+val,  // data to submit",  // data to submit
				dataType: "html",
				beforeSend: function () {
					Dica_app.block_start_one(block_ele);
				},
				success: function (data, status, xhr) {
					Dica_app.block_finish_one(block_ele);

					$('#'+clickable_place_form).html(data);
					$('.'+form_focus).focus();

					$("."+clickable_place_form).focusout(function(){
					  	$('#'+clickable_span).show();
					  	$('.tolee_form_').remove();
					});

					$("."+form_focus).focusout(function(){
					  	$('#'+clickable_span).show();
					  	$('.tolee_form_').remove();
					});
					
					$("."+form_focus).keypress(function (e) {
						var key = e.which;
						var value = $("."+form_focus).val();
						if(key == 13)  // the enter key code
						{

							if (save_column == 'tgl_keluar')
							{
								var index = value.indexOf(' ');
								var new_value = value.substring(0, index);


								var index2 = val.indexOf(' ');
								var past_value = val.substring(0, index2);
								let exc;
								

								if (except_date == 1)
								{
									
									 exc = "" != "";
									 
								}
								else
								{
									 exc = new_value != past_value;
								}

								if (exc)
								{
									// alert(value+" || "+val);

									swal.fire({
								    title: 'Konfirmasi',
								    text: 'Data Tanggal Keluar yang berbeda akan dipindahkan dari halaman ini ?',
								    type: 'warning',
								    showCancelButton: true,
								    cancelButtonText: 'Tidak',
								    confirmButtonText: 'Ya'
								    }).then(function(result) 
								    {
								        if (result.value) {
								            $.ajax(url_save, {
												type: 'POST',
												data: "value="+value+"&code="+code+"&column="+save_column+"&type_val="+type_val, 
												dataType: "json",
												beforeSend: function () {
													Dica_app.block_start_one(block_ele);
												},
												success: function (data, status, xhr) {
													Dica_app.block_finish_one(block_ele);

													if (data.status == 'error')
													{
														Swal.fire({
									                        type: 'error',
									                        title: 'Peringatan',
									                        html: data.message,
								                        })

								                        return false;
													}
													
													$('#'+clickable_span).show();
													
									  				$('.tolee_form_').remove();
									  				$('#'+clickable_span).data('val', value);
									  				// $('.'+clickable_span).attr("val",value); //setter
									  				if (value == '')
									  				{
									  					$('#'+clickable_span).text('-');
									  				}
									  				else
									  				{
									  					$('#'+clickable_span).text(value);
									  				}

									  				let row_kosong = "<tr class='data_klaim_kosong'>"+
																	"<td colspan='17' style='padding:10px;' class='bpad7 bleft bright bbottom'>"+
																			"Data Kosong"+
																		"</td>"+
																	"</tr>";

									  				$('#line_'+code).remove();
				        							$('#t_data_klaim tbody').before(row_kosong);
												},
												error: function (jqXhr, textStatus, errorMessage) {
													Dica_app.block_finish_one(block_ele);
													alert(errorMessage);
												}
											});	
								        }
								    });
								}
								else
								{

									$.ajax(url_save, {
										type: 'POST',
										data: "value="+value+"&code="+code+"&column="+save_column+"&type_val="+type_val, 
										dataType: "json",
										beforeSend: function () {
											Dica_app.block_start_one(block_ele);
										},
										success: function (data, status, xhr) {
											Dica_app.block_finish_one(block_ele);

											if (data.status == 'error')
											{
												Swal.fire({
							                        type: 'error',
							                        title: 'Peringatan',
							                        html: data.message,
						                        })

						                        return false;
											}
											
											
											$('#'+clickable_span).show();
											
							  				$('.tolee_form_').remove();
							  				$('#'+clickable_span).data('val', value);
							  				// $('.'+clickable_span).attr("val",value); //setter
							  				if (value == '')
							  				{
							  					$('#'+clickable_span).text('-');
							  				}
							  				else
							  				{
							  					$('#'+clickable_span).text(value);
							  				}
										},
										error: function (jqXhr, textStatus, errorMessage) {
											Dica_app.block_finish_one(block_ele);
											alert(errorMessage);
										}
									});	


								}
								
							}
							else
							{
								$.ajax(url_save, {
										type: 'POST',
										data: "value="+value+"&code="+code+"&column="+save_column+"&type_val="+type_val, 
										dataType: "json",
										beforeSend: function () {
											Dica_app.block_start_one(block_ele);
										},
										success: function (data, status, xhr) {
											Dica_app.block_finish_one(block_ele);

											if (data.status == 'error')
											{
												Swal.fire({
							                        type: 'error',
							                        title: 'Peringatan',
							                        html: data.message,
						                        })

						                        return false;
											}
											
											
											$('#'+clickable_span).show();
											
							  				$('.tolee_form_').remove();
							  				$('#'+clickable_span).data('val', value);
							  				// $('.'+clickable_span).attr("val",value); //setter
							  				if (value == '')
							  				{
							  					$('#'+clickable_span).text('-');
							  				}
							  				else
							  				{
							  					$('#'+clickable_span).text(value);
							  				}
										},
										error: function (jqXhr, textStatus, errorMessage) {
											Dica_app.block_finish_one(block_ele);
											alert(errorMessage);
										}
									});	
							}

							
						  	
						}
					});   


				},
				error: function (jqXhr, textStatus, errorMessage) {
					Dica_app.block_finish_one(block_ele);
					alert(errorMessage);
				}
			});
		},

		ajax_form_table_select2 : function (clickable_span, code, type_val, val, url, clickable_place_form, form_focus, url_save, save_column, url_select){
			var block_ele = $('.content-body');
			$('#'+clickable_span).hide();
	        $.ajax(url, 
        	{
				type: 'POST',
				data: "val="+val+"&url_select="+url_select,  // data to submit",  // data to submit
				dataType: "html",
				beforeSend: function () {
					Dica_app.block_start_one(block_ele);
				},
				success: function (data, status, xhr) {
					Dica_app.block_finish_one(block_ele);

					$('#'+clickable_place_form).html(data);
					$('.'+form_focus).focus();

					$("."+clickable_place_form).focusout(function(){
					  	$('#'+clickable_span).show();
					  	$('.tolee_form_').remove();
					});

					$(document).click(function(e) {
			            if( $(e.target).attr('class') != 'select2-selection__rendered' && e.target.id != 'info' && $(e.target).attr('class') != 'dc' && $(e.target).attr('class') != undefined) {
			                if (e.target.id != clickable_span)
			                {
			                	$('#'+clickable_span).show();
			                	$('.tolee_form_').remove();
			                	// alert();
			                }
			            }

			        });
					
					
					$("."+form_focus).change(function (e) {
						var key = e.which;
						var value = $("."+form_focus).val();
						
						  	$.ajax(url_save, {
								type: 'POST',
								data: "value="+value+"&code="+code+"&column="+save_column, 
								dataType: "json",
								beforeSend: function () {
									Dica_app.block_start_one(block_ele);
								},
								success: function (data, status, xhr) {
									Dica_app.block_finish_one(block_ele);
									// Swal.fire({
				     //                    type: 'success',
				     //                    title: 'Berhasil',
				     //                    html: data.message,
			      //                   })

									$('#'+clickable_span).show();
									
					  				$('.tolee_form_').remove();
					  				$('.select2-container').remove();
					  				$('#'+clickable_span).data('val', value);
					  				// $('.'+clickable_span).attr("val",value); //setter
					  				
					  				if (value == '' || value == null)
					  				{
					  					$('#'+clickable_span).text('-');
					  				}
					  				else
					  				{
					  					$('#'+clickable_span).text(value);
					  				}
								},
								error: function (jqXhr, textStatus, errorMessage) {
									Dica_app.block_finish_one(block_ele);
									alert(errorMessage);
								}
							});	
						
					});   


				},
				error: function (jqXhr, textStatus, errorMessage) {
					Dica_app.block_finish_one(block_ele);
					alert(errorMessage);
				}
			});
		},

		block_start_one : function (block_ele){
			return $(block_ele).block({
			      message: '<div class="semibold"><span class="bx bx-revision icon-spin text-left"></span>&nbsp; Loading ...</div>',
			      fadeIn: 1000,
			      fadeOut: 1000,
			      overlayCSS: {
			        backgroundColor: '#fff',
			        opacity: 0.8,
			        cursor: 'wait'
			      },
			      css: {
			        border: 0,
			        padding: '10px 15px',
			        color: '#fff',
			        width: 'auto',
			        backgroundColor: '#333'
			      }
			    });
		},

		block_finish_one : function (block_ele){
			return $(block_ele).unblock();
		},

		select2_initial : function (placeholder){
			return $(".select2").select2({
				    dropdownAutoWidth: true,
				    allowClear: true,
				    placeholder: placeholder,
				    width: '100%'
			 	});
		},
		
        setAssetsPath: function(path) {
            assetsPath = path;
        },

        setGlobalImgPath: function(path) {
            globalImgPath = assetsPath + path;
        },

        getGlobalImgPath: function() {
            return globalImgPath;
        },
		handle_data_table : function(data_table,data) {
			return  data_table.dataTable(
						data
					);
		},
		
		handle_Link : function(url) {
			handleAjaxLink(url);
		},
		attrDefault : function($el, data_var, default_val) {

			if(typeof $el.data(data_var) != 'undefined')
			{
				return $el.data(data_var);
			}
			return default_val;
		},

		ajax_func : function(url,data,dataType,method) {
			return $.ajax({
				url: url,
				method: method,
				async: true,
				dataType: dataType,
				data: data,
				cache: false
			}); 
		},
		
		ajax_dic_func : function(url,data,dataType,type) {
			return $.ajax({
				type: type,
				url: url,
				data: data,
				dataType: dataType,
				async: true
			  });
		},

		ajax_process_html : function(url,data,dataType,type) {
			var block_ele = $('.content-body');
			return $.ajax({
				    type: type,
				    cache: false,
				    data: data,
				    url: url,
				    dataType: dataType,
				    beforeSend: function(){
		       			Dica_app.block_start_one(block_ele);
				    },
				    complete: function(){
				       Dica_app.block_finish_one(block_ele);
				    },
				    success: function (data, status, xhr) 
				    {

				    	let rum = JSON.stringify(data);
				    	// alert(rum);
	        			let rume = JSON.parse(rum);
				        return rume;
				    },
				    error: function(xhr, ajaxOptions, thrownError)
				    {
				        
				    },
				    async: false
				});
		}, 

		ajax_embbed_save : function(url, data) {
			var block_ele = $('.content-body');
			return $.ajax(url, {
					type: 'POST', 
					data: data,
					contentType: false,       
					cache: false,             
					processData:false,        
					dataType: 'html',
					beforeSend: function(){
		       			Dica_app.block_start_one(block_ele);
				    },
				    complete: function(){
				       Dica_app.block_finish_one(block_ele);
				    },
					success: function (data, status, xhr) {
						Dica_app.block_finish_one(block_ele);
					},
					error: function (jqXhr, textStatus, errorMessage) {
						Dica_app.block_finish_one(block_ele);
					}
				});	
		},

		ajax_save_respone : function(url, data, datatype) {
			var block_ele = $('.content-body');
			return $.ajax(url, {
					type: 'POST', 
					data: data,
					contentType: false,       
					cache: false,             
					processData:false,        
					dataType: datatype,
					beforeSend: function(){
		       			Dica_app.block_start_one(block_ele);
				    },
				    complete: function(){
				       Dica_app.block_finish_one(block_ele);
				    },
					success: function (data, status, xhr) {
						Dica_app.block_finish_one(block_ele);
					},
					error: function (jqXhr, textStatus, errorMessage) {
						Dica_app.block_finish_one(block_ele);
					}
				});	
		},

		
		ajax_process_json : function(url,data,dataType,type) {
			var block_ele = $('.content-body');
			return $.ajax({
				    type: type,
				    cache: false,
				    data: data,
				    url: url,
				    dataType: dataType,
				    beforeSend: function(){
		       			Dica_app.block_start_one(block_ele);
				    },
				    complete: function(){
				       Dica_app.block_finish_one(block_ele);
				    },
				    success: function (data, status, xhr) 
				    {
				    	let rum = JSON.stringify(data);
	        			let rume = JSON.parse(rum);
				        return rume;
				    },
				    error: function(xhr, ajaxOptions, thrownError)
				    {
				        
				    },
				    async: false
				});
		}, 

		ajax_datatable_cus : function(url,data,tbl) {
			return $.ajax({
				type: "POST",
				cache: false,
				url: url,
				data: data,
				dataType: "html",
				async: true,
				beforeSend: function(){
		       
			    },
			    complete: function(){
			       
			    },
			    success: function (data_res, status_res, xhr) {
			    	$("."+tbl+"_arrah").remove();
			    	var resp = JSON.parse(data_res);
			    	$("#"+tbl+" > tbody").html(resp.datae);
			    	$("#"+tbl+"").after(resp.recordPagination);
			    },
			    error: function (jqXhr, textStatus, errorMessage) {
			        alert(errorMessage);
			    }
		  	});
		},

		datatable_cus_pageNum: function(pageNumber) {
            if (pageNumber === undefined)
			{
				page = 1;
			}
			else
			{
				page = pageNumber;
			}

			return page;
        },

        datatable_cus_filter: function(filter) {
            if (filter === undefined)
			{
				pilter = '';
			}
			else
			{
				pilter = filter;
			}

			return pilter;
        },
	

		datatable_cus_expand_event_click: function (baseDat, optDat, divDat){
			// alert(baseDat+" - "+optDat);

			if(baseDat==optDat){
				$("div[id^="+divDat+"]").remove();
				baseDat = 0;
			 }
			 else if(baseDat==0 || optDat != baseDat){
				if($("div[id^="+divDat+"]").length>0){
				   $("div[id^="+divDat+"]").remove();
				   baseDat = 0;
				}
				baseDat = optDat;
		 	}
		 	
		 	return baseDat;
		},


		datatable_cus_expand_event_click_no_id: function (divDat){
				$("div[id^="+divDat+"]").remove();
		},



		form_process_valid_additional : function (divDat){
			
		},




		// $.ajax(
		// 	Dica_app.getPath() + "Dev/Table_one/tese", {
		//     type: 'POST',  // http method
		//     data: 'filter='+pilter+"&page="+page+"&tbl=tese",  // data to submit
		//     dataType: "html",
		//     beforeSend: function(){
		       
		//     },
		//     complete: function(){
		       
		//     },
		//     success: function (data, status, xhr) {
		//     	$('.tese_arrah').remove();
		//     	var resp = JSON.parse(data);
		//     	$("#tese > tbody").html(resp.datae);
		//     	$("#tese").after(resp.recordPagination);
		//     },
		//     error: function (jqXhr, textStatus, errorMessage) {
		//         alert(errorMessage);
		//     }
		// });
		
		
		custom_validation : function(require,message,) {
			return require;
		},
		
		
		
		iCheckHandle: function(check){
			check.iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue'
			});
		},
		
		validateHandle: function () {
            $("form.validate").each(function (i, el) {
                var $this = $(el),
					opts = {
					   
						rules: {
						},
					    messages: {},
					    errorElement: 'span', //default input error message container
					    errorClass: 'help-block', // default input error message class
					    focusInvalid: false, // do not focus the last invalid input
					    ignore: "",
						highlight: function (element) {
					        $(element).closest('.form-rror').addClass('has-error');
					    },
					    unhighlight: function (element) {
					        $(element).closest('.form-rror').removeClass('has-error');
					    },
						
						
						
					   errorPlacement: function(error, element) {
							var name = element.attr('name');
							var errorSelector = '.validation_error_message[for="' + name + '"]';
							var $element = $(errorSelector);
							if ($element.length) { 
								$(errorSelector).html(error.html());
							} else {
								error.insertAfter(element);
							}
						}
					},
					$fields = $this.find('[data-validate]');


                $fields.each(function (j, el2) {
                    var $field = $(el2),
						name = $field.attr('name'),
						validate = attrDefault($field, 'validate', '').toString(),
						_validate = validate.split(',');

                    for (var k in _validate) {
                        var rule = _validate[k],
							params,
							message;

                        if (typeof opts['rules'][name] == 'undefined') {
                            opts['rules'][name] = {};
                            opts['messages'][name] = {};
                        }

                        if ($.inArray(rule, ['required', 'url', 'email', 'number', 'date', 'creditcard']) != -1) {
                            opts['rules'][name][rule] = true;

                            message = $field.data('message-' + rule);

                            if (message) {
                                opts['messages'][name][rule] = message;
                            }
                        }
                            // Parameter Value (#1 parameter)
                        else
                            if (params = rule.match(/(\w+)\[(.*?)\]/i)) {
                                if ($.inArray(params[1], ['min', 'max', 'minlength', 'maxlength', 'equalTo']) != -1) {
                                    opts['rules'][name][params[1]] = params[2];

                                    message = $field.data('message-' + params[1]);

                                    if (message) {
                                        opts['messages'][name][params[1]] = message;
                                    }
                                }
                            }
                    }
                });

                $this.validate(opts);
            });
        }
		
    };

}();
function attrDefault($el, data_var, default_val) {
    if (typeof $el.data(data_var) != 'undefined') {
        return $el.data(data_var);
    }

    return default_val;
}