
var Manage_klaim = function () {
	var t_list_manage_klaim = $("#t_list_manage_klaim");

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


	var handle_datatable = function () {
		

		window.onload = function() {
			t_list_manage_klaim(1, '');
		};

		$("#change_periode").click(function(){
			$('#periode_sess').text($( "#bulan option:selected" ).text() + ' ' + $('#tahun').val());
			$('#berkas_klaim').text($( "#bulan option:selected" ).text() + ' ' + $('#tahun').val());
		  	t_list_manage_klaim(1, '');
		});

		$(document).on("click", "#input_data_klaim", function () {
			var block_ele = $('.content-body');

			let data 		= '';
			let url 		= Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/check_date_out";
	
			Dica_app.ajax_embbed_save(url, data).done(function(response){
				let d = JSON.parse(response);
			    // if (d.message == 'success')
			    // {
		    	 	 let step 		= 1;
				  	 let dateout 	= d.date;
				  
				  	 $.redirect(Dica_app.getPath() + "Transaction/klaim/Data_klaim",
					 {
						step_newpage: step,
						dateout: dateout,
					 },
					 "POST", "_self");
			    // }
			    // else
			    // {
			    	
			    // }
			});
		});

		$(document).on("click", ".detail_list_code", function () {
		
	    	 	 let code_log 		= $(this).data('code_log');
			  	 let step_newpage 	= $(this).data('step_newpage');
			  	 let dateout 		= $(this).data('dateout');
			  
			  	 $.redirect(Dica_app.getPath() + "Transaction/klaim/Data_klaim/approval_prog",
				 {
					step_newpage: step_newpage,
					dateout: dateout,
					code_log: code_log,
				 },
				 "POST", "_self");
		});

		dateoutx = 0;
		$(document).on("click", ".clickable_history", function () {
	       let dateout 	= $(this).data('dateout');
	       let step 	= $(this).data('step');

	       dateoutx = Dica_app.datatable_cus_expand_event_click(dateoutx, dateout, "nduuk_");
	      	if (dateoutx == 0){
	        	return false;
	        }
	        
	        let url 		= Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/history_app";
	        let data 		= 'dateout='+dateout+"&step="+step;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_sec_'+dateout).append(dateee.responseText);
	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });

		function t_list_manage_klaim(pageNumber, filter){
			let page 	= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 	= Dica_app.datatable_cus_filter(filter);

			let month_var 	= $('#bulan').val();
			let year_var 	= $('#tahun').val();
			
			let url_send	 = Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/t_list_manage_klaim";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&tbl=t_list_manage_klaim&month_var="+month_var+"&year_var="+year_var;

			Dica_app.ajax_datatable_cus(url_send, data_send, "t_list_manage_klaim");
		}

		$('#qfilter').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				if(value.length < 3 && value.length > 0) {
				  return '';
				}
				table_t_user(1, value);
			}
		});

		$(document).on("click", ".t_list_user", function () {
	        let sdfds = $(this).data('page');
	        t_list_manage_klaim(sdfds, '');
	    });

		// $(document).on("mouseover", ".hmm", function () {
		//    $(this).css("background","#e3e3e3");

		// });
		
		// $(document).on("mouseleave", ".hmm", function () {
		//    $(this).css("background","");

		// });

		$(document).on("click", "#import_data_klaim", function () {
		   setTimeout(function(){ 
				window.open(Dica_app.getPath() + "Transaction/Klaim/Import_klaim", "_self"); 
				
				}, 100);
		});

		var codex = 0;
		$(document).on("click", ".clickable_desc", function () {
	        let code = $(this).data('code');
	        let step = $(this).data('step');
	        
	        codex = Dica_app.datatable_cus_expand_event_click(codex, code, "tolee_");
	        if (codex == 0){
	        	return false;
	        }
	        	
	        let url 		= Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/detail_klaim_pertanggal_keluar";
	        let data 		= 'code='+code+'&step='+step;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+code).append(dateee.responseText);
	        	t_list_klaim_pertanggal_keluar(1, step, '');

	        	$('#kt_datepicker').datepicker({
		            todayHighlight: true,
		            todayBtn: true,
		            clearBtn: true,
		            autoclose: true,
		            orientation: "bottom left",
					format:'dd MM yyyy',
		            templates: {
		                leftArrow: '<i class="la la-angle-left"></i>',
		                rightArrow: '<i class="la la-angle-right"></i>'
		            }
		        });

	        	$('#kt_datepicker').on('change', function() {
					t_list_klaim_pertanggal_keluar(1, step, this.value);
				});


				$(document).on("click", ".clickable_dateout", function () {
				   let step 	= $(this).data('step');
				   let dateout 	= $(this).data('dateout');
				  // alert(step);
				   $.redirect(Dica_app.getPath() + "Transaction/klaim/Data_klaim",
					  {
						step_newpage: step,
						dateout: dateout,
					  },
					  "POST", "_self");
				});

	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });


		$(document).on("click", ".clickable_print", function () {
			let dateout 	= $(this).data('dateout');
			let step 		= $(this).data('step');
			
			$.redirect(Dica_app.getPath() + "Transaction/klaim/Manage_klaim/View_data_klaim",
			  {
				step: step,
				dateout: dateout,
			  },
			  "POST", "_blank");
		});

		$(document).on("click", ".t_list_klaim_pertanggal_keluar", function () {
			let pageNumber 	= $(this).data('page');
			let step 		= $(this).data('step');
			let filter 		= $(this).data('filter');



			t_list_klaim_pertanggal_keluar(pageNumber, step, filter);
		});

		$(document).on("click", ".clickable_desc_end", function () {
	        let code = '100';
	        let step = $(this).data('step');
	        
	        codex = Dica_app.datatable_cus_expand_event_click(codex, code, "tolee_");
	        if (codex == 0){
	        	return false;
	        }
	        	
	        let url 		= Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/detail_klaim_pertanggal_keluar";
	        let data 		= 'code='+code;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+code).append(dateee.responseText);
	        	t_list_klaim_pertanggal_keluar(1, step, '');

	        	$('#kt_datepicker').datepicker({
		            todayHighlight: true,
		            todayBtn: true,
		            clearBtn: true,
		            autoclose: true,
		            orientation: "bottom left",
					format:'dd MM yyyy',
		            templates: {
		                leftArrow: '<i class="la la-angle-left"></i>',
		                rightArrow: '<i class="la la-angle-right"></i>'
		            }
		        });

	        	$('#kt_datepicker').on('change', function() {
					t_list_klaim_pertanggal_keluar(1, step, this.value);
				});

				$(document).on("click", ".clickable_dateout", function () {
				   let step 	= $(this).data('step');
				   let dateout 	= $(this).data('dateout');
				  // alert(step);
				   $.redirect(Dica_app.getPath() + "Transaction/klaim/Data_klaim",
					  {
						step_newpage: step,
						dateout: dateout,
					  },
					  "POST", "_self");
				});
	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });



	    function t_list_klaim_pertanggal_keluar(pageNumber, step, filter){
			let page 	= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 	= Dica_app.datatable_cus_filter(filter);
			
			let bulan = $('#bulan').val();
			let tahun = $('#tahun').val();

			let url_send	 = Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/t_list_klaim_pertanggal_keluar";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&tbl=t_list_klaim_pertanggal_keluar"+"&step="+step+"&month_var="+bulan+"&year_var="+tahun;

			Dica_app.ajax_datatable_cus(url_send, data_send, "t_list_klaim_pertanggal_keluar");
		}


	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();