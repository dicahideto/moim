
var Import_klaim = function () {
	
	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


	var handle_datatable = function () {
		
		$("#but_upload").click(function(){
			var block_ele = $('.content-body');

			let formse 		= $('#form_main')[0];
			let data 		= new FormData(formse);
			let url 		= Dica_app.getPath() + "Transaction/Klaim/Import_klaim/prosesExcelSpread";
	
			Dica_app.ajax_embbed_save(url, data).done(function(response){
				let d = JSON.parse(response);
			    if (d.status=='success')
                {
                    setTimeout(function(){ 
                        window.open(Dica_app.getPath() + "Transaction/Klaim/List_import_klaim", "_self"); 
                        
				    }, 100);
                }
                else
                {
                    Swal.fire({
                        type: 'error',
                        title: 'Perhatian',
                        html: 'Gagal Import Data',
                    })
                    return false;
                }
			});
	    });

	    $(".select2").select2({
		    // the following code is used to disable x-scrollbar when click in select input and
		    // take 100% width in responsive also
		    dropdownAutoWidth: true,
		    allowClear: true,
		    placeholder: "Pilih Format Tanggal Data Excel",
		    width: 'resolve',
		    
	 	});
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();