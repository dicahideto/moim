
var Data_klaim_global = function () {
	var t_list_manage_klaim = $("#t_list_manage_klaim");

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


	var handle_datatable = function () {
		

		window.onload = function() {
			
		};

		$(document).keydown(function(event) {
		    if (event.which == 39 && event.ctrlKey) {
				document.getElementById('abcde').scrollLeft += 300;
			}

			if (event.which == 37 && event.ctrlKey) {
				document.getElementById('abcde').scrollLeft -= 300;
			}
	    });

		$("#right-button").click(function() {
		  document.getElementById('abcde').scrollLeft += 300;
		});

		$("#left-button").click(function() {
		  document.getElementById('abcde').scrollLeft -= 300;
		});
	
			
		$("#top-button-default").click(function() {
		   document.getElementById('abcde').scrollTop -= 300;
		});
		
		$("#bottom-button-default").click(function() {
		   document.getElementById('abcde').scrollTop += 300;
		});
		
		$("#top-button-full").click(function() {
		   window.scrollBy(0, -100);
		});
		
		$("#bottom-button-full").click(function() {
		   window.scrollBy(0, 100);
		});

		$(".select2").select2({
		    dropdownAutoWidth: true,
		    allowClear: true,
		    placeholder: "Search Username...",
		    width: '40%'
	 	});

		$("#value_select_filter").change(function() {
			let value_filter_utama = $('#value_filter_utama').val();
			let value_select_filter = $('#value_select_filter').val();

		   if (value_select_filter == 'tgl_keluar')
		   {
		   		$('#filter_date').show();
		   		$('#filter_normal').hide();
		   		$('#filter_date_range').hide();

		   		$('#value_filter_utama').val('');
		   		$('#value_filter_utama_date').val('');
				$('#value_filter_utama_date_one').val('');
				$('#value_filter_utama_date_two').val('');
		   }
		   else if (value_select_filter == 'tgl_keluar_range')
		   {
				$('#filter_date').hide();
				$('#filter_normal').hide();
				$('#filter_date_range').show();

				$('#value_filter_utama').val('');
				$('#value_filter_utama_date').val('');
				$('#value_filter_utama_date_one').val('');
				$('#value_filter_utama_date_two').val('');
		   }
		   else 
		   {
		   		$('#filter_date').hide();
		   		$('#filter_date_range').hide();
		   		$('#filter_normal').show();

		   		$('#value_filter_utama').val('');
		   		$('#value_filter_utama_date').val('');
				$('#value_filter_utama_date_one').val('');
				$('#value_filter_utama_date_two').val('');
		   } 
		});

		$("#submit_excel").click(function() {
			
				var htmls = document.getElementById('t_data_klaim').innerHTML;
				var uri = 'data:application/vnd.ms-excel;base64,';
				var template = `<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns='http://www.w3.org/TR/REC-html40'><head><style> .str{ mso-number-format:\@; } </style><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body class='str'><table class='str'>{table}</table></body></html>`;
				var base64 = function(s) {
					return window.btoa(unescape(encodeURIComponent(s)))
				};
	
				var format = function(s, c) {
					return s.replace(/{(\w+)}/g, function(m, p) {
						return c[p];
					})
				};
	
				var ctx = {
					worksheet : 'Worksheet',
					table : htmls
				}
	
				var link = document.createElement('a');
				link.download = 'Laporan.xls';
				link.href = uri + base64(format(template, ctx));
				link.click();


		});

	
		
		$('#value_filter_utama_date').datepicker({
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
            autoclose: true,
            orientation: "bottom left",
			format:'d MM yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

		
		$('.value_filter_utama_date_').datepicker({
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
            autoclose: true,
            orientation: "bottom left",
			format:'d MM yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });


		$(document).on("click", "#submit_filter_cari", function () {
			
			let value_filter_utama 		= '';
			let value_filter_utama_sec 	= '';
			let value_select_filter 	= $('#value_select_filter').val();
			let data = "";

		   if (value_select_filter == 'tgl_keluar')
		   {
		   		value_filter_utama 	= $('#value_filter_utama_date').val();		

				data 				= 'value_filter_utama='+value_filter_utama+'&value_select_filter='+value_select_filter;
		   }
		   else if (value_select_filter == 'tgl_keluar_range')
		   {
				value_filter_utama 			= $('#value_filter_utama_date_one').val();	
				value_filter_utama_sec 		= $('#value_filter_utama_date_two').val();	

				if (value_filter_utama == '')
				{
					Swal.fire({
							type: 'error',
							title: 'Perhatian',
							html: 'Mohon Isi Tanggal Awal',
						})
					return false;
				}
				if (value_filter_utama_sec == '')
				{
					Swal.fire({
							type: 'error',
							title: 'Perhatian',
							html: 'Mohon Isi Tanggal Akhir',
						})
					return false;
				}

				data 				= 'value_filter_utama='+value_filter_utama+'&value_filter_utama_sec='+value_filter_utama_sec+'&value_select_filter='+value_select_filter;		
		   }
		   else
		   {
				value_filter_utama 	= $('#value_filter_utama').val();

				data 				= 'value_filter_utama='+value_filter_utama+'&value_select_filter='+value_select_filter;			
		   }
			
			
			if (value_select_filter == '')
			{
				Swal.fire({
                        type: 'error',
                        title: 'Perhatian',
                        html: 'Mohon Pilih Kriteria Filter',
                    })
	        	return false;
			}

			if (value_filter_utama == '')
			{
				Swal.fire({
                        type: 'error',
                        title: 'Perhatian',
                        html: 'Mohon Isi Filter Utama',
                    })
	        	return false;
			}

			let url 		= Dica_app.getPath() + "Transaction/Klaim/Data_klaim_global/result_table_filter";
		    let dataType 	= 'json';
		    let type 		= 'POST';

		    dateee 		= Dica_app.ajax_process_json(url, data, dataType, type);
		    let rest 	= JSON.parse(dateee.responseText);

		    if (dateee.status == 200)
		    {
		    	$('#default_empty_td').remove();
		    	$('#t_data_klaim tbody').html(rest.row_result);
		    }
		    else
		    {
		    	alert(dateee.statusText);
		    }

  		});

		
		$(document).on("click", ".clickable_print", function () {
			let dateout 	= $(this).data('dateout');
			let step 		= $(this).data('step');
			
			$.redirect(Dica_app.getPath() + "Transaction/klaim/Manage_klaim/View_data_klaim",
			  {
				step: step,
				dateout: dateout,
			  },
			  "POST", "_blank");
		});

		$(document).on("click", ".t_list_klaim_pertanggal_keluar", function () {
			let pageNumber 	= $(this).data('page');
			let step 		= $(this).data('step');
			let filter 		= $(this).data('filter');

			t_list_klaim_pertanggal_keluar(pageNumber, step, filter);
		});

		$(document).on("click", ".clickable_desc_end", function () {
	        let code = '100';
	        let step = $(this).data('step');
	        
	        codex = Dica_app.datatable_cus_expand_event_click(codex, code, "tolee_");
	        if (codex == 0){
	        	return false;
	        }
	        	
	        let url 		= Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/detail_klaim_pertanggal_keluar";
	        let data 		= 'code='+code;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+code).append(dateee.responseText);
	        	t_list_klaim_pertanggal_keluar(1, step, '');

	        	$('#kt_datepicker').datepicker({
		            todayHighlight: true,
		            todayBtn: true,
		            clearBtn: true,
		            autoclose: true,
		            orientation: "bottom left",
					format:'dd MM yyyy',
		            templates: {
		                leftArrow: '<i class="la la-angle-left"></i>',
		                rightArrow: '<i class="la la-angle-right"></i>'
		            }
		        });

	        	$('#kt_datepicker').on('change', function() {
					t_list_klaim_pertanggal_keluar(1, step, this.value);
				});

				$(document).on("click", ".clickable_dateout", function () {
				   let step 	= $(this).data('step');
				   let dateout 	= $(this).data('dateout');
				  // alert(step);
				   $.redirect(Dica_app.getPath() + "Transaction/klaim/Data_klaim",
					  {
						step_newpage: step,
						dateout: dateout,
					  },
					  "POST", "_self");
				});
	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });



	    function t_list_klaim_pertanggal_keluar(pageNumber, step, filter){
			let page 	= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 	= Dica_app.datatable_cus_filter(filter);
			
			// let url_send	 = Dica_app.getPath() + "Master/Manage_users/list_user";
			let url_send	 = Dica_app.getPath() + "Transaction/Klaim/Manage_klaim/t_list_klaim_pertanggal_keluar";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&tbl=t_list_klaim_pertanggal_keluar"+"&step="+step;

			Dica_app.ajax_datatable_cus(url_send, data_send, "t_list_klaim_pertanggal_keluar");
		}




		$(document).on("click", ".clickable_ext_id", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val					= 'text';

			// alert(val);
			let clickable_span 			= "clickable_ext_id_"+code;
			let clickable_place_form 	= "clickable_ext_id_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "ext_id";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		$(document).on("click", ".clickable_person", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_person_"+code;
			let clickable_place_form 	= "clickable_person_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "person_nm";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		$(document).on("click", ".clickable_jaminan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_jaminan_"+code;
			let clickable_place_form 	= "clickable_jaminan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/fetch_select_jaminan";
			let save_column				= "jaminan";

			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });

		$(document).on("click", ".clickable_kelengkapan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_kelengkapan_"+code;
			let clickable_place_form 	= "clickable_kelengkapan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/fetch_select_kelengkapan";
			let save_column				= "kelengkapan";

			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });


		$(document).on("click", ".clickable_tgl_masuk", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';
			// alert(val);

			let clickable_span 			= "clickable_tgl_masuk_"+code;
			let clickable_place_form 	= "clickable_tgl_masuk_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_masuk";

			
			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

		$(document).on("click", ".clickable_tgl_keluar", function () {

			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';

			var block_ele = $('.content-body');
			
			// alert(val);

			let clickable_span 			= "clickable_tgl_keluar_"+code;
			let clickable_place_form 	= "clickable_tgl_keluar_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_keluar";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

		// $(document).on("click", ".clickable_tgl_diterima_rm", function () {
		// 	var block_ele = $('.content-body');
		// 	let val 					= $(this).data('val');
		// 	let code 					= $(this).data('code');
		// 	let type_val 				= 'date';
		// 	// alert(val);

		// 	let clickable_span 			= "clickable_tgl_diterima_rm_"+code;
		// 	let clickable_place_form 	= "clickable_tgl_diterima_rm_form_"+code;
		// 	let form_focus			 	= "form_text_embbed_table";
		// 	let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
		// 	let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
		// 	let save_column				= "tgl_diterima_rm";

		// 	Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	 //    });

		$(document).on("click", ".clickable_tgl_rm_dilengkapi", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';
			// alert(val);

			let clickable_span 			= "clickable_tgl_rm_dilengkapi_"+code;
			let clickable_place_form 	= "clickable_tgl_rm_dilengkapi_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_rm_dilengkapi";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		$(document).on("click", ".clickable_org", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_org_"+code;
			let clickable_place_form 	= "clickable_org_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Dev/Form_one/fetch_select_loadmore";
			let save_column				= "org";
			
			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });

	    $(document).on("click", ".clickable_ruang", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_ruang_"+code;
			let clickable_place_form 	= "clickable_ruang_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "ruang";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_dpjp", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_dpjp_"+code;
			let clickable_place_form 	= "clickable_dpjp_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "dpjp";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_keterangan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_keterangan_"+code;
			let clickable_place_form 	= "clickable_keterangan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "keterangan";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


	    $(document).on("click", ".clickable_gap", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_gap_"+code;
			let clickable_place_form 	= "clickable_gap_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "gap";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


	    $(document).on("click", ".clickable_hari", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_hari_"+code;
			let clickable_place_form 	= "clickable_hari_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "hari";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_jam", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_jam_"+code;
			let clickable_place_form 	= "clickable_jam_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "jam";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    


	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();