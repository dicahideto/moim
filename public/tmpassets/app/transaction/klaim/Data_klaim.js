
var Data_klaim = function () {
	var t_data_klaim = $("#t_data_klaim");

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


	var handle_datatable = function () {
		

		window.onload = function() {
			t_data_klaim(1, '');
		};

		$(document).keydown(function(event) {
		    if (event.which == 39 && event.ctrlKey) {
				document.getElementById('abcde').scrollLeft += 300;
			}

			if (event.which == 37 && event.ctrlKey) {
				document.getElementById('abcde').scrollLeft -= 300;
			}
	    });


		$("#right-button").click(function() {
		  document.getElementById('abcde').scrollLeft += 300;
		});

		$("#left-button").click(function() {
		  document.getElementById('abcde').scrollLeft -= 300;
		});
	
			
		$("#top-button-default").click(function() {
		   document.getElementById('abcde').scrollTop -= 300;
		});
		
		$("#bottom-button-default").click(function() {
		   document.getElementById('abcde').scrollTop += 300;
		});
		
		$("#top-button-full").click(function() {
		   window.scrollBy(0, -100);
		});
		
		$("#bottom-button-full").click(function() {
		   window.scrollBy(0, 100);
		});

		function t_data_klaim(pageNumber, filter){
			let page 			= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 			= Dica_app.datatable_cus_filter(filter);
			let step_newpage 	= $('#step_newpage').val();
			let dateout 		= $('#dateout').val();
			let code_log 		= $('#code_log').val();
			
			// let url_send	 = Dica_app.getPath() + "Master/Manage_users/list_user";
			let url_send	 = Dica_app.getPath() + "Transaction/Klaim/Data_klaim/t_data_klaim";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&step_newpage="+step_newpage+"&dateout="+dateout+"&tbl=t_data_klaim"+"&code_log="+code_log;

			Dica_app.ajax_datatable_cus(url_send, data_send, "t_data_klaim");
		}



		$('#qfilter').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				if(value.length < 3 && value.length > 0) {
				  return '';
				}
				table_t_user(1, value);
			}
		});

		$(document).on("click", "#tambah_klaim", function () {
			let code 					= $(this).data('code');

			swal.fire({
			    title: 'Konfirmasi',
			    text: 'Apakah Anda yakin tambah record dengan default tanggal keluar per hari ini?',
			    type: 'warning',
			    showCancelButton: true,
			    cancelButtonText: 'Tidak',
			    confirmButtonText: 'Ya'
			    }).then(function(result) 
			    {
			        if (result.value) 
			        {
			            let url 		= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/t_data_klaim_newone";
				        let data 		= '';
				        let dataType 	= 'json';
				        let type 		= 'POST';

				        dateee 		= Dica_app.ajax_process_json(url, data, dataType, type);
				        let rest 	= JSON.parse(dateee.responseText);
				        let code = rest.code;
				        if (dateee.status == 200)
				        {
					        let url 		= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/t_data_klaim_newone_html";
					        let data 		= 'code='+code;
					        let dataType 	= 'html';
					        let type 		= 'POST';

					        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
					        if (dateee.status == 200)
					        {
					        	
					        	$('#t_data_klaim tbody').before(dateee.responseText);
					        	$('.data_klaim_kosong').remove();
					        }
					        else
					        {
					        	alert(dateee.statusText);
					        }
				        }
				        else
				        {
				        	Swal.fire({
			                        type: 'error',
			                        title: 'Perhatian',
			                        html: 'Gagal Tambah Data',
		                        })
				        	return false;
				        }
			        }
			    });
	    });

		$(document).on("click", ".clickable_del", function () {
			let code 					= $(this).data('code');

			swal.fire({
			    title: 'Konfirmasi',
			    text: 'Apakah Anda yakin untuk menghapusnya ?',
			    type: 'warning',
			    showCancelButton: true,
			    cancelButtonText: 'Tidak',
			    confirmButtonText: 'Ya'
			    }).then(function(result) 
			    {
			        if (result.value) {
			            
			            let url 		= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/del_data_klaim";
				        let data 		= 'code='+code;
				        let dataType 	= 'json';
				        let type 		= 'POST';

				        dateee 		= Dica_app.ajax_process_json(url, data, dataType, type);
				        let rest 	= JSON.parse(dateee.responseText);

				        if (dateee.status == 200)
				        {
				        	Swal.fire({
			                        type: 'success',
			                        title: 'Berhasil',
			                        html: rest.message,
		                        })
				        	$('#line_'+code).remove();
				        	$('#t_data_klaim tbody').before(rest.row_kosong);
				        }
				        else
				        {
				        	alert(dateee.statusText);
				        }
			        }
			    });
	    });

		$(document).on("click", ".clickable_ext_id", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val					= 'text';

			// alert(val);
			let clickable_span 			= "clickable_ext_id_"+code;
			let clickable_place_form 	= "clickable_ext_id_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "ext_id";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		$(document).on("click", ".clickable_person", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_person_"+code;
			let clickable_place_form 	= "clickable_person_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "person_nm";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		$(document).on("click", ".clickable_jaminan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_jaminan_"+code;
			let clickable_place_form 	= "clickable_jaminan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/fetch_select_jaminan";
			let save_column				= "jaminan";

			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });


		$(document).on("click", ".clickable_kelengkapan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_kelengkapan_"+code;
			let clickable_place_form 	= "clickable_kelengkapan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/fetch_select_kelengkapan";
			let save_column				= "kelengkapan";

			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });
		

		$(document).on("click", ".clickable_org", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'select';
			// alert(val);

			let clickable_span 			= "clickable_org_"+code;
			let clickable_place_form 	= "clickable_org_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_select";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let url_select				= Dica_app.getPath() + "Dev/Form_one/fetch_select_loadmore";
			let save_column				= "org";
			
			Dica_app.ajax_form_table_select2(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column, url_select);
	    });

		$(document).on("click", ".clickable_tgl_masuk", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';
			// alert(val);

			let clickable_span 			= "clickable_tgl_masuk_"+code;
			let clickable_place_form 	= "clickable_tgl_masuk_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_masuk";

			
			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

		$(document).on("click", ".clickable_tgl_keluar", function () {

			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';

			var block_ele = $('.content-body');
			
			// alert(val);

			let clickable_span 			= "clickable_tgl_keluar_"+code;
			let clickable_place_form 	= "clickable_tgl_keluar_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_keluar";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

		// $(document).on("click", ".clickable_tgl_diterima_rm", function () {
		// 	var block_ele = $('.content-body');
		// 	let val 					= $(this).data('val');
		// 	let code 					= $(this).data('code');
		// 	let type_val 				= 'date';
		// 	// alert(val);

		// 	let clickable_span 			= "clickable_tgl_diterima_rm_"+code;
		// 	let clickable_place_form 	= "clickable_tgl_diterima_rm_form_"+code;
		// 	let form_focus			 	= "form_text_embbed_table";
		// 	let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
		// 	let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
		// 	let save_column				= "tgl_diterima_rm";

		// 	Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	 //    });

		$(document).on("click", ".clickable_tgl_rm_dilengkapi", function () {
			var block_ele = $('.content-body');
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'date';
			// alert(val);

			let clickable_span 			= "clickable_tgl_rm_dilengkapi_"+code;
			let clickable_place_form 	= "clickable_tgl_rm_dilengkapi_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill_date";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "tgl_rm_dilengkapi";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


		

	    $(document).on("click", ".clickable_ruang", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_ruang_"+code;
			let clickable_place_form 	= "clickable_ruang_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "ruang";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_dpjp", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_dpjp_"+code;
			let clickable_place_form 	= "clickable_dpjp_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "dpjp";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_keterangan", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_keterangan_"+code;
			let clickable_place_form 	= "clickable_keterangan_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "keterangan";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


	    $(document).on("click", ".clickable_gap", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_gap_"+code;
			let clickable_place_form 	= "clickable_gap_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "gap";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });


	    $(document).on("click", ".clickable_hari", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_hari_"+code;
			let clickable_place_form 	= "clickable_hari_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "hari";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    $(document).on("click", ".clickable_jam", function () {
			let val 					= $(this).data('val');
			let code 					= $(this).data('code');
			let type_val 				= 'text';
			// alert(val);

			let clickable_span 			= "clickable_jam_"+code;
			let clickable_place_form 	= "clickable_jam_form_"+code;
			let form_focus			 	= "form_text_embbed_table";
			let url 					= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/form_fill";
			let url_save				= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/save_form_fill";
			let save_column				= "jam";

			Dica_app.ajax_form_table(clickable_span, code, val, type_val, url, clickable_place_form, form_focus, url_save, save_column);
	    });

	    

	    $('#checkAll').click(function () {    
		     $('input:checkbox').prop('checked', this.checked);    
		 });

	    $("#submit_process").click(function(event){
	    	swal.fire({
			    title: 'Konfirmasi',
			    text: 'Apakah Anda yakin untuk memprosesnya ?',
			    type: 'warning',
			    showCancelButton: true,
			    cancelButtonText: 'Tidak',
			    confirmButtonText: 'Ya'
			    }).then(function(result) 
			    {
			        if (result.value) {
			        	var code_group_date_out = $('#code_group_date_out').val();
			        	var dateout = $('#dateout').val();
			            var checkboxValues = [];
						$('input[name=name_checked_radio]:checked').map(function() {
							// let val = $('.radio_check').val();
				         	checkboxValues.push("'"+$(this).val()+"'");
						});

						if (checkboxValues == '')
						{
							Swal.fire({
			                        type: 'error',
			                        title: 'Perhatian',
			                        html: 'Mohon Pilih Data Klaim Terlebih Dahulu',
			                    })
				        	return false;
						}	
						else
						{
									let url 		= Dica_app.getPath() + "Transaction/Klaim/Data_klaim/klaim_process";
							        let data 		= 'listcode='+checkboxValues+"&code_group_date_out="+code_group_date_out+"&dateout="+dateout;
							        let dataType 	= 'json';
							        let type 		= 'POST';

							        dateee 		= Dica_app.ajax_process_json(url, data, dataType, type);
							        let rest 	= JSON.parse(dateee.responseText);
							        let code 	= rest.message;
									if (rest.error == 'error')
							        {
							        	Swal.fire({
						                        type: 'error',
						                        title: 'Perhatian',
						                        html: code,
						                    })
							        	return false;
							        }
							        else
							        {
										if (dateee.status == 200)
										{	
											setTimeout(function(){ 
												window.open(Dica_app.getPath() + "Transaction/Klaim/Manage_klaim", "_self"); 
												
											}, 100);
										}
										else
										{
											Swal.fire({
													type: 'error',
													title: 'Perhatian',
													html: 'Gagal Tambah Data',
												})
											return false;
										}
							        }

							       
						}
			        
			        }
			    });


		   
			
			
	
		});



	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
	
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();