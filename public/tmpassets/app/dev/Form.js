$(document).click(function(e) {
	if( e.target.id != 'info' && $(e.target).attr('class') != 'dc' && $(e.target).attr('class') != undefined) {
		$('div[id^=subres_]').remove();
	}
});

var Form = function () {
	var table_daftar_item_fp = $("#table_daftar_item_fp");
	var table_pilih_barang = $("#table_pilih_barang");
	var table_pilih_supplier = $("#table_pilih_supplier");
	var table_batch_number_faktur_pembelian = $("#table_batch_number_faktur_pembelian");

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_title = function () {
	
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function () {
		$('#select2loadmore').select2({
		    // dropdownParent: $("#modal-compose") ,
		    placeholder: "Search Username...",
		    // minimumInputLength: 1,
		    allowClear: true,
		    ajax: {
		        url: Dica_app.getPath() + "Dev/Form_one/fetch_select_loadmore",
		        dataType: 'json',
		        delay: 250,
		        cache: false,
		        data: function (params) {
		            return {
		                term: params.term,
		                page: params.page || 1,
		            };
		        },
		        processResults: function(data, params) {
		            console.log(data);
		            var page = params.page || 1;
		            return {
		                results: $.map(data, function (item) { return {id: item.col, text: item.col}}),
		                pagination: {
		                    more: (page * 10) <= data[0].total_count
		                }
		            };
		        },              
		    }
		});
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		$(".readonly").keydown(function (e) {
			e.preventDefault();
		});

		$(document).on('click', '#submit_form_main', function () {
			if ($("#form_main").valid() == true) {
				alert();
			}
		});

		$('.form').submit(function(){
			var nama = $('#nama').val().length;
			
			if (nama == 0){
				$('.pesan-nama').css('display', 'block');
				return false;
			}
		})

		// $(document).ready(function () {
			var validobj = $("#form_main").validate({

				rules: {
					fname: {
						required: true,
					},
					selectselect: {
						required: true,
					},
					Color: {
						required: true,
					},
					Colo2r: {
						required: true,
					},
					agree: {
						required: true,
					},
					select2loadmore: {
						required: true,
					},
				},
				messages: {
					fname: {
						required: "Jumlah harus diisi",
					},
					selectselect: {
						required: "Select harus diisi",
					},
					Color: {
						required: "Gender harus diisi",
					},
					Colo2r: {
						required: "Gender harus diisi",
					},
					agree: {
						required: "Check harus diisi",
					},
					select2loadmore: {
						required: "Select Loadmore harus diisi",
					},
				},
			
				errorElement: "em",
				errorPlacement: function (error, element) {
					var element = $(element);

					element.addClass('is-invalid');
					error.addClass('invalid-feedback');

					if (element.is(':checkbox') || element.is(':radio')) {
						// element.closest('.form-rror').find('> span').after(error);
						// error.insertAfter(element.parent('.radiocheck_span'));
						// error.insertAfter(element);
						// alert(JSON.stringify(element));
						error.insertAfter     (element.closest('.form-group').find('.radiocheck_span'));
					} else if (element.hasClass("select2")) {
		                error.insertAfter(element.next('.select2'));
		            } else {
		                element.after(error);
		            }
				},

				highlight: function (element, errorClass, validClass) {
		            var element = $(element);
		            if (element.hasClass("select2")) {
		                $("#s2id_" + element.attr("id") + " ul").addClass(errorClass);
		            } else {
		                element.addClass(errorClass);
		            }
		        },

		        //When removing make the same adjustments as when adding
		        unhighlight: function (element, errorClass, validClass) {
		            var element = $(element);
		            if (element.hasClass("select2")) {
		                $("#s2id_" + element.attr("id") + " ul").removeClass(errorClass);
		            } else {
		                element.removeClass(errorClass);
		            }
		        }
			});
		// });

		$(document).on("change", ".select2", function () {
	        if (!$.isEmptyObject(validobj.submitted)) {
	            validobj.form();
	        }
	    });


		$(document).on('click', '#submit_form_batch', function () {
			if ($("#form_batch").valid() == true) {
				swal.fire({
					title: 'Apakah Anda yakin untuk menambah data batch ini?',
					text: "",
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'Tidak',
					confirmButtonText: 'Ya'
				}).then(function (result) {

					if (result.value) {
						$.ajax(Dica_app.getPath() + "pembelian/faktur_pembelian/save_batch", {
							type: 'POST',
							data: $('#form_batch').serialize() + "&no_dokumen_faktur=" + $('#no_dokumen_faktur').val(),  // data to submit",  // data to submit
							dataType: "json",
							beforeSend: function () {
								KTApp.blockPage({
									overlayColor: '#000000',
									type: 'v2',
									state: 'success',
									message: 'Please wait...'
								});
							},
							success: function (data, status, xhr) {
								KTApp.unblockPage();
								if (data.status == 'error') {
									swal.fire({
										title: "",
										text: data.message,
										type: "warning"
									}).then(function () {

									});
								}
								else {
									swal.fire({
										title: "",
										text: data.message,
										type: "success"
									}).then(function () {
										$('#kt_modal_b').modal('hide');
										table_batch_number_faktur_pembelian.api().ajax.url(Dica_app.getPath() + "allTable/table_batch_number_faktur_pembelian?code_barang=" + $('#code_barang_mdl').val() + "&no_dokumen_faktur=" + $("#no_dokumen_faktur").val()).load();
									});
								}
							},
							error: function (jqXhr, textStatus, errorMessage) {
								KTApp.unblockPage();
								alert(errorMessage);
							}
						});
					}
				});
			}
		});

		$(document).ready(function () {
			$("#form_batch").validate({
				rules: {
					batch_mdl: {
						required: true,
					},
					batch_date_mdl: {
						required: true,
					},
					value_batch_mdl: {
						required: true
					},
				},
				messages: {
					batch_mdl: {
						required: "Kode Batch harus diisi",
					},
					batch_date_mdl: {
						required: "Tanggal Batch harus diisi",
					},
					value_batch_mdl: {
						required: "Nilai Batch harus diisi",
					},
				},
				errorElement: "em",
				errorPlacement: function (error, element) {
					var element = $(element);

					var group = $(this).parent().find(element);
					var help = group.find('.form-text');

					if (group.find('.valid-feedback, .invalid-feedback').length !== 0) {
						return;
					}

					element.addClass('is-invalid');
					error.addClass('invalid-feedback');

					if (help.length > 0) {
						help.before(error);
					} else {
						if (element.closest('.bootstrap-select').length > 0) {     //Bootstrap select
							element.closest('.bootstrap-select').find('.bs-placeholder').after(error);
						} else if (element.closest('.input-group').length > 0) {   //Bootstrap group
							element.after(error);
						} else {                                                   //Checkbox & radios
							if (element.is(':checkbox')) {
								element.closest('.kt-checkbox').find('> span').after(error);
							} else {
								element.after(error);
							}
						}
					}
				},

				highlight: function (element, errorClass, validClass) {
					if ($(element).is('select')) {
						var group = valGetParentContainer(element);
						group.addClass('validate');
						group.addClass('is-invalid');
						$(element).removeClass('is-valid');

					}
					else {
						$(element).addClass("is-invalid").removeClass("is-valid");
					}

				},
				unhighlight: function (element, errorClass, validClass) {
					if ($(element).is('select')) {
						var group = valGetParentContainer(element);
						group.removeClass('validate');
						group.removeClass('is-invalid');
						$(element).removeClass('is-invalid');

					}
					else {
						// $(element).addClass("is-valid").removeClass("is-invalid");
					}
				}

			});
		});

		$(document).on('click', '#submit_main', function () {

			if ($("#form_input_main").valid() == true) {
				swal.fire({
					title: 'Apakah Anda yakin untuk menyimpan data Pembelian ini?',
					text: "",
					type: 'warning',
					showCancelButton: true,
					cancelButtonText: 'Tidak',
					confirmButtonText: 'Ya'
				}).then(function (result) {

					if (result.value) {
						$.ajax(Dica_app.getPath() + "pembelian/faktur_pembelian/save_main", {
							type: 'POST',
							data: $('#form_input_main').serialize() + "&no_dokumen_faktur=" + $('#no_dokumen_faktur').val(),  // data to submit",  // data to submit
							// contentType: false, 
							// cache: false,   
							// processData:false,  
							dataType: "json",
							beforeSend: function () {
								KTApp.blockPage({
									overlayColor: '#000000',
									type: 'v2',
									state: 'success',
									message: 'Please wait...'
								});
							},
							success: function (data, status, xhr) {
								KTApp.unblockPage();
								if (data.status == 'error') {
									swal.fire({
										title: "",
										text: data.message,
										type: "warning"
									}).then(function () {

									});
								}
								else {
									swal.fire({
										title: "",
										text: data.message,
										type: "success"
									}).then(function () {
										// setTimeout(function(){ window.open(Dica_app.getPath() + "pembelian/Order_pembelian", "_self"); }, 500);
									});
								}
							},
							error: function (jqXhr, textStatus, errorMessage) {
								KTApp.unblockPage();
								alert(errorMessage);
								
							}
						});
					}
				});
			}
		});

		$('#qemp').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				var x = $(window).height();
		        var y = $(document).height(); 
		      	
		      	

				$('div[id^=subres_]').remove();
				var parent_div_name 		= $(this).attr('id');
				var pagap 					= Dica_app.getPath() + "Dev/Form_one/fetch_select_paging";
				Dica_app.selectCustomPage(parent_div_name, pagap, 1, value);

				$(document).on("click", '.selcusdca', function(event) {
					var asdf 	= $(this).data("id");
					var sel 	= $(this).data("sel");
					// alert(sel.org_nm);
					$('#qemp').val(sel.org_nm);
				});
			}
		});

		$(document).on("click", '.navigate_select_custom_page', function(event) {
			$('div[id^=subres_]').remove();
			var parent_div_name = $(this).data("parent");
			var pagap = $(this).data("page_route");
			var pagenum = $(this).data("npage");
			
			Dica_app.selectCustomPage(parent_div_name, pagap, pagenum);
		});

		$('#qemp2').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				var x = $(window).height();
		        var y = $(document).height(); 
		      	

				$('div[id^=subres_]').remove();
				var parent_div_name 		= $(this).attr('id');
				var pagap 					= Dica_app.getPath() + "Dev/Form_one/fetch_select_paging2";
				Dica_app.selectCustomPage(parent_div_name, pagap, 1, value);

				$(document).on("click", '.selcusdca', function(event) {
					var asdf 	= $(this).data("id");
					var sel 	= $(this).data("sel");
					// alert(sel.org_nm);
					$('#qemp').val(sel.org_nm);
				});
			}
		});

		$(document).on("click", '.navigate_select_custom_page', function(event) {
			$('div[id^=subres_]').remove();
			var parent_div_name = $(this).data("parent");
			var pagap = $(this).data("page_route");
			var pagenum = $(this).data("npage");
			
			Dica_app.selectCustomPage(parent_div_name, pagap, pagenum);
		});

	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_modal = function () {
					   
		$(".select2").select2({
		    // the following code is used to disable x-scrollbar when click in select input and
		    // take 100% width in responsive also
		    dropdownAutoWidth: true,
		    allowClear: true,
		    placeholder: "Search Username...",
		    width: '100%'
	 	});

        $('#kt_datepicker').datepicker({
            todayHighlight: true,
            todayBtn: true,
            clearBtn: true,
            autoclose: true,
            orientation: "bottom left",
			format:'d MM yy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });


        $('#kt_datetimepicker').datetimepicker({
            todayHighlight: true,
            clearBtn: true,
            autoclose: true,
            pickerPosition: 'top-right',
            todayBtn: true,
            format: 'd MM yy hh:ii',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });


        $('#cal2').click(function(){
		       $(document).ready(function(){
		           $('#my-datepicker').datetimepicker({
			            todayHighlight: true,
			            clearBtn: true,
			            
        				// showOn: "button",
			            autoclose: true,
			            pickerPosition: 'top-right',
			            todayBtn: true,
			            format: 'd MM yy hh:ii',
			            templates: {
			                leftArrow: '<i class="la la-angle-left"></i>',
			                rightArrow: '<i class="la la-angle-right"></i>'
			            }
			        }).focus();
		       });
		   });


        $('#my-datepicker').change(function () {
		    alert($('#my-datepicker').val());
		});

		$('#jqdatetimepicker').datetimepicker({
		  format:'d/m/Y H:i',
		  lang:'id'
		});

		$("#jqdatetimepicker").keypress(function (e) {
			var key = e.which;
			var value = $("#jqdatetimepicker").val();
			if(key == 13)  // the enter key code
			{
				alert(value);
			}
		});
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_title();
			handle_modal();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();