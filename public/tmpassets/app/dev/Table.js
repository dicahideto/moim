
var Table = function () {
	var table_daftar_item_fp = $("#table_daftar_item_fp");


	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_title = function () {
	
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function () {
		$(document).ready(function () {
			Dica_app.handle_data_table(
				table_daftar_item_fp,
				{
					// "order": [[ 4, "asc" ],[ 3, "asc" ]],
					"processing": true,
					"serverSide": true,
					"columnDefs": [
						{ "visible": false, "targets": [] },
						{ "width": "5px", "targets": 0 },
					],

					"lengthMenu": [ 5, 25, 50, 75, 100 ],
					"colReorder": true,
					"dom": 'Bfrtip',
					"buttons": [
						{
							"extend": 'copyHtml5',
							"exportOptions": {
								"columns": [ 1, ':visible' ]
							}
						},
						{
							"extend": 'excelHtml5',
							"exportOptions": {
								"columns": ':visible'
							}
						},
						{
							"extend": 'pdfHtml5',
							"exportOptions": {
								"columns": [ 1, 2, 4 ]
							}
						},
						
					],
					"scrollX": true,
					"pagingType": "first_last_numbers",
					"ajax": Dica_app.getPath() + "Dev/Table_one/table_sample"
				}
			);

			table_daftar_item_fp.on('draw.dt', function () {
				setTimeout(function () {
					$('#table_daftar_item_fp').DataTable().columns.adjust();
				}, 100)
			});

			table_daftar_item_fp.on('draw.dt', function () {
				var table = $('#table_daftar_item_fp').DataTable();
				var info = table.page.info();
				table.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});
			
			$('a.toggle-vis').on( 'click', function (e) {
				e.preventDefault();
				var table = $('#table_daftar_item_fp').DataTable();
				// Get the column API object
				var column = table.column( $(this).attr('data-column') );
		 
				// Toggle the visibility
				column.visible( ! column.visible() );
			} );

		});
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		$.ajax(
			Dica_app.getPath() + "Dev/Table_one/custom_sample_table", {
		    type: 'POST',  // http method
		    data: '',  // data to submit
		    dataType: "json",
		    beforeSend: function(){
		       
		    },
		    
		    complete: function(){
		       
		    },
		    success: function (data, status, xhr) {
		       $.ajax(Dica_app.getPath() + "Dev/Table_one/custom_sample_table_proses", {
				    type: 'POST',  // http method
				    data: "data="+JSON.stringify(data),  // data to submit
				    dataType: "html",
				    beforeSend: function(){
				       
				    },
				    complete: function(){
				     
				    },
				    success: function (data, status, xhr) {
				       // $('#abcde').html(data);
				       // $('#tes tr:last').after(data);
				       $("#tes > tbody").append(data);
				    },
				    error: function (jqXhr, textStatus, errorMessage) {
			            alert(errorMessage);
				    }
				});	
		    },
		    error: function (jqXhr, textStatus, errorMessage) {
		            alert(errorMessage);
		    }
		});	

		window.onload = function() {
			table_tese(1, '');
		};

		function table_tese(pageNumber, filter){
			let page 	= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 	= Dica_app.datatable_cus_filter(filter);
			
			let url_send	 = Dica_app.getPath() + "Dev/Table_one/tese";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&tbl=tese";

			Dica_app.ajax_datatable_cus(url_send, data_send, "tese");
		}

		$('#qfilter').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				if(value.length < 3 && value.length > 0) {
				  return '';
				}
				table_tese(1, value);
			}
		});

		$(document).on("click", ".table_tese", function () {
	        let sdfds = $(this).data('page');
	        table_tese(sdfds, '');
	    });

		var org_idx = 0;
		$(document).on("click", ".tese_click", function () {
	        let org_id = $(this).data('org_id')
	        
	        org_idx = Dica_app.datatable_cus_expand_event_click(org_idx, org_id, "tolee_");
	        if (org_idx == 0){
	        	return false;
	        }
	        	
	        let url 		= Dica_app.getPath() + "Dev/Table_one/view_tese_detail";
	        let data 		= 'org_id='+org_id;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+org_id).append(dateee.responseText);
	        }
	        else
	        {
	        	alert('gagal ajax req');
	        }
	    });
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	var handle_modal = function () {
				
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_title();
			handle_modal();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();