
var Manage_users = function () {
	var table_t_user = $("#table_t_user");

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


	var handle_datatable = function () {
		

		window.onload = function() {
			table_t_user(1, '');
		};

		function table_t_user(pageNumber, filter){
			let page 	= Dica_app.datatable_cus_pageNum(pageNumber);
			let pilter 	= Dica_app.datatable_cus_filter(filter);
			
			// let url_send	 = Dica_app.getPath() + "Master/Manage_users/list_user";
			let url_send	 = Dica_app.getPath() + "Master/Users/Manage_users/tq_daftar_user";
			let data_send 	 = 'filter='+pilter+"&page="+page+"&tbl=t_list_user";

			Dica_app.ajax_datatable_cus(url_send, data_send, "t_list_user");
		}

		$('#qfilter').on('keypress', function(event) {
			var value = jQuery(this).val().toLowerCase();
			if (event.which == 13 && !event.shiftKey) {
				event.preventDefault();
				if(value.length < 3 && value.length > 0) {
				  return '';
				}
				table_t_user(1, value);
			}
		});

		$(document).on("click", ".t_list_user", function () {
	        let sdfds = $(this).data('page');
	        table_t_user(sdfds, '');
	    });

		$(document).on("click", "#tambah_user", function () {
	       let code = '';

	       codex = Dica_app.datatable_cus_expand_event_click(codex, code, "tolee_");
	      

	        let url 		= Dica_app.getPath() + "Master/Users/Manage_users/Form_user";
	        let data 		= 'code='+code;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+code).append(dateee.responseText);
	        	Dica_app.select2_initial('');
	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });

		var codex = 0;
		$(document).on("click", ".name_click", function () {
	        let code = $(this).data('code')
	        
	        codex = Dica_app.datatable_cus_expand_event_click(codex, code, "tolee_");
	        if (codex == 0){
	        	return false;
	        }
	        	
	        let url 		= Dica_app.getPath() + "Master/Users/Manage_users/Form_user";
	        let data 		= 'code='+code;
	        let dataType 	= 'html';
	        let type 		= 'POST';

	        dateee = Dica_app.ajax_process_html(url, data, dataType, type);
	        if (dateee.status == 200)
	        {
	        	$('#form_edit_'+code).append(dateee.responseText);
	        	Dica_app.select2_initial('');
	        }
	        else
	        {
	        	alert(dateee.statusText);
	        }
	    });

	    $(document).on("click", "#batal_form_main", function () {
	    	Dica_app.datatable_cus_expand_event_click_no_id("tolee_");
		});

	    var block_ele = $('.content-body');

		$(document).on('click', '#submit_form_main', function () {
			var validobj = $("#form_main").validate({
				rules: {
					username: {
						required: true,
					},
					password: {
						required: true,
					},
					nama: {
						required: true,
					},
				},
				messages: {
					username: {
						required: "Username harus diisi",
					},
					password: {
						required: "Password harus diisi",
					},
					nama: {
						required: "Nama harus disi",
					},
				},
			
				errorElement: "em",
				errorPlacement: function (error, element) {
					var element = $(element);

					element.addClass('is-invalid');
					error.addClass('invalid-feedback');

					if (element.is(':checkbox') || element.is(':radio')) {
						error.insertAfter     (element.closest('.form-group').find('.radiocheck_span'));
					} else if (element.hasClass("select2")) {
		                error.insertAfter(element.next('.select2'));
		            } else {
		                element.after(error);
		            }
				},

				highlight: function (element, errorClass, validClass) {
		            var element = $(element);
		            if (element.hasClass("select2")) {
		                $("#s2id_" + element.attr("id") + " ul").addClass(errorClass);
		            } else {
		                element.addClass(errorClass);
		            }
		        },

		        //When removing make the same adjustments as when adding
		        unhighlight: function (element, errorClass, validClass) {
		            var element = $(element);
		            if (element.hasClass("select2")) {
		                $("#s2id_" + element.attr("id") + " ul").removeClass(errorClass);
		            } else {
		                element.removeClass(errorClass);
		            }
		        }
			});

			if ($("#form_main").valid() == true) {
				swal.fire({
					title: 'Konfirmasi',
				    text: 'Apakah Anda yakin untuk menyimpannnya ?',
				    type: 'warning',
				    showCancelButton: true,
				    cancelButtonText: 'Tidak',
				    confirmButtonText: 'Ya'
				}).then(function (result) {
					if (result.value) {
						$.ajax(Dica_app.getPath() + "Master/Users/Manage_users/save_user", {
							type: 'POST',
							data: $('#form_main').serialize(),  // data to submit",  // data to submit
							dataType: "json",
							beforeSend: function () {
								Dica_app.block_start_one(block_ele);
							},
							success: function (data, status, xhr) {
								Dica_app.block_finish_one(block_ele);
								Swal.fire({
			                        type: 'success',
			                        title: 'Berhasil',
			                        html: data.message,
		                        })

								table_t_user(1, '');
								Dica_app.datatable_cus_expand_event_click_no_id("tolee_");

							},
							error: function (jqXhr, textStatus, errorMessage) {
								alert(errorMessage);
							}
						});
					}
				});
			}
		});


		$(document).on("click", ".non_aktif", function () {
	    	let code = $(this).data('code');

	    	swal.fire({
					title: 'Konfirmasi',
				    text: 'Apakah Anda yakin untuk menonaktifkannya ?',
				    type: 'warning',
				    showCancelButton: true,
				    cancelButtonText: 'Tidak',
				    confirmButtonText: 'Ya'
				}).then(function (result) {
					if (result.value) {
						$.ajax(Dica_app.getPath() + "Master/Users/Manage_users/non_aktif_hapus", {
							type: 'POST',
							data: "code="+code+"&tipe=non",  // data to submit",  // data to submit
							dataType: "json",
							beforeSend: function () {
								Dica_app.block_start_one(block_ele);
							},
							success: function (data, status, xhr) {
								Dica_app.block_finish_one(block_ele);
								Swal.fire({
			                        type: 'success',
			                        title: 'Berhasil',
			                        html: data.message,
		                        })

								table_t_user(1, '');
								Dica_app.datatable_cus_expand_event_click_no_id("tolee_");

							},
							error: function (jqXhr, textStatus, errorMessage) {
								alert(errorMessage);
							}
						});
					}
				});
		});

		$(document).on("click", ".hapus", function () {
	    	let code = $(this).data('code');
	    	swal.fire({
					title: 'Konfirmasi',
				    text: 'Apakah Anda yakin untuk menghapusnya ?',
				    type: 'warning',
				    showCancelButton: true,
				    cancelButtonText: 'Tidak',
				    confirmButtonText: 'Ya'
				}).then(function (result) {
					if (result.value) {
						$.ajax(Dica_app.getPath() + "Master/Users/Manage_users/non_aktif_hapus", {
							type: 'POST',
							data: "code="+code+"&tipe=hap",  // data to submit",  // data to submit
							dataType: "json",
							beforeSend: function () {
								Dica_app.block_start_one(block_ele);
							},
							success: function (data, status, xhr) {
								Dica_app.block_finish_one(block_ele);
								Swal.fire({
			                        type: 'success',
			                        title: 'Berhasil',
			                        html: data.message,
		                        })

								table_t_user(1, '');
								Dica_app.datatable_cus_expand_event_click_no_id("tolee_");

							},
							error: function (jqXhr, textStatus, errorMessage) {
								alert(errorMessage);
							}
						});
					}
				});
		});

	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
		// var block_ele = $(this).closest('.card');
		// $('.block').on('click', function () {
		//    Dica_app.block_start_one(block_ele);
	 //  	});

	 //  	$('.unblock').on('click', function () {
	 			// Dica_app.block_start_one(block_ele);
	 //     	Dica_app.block_finish_one(block_ele);
	 //    });


		

		// $(document).ready(function () {
			
		// });
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();