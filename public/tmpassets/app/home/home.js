
var Home = function () {
	
	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	var handle_datatable = function () {
		$(document).on("click", ".card_berkas", function () {
			let id 					= $(this).data('id');
			let year_var 					= $(this).data('year');
			let month_var 					= $(this).data('month');

	
			var block_ele = $('.content-body');

            $.ajax(Dica_app.getPath() + "home/app_detail_progress", {
                type: 'POST',  // http method
                data: 'id='+id+'&year_var='+year_var+'&month_var='+month_var,
                dataType: "html",
                beforeSend: function(){
                    Dica_app.block_start_one(block_ele);
                },
                
                complete: function(){
                    Dica_app.block_finish_one(block_ele);
                },
                success: function (data, status, xhr) {
                    $('#data_main').html(data);
                    
                    Dica_app.block_finish_one(block_ele);
                },
                error: function (jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                        Dica_app.block_finish_one(block_ele);
                }
            });	

	    });

		$("#change_periode").click(function(){
			$('#periode_sess').text($( "#bulan option:selected" ).text() + ' ' + $('#tahun').val());
		  	$.ajax({
			    type: "POST",
			    cache: false,
			    url: Dica_app.getPath() + "home/periode_monitoring",
			    dataType: "html",
			    data:{
		          year_var: $('#tahun').val(),
		          month_var: $('#bulan').val()
		        },
			    success: function(res) 
			    {
			        $('#result_monitoring').html(res);

			    },
			    error: function(xhr, ajaxOptions, thrownError)
			    {
			        
			    },
			    async: false
			});
		});

		$.ajax({
		    type: "POST",
		    cache: false,
		    url: Dica_app.getPath() + "home/periode_monitoring",
		    dataType: "html",
		    data:{
	          year_var: $('#tahun').val(),
	          month_var: $('#bulan').val()
	        },
		    success: function(res) 
		    {
		        $('#result_monitoring').html(res);
		    },
		    error: function(xhr, ajaxOptions, thrownError)
		    {
		        
		    },
		    async: false
		});



	    $("#bulan").select2({
		    dropdownAutoWidth: true,
		    allowClear: true,
		    placeholder:'Bulan',
		    width: '100%'
	 	});

	    $("#tahun").select2({
		    dropdownAutoWidth: true,
		    allowClear: true,
		    placeholder:'Tahun',
		    width: '100%'
	 	});
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	var handle_form = function () {
		
	};

	//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	return {
		//main function to initiate the module
		init: function () {
			handle_form();
			handle_datatable();
			// Dica_app.wew();
		}

	};

}();